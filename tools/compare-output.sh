#!/bin/sh

cd output

for fl in *.out ; do
    cmp $fl ../../theme-d-code/tests/$fl
done
