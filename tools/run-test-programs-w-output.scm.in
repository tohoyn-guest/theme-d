#!@theme_d_guile@ \
-e main -s
!#


;; Copyright (C) 2018 Tommi Höynälänmaa
;; Distributed under GNU General Public License version 3,
;; see file doc/GPL-3.


(import (theme-d common theme-d-config))
(import (theme-d testing test-info))
    

(init-theme-d-config)

(define gl-theme-d-run-path
  (get-theme-d-config-var 'run-path))


(define gl-theme-d-run-split-path
  (get-theme-d-config-var 'run-split-path))


(define (get-str-test-name i)
  (string-append "test" (number->string i)))


(define (get-test-filename i split?)
  (if split?
    (string-append (get-str-test-name i) ".build")
    (string-append (get-str-test-name i) ".go")))


(define (get-test-path i split?)
  (let ((filename (get-test-filename i split?)))
    (string-append "../theme-d-code/tests/" filename)))


(define (get-output-filename i)
    (string-append "test" (number->string i)  ".out"))


(define (get-output-path i)
  (let ((filename (get-output-filename i)))
    (string-append "output/" filename)))


(define (get-custom-file-path filename)
  (string-append "../theme-d-code/tests/" filename))


(define (run-tests)
  (do ((i 1 (+ i 1))) ((> i nr-of-tests))
    (if (and (not (member i no-run)) (not (member i modules)))
    (let* ((split? (memv i l-split-linking))
           (path (get-test-path i split?))
           (output-path (get-output-path i))
           (str-run-cmd
            (if split? gl-theme-d-run-split-path gl-theme-d-run-path)))
      (if (file-exists? path)
          (begin
        (display "Starting test ")
        (display i)
        (newline)
        (system (string-append str-run-cmd " " path
                       " 2>&1 | tee " output-path))
        (display "Finished test ")
        (display i)
        (newline)))))))


(define (main args)
  (run-tests))
