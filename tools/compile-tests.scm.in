#!@theme_d_guile@ \
-e main -s
!#

;; Copyright (C) 2008-2018, 2022, 2024 Tommi Höynälänmaa
;; Distributed under GNU General Public License version 3,
;; see file doc/GPL-3.


(import (rnrs exceptions))
(import (rnrs conditions))
(import (rnrs base))
(import (theme-d common theme-d-config))
(import (theme-d testing test-info))
(import (th-scheme-utilities hrecord))
(import (th-scheme-utilities parse-command-line))


(define (get-test-name i)
  (string-append "test" (number->string i)))


(define (get-source-ext unit-type)
  (case unit-type
    ((interface) ".thi")
    ((body) ".thb")
    ((proper-program) ".thp")
    ((script) ".ths")
    (else (raise 'invalid-unit-type))))


(define (get-target-ext unit-type)
  (case unit-type
    ((interface) ".tci")
    ((body) ".tcb")
    ((proper-program) ".tcp")
    ((script) ".tcs")
    (else (raise 'invalid-unit-type))))


(define (get-source-filename i unit-type)
  (let ((ext (get-source-ext unit-type)))
    (string-append "test" (number->string i) ext)))


(define (get-target-filename i unit-type)
  (let ((ext (get-target-ext unit-type)))
    (string-append "test" (number->string i) ext)))


(define (get-test-path filename)
  (string-append "../theme-d-code/tests/" filename))


(define (get-unit-type-string unit-type)
  (case unit-type
    ((proper-program) "proper program")
    ((script) "script")
    ((interface) "interface")
    ((body) "body")
    (else (raise 'internal-error))))


(define (compile-unit index unit-type str-compiler-cmd str-extra-options
		      output-file)
  (assert (integer? index))
  (assert (and (string? str-compiler-cmd)
	       (not (string-null? str-compiler-cmd))))
  (assert (string? str-extra-options))
  (assert (output-port? output-file))
  (let ((source-filename (get-source-filename index unit-type))
	(target-filename (get-target-filename index unit-type)))
    (let ((source-path (get-test-path source-filename))
	  (target-path (get-test-path target-filename)))
      (display (string-append "Compiling " (get-unit-type-string unit-type)
			      " "))
      (display source-filename)
      (newline)
      (flush-all-ports)
      (let ((str-command
	     (string-append
	      str-compiler-cmd
	      " -m ../theme-d-code: -o "
	      target-path " "
	      str-extra-options " " source-path " 2>&1")))
	(display str-command)
	(newline)
	(let ((result (system str-command)))		   
	  (if (= result 0)
	      (begin
		(display (string-append "Compilation of "
					(symbol->string unit-type) " ")
			 output-file)
		(display source-filename output-file)
		(display " succeeded." output-file)
		(newline output-file))
	      (begin
		(display "Compilation of " output-file)
		(display source-filename output-file)
		(display " failed." output-file)
		(newline output-file))))))))


(define (compile-components str-compiler-cmd str-extra-options
			    unit-type output-file)
  (do ((cur-modules modules (cdr cur-modules))) ((null? cur-modules))
    (let ((i-cur (car cur-modules)))
      (compile-unit i-cur unit-type str-compiler-cmd str-extra-options
		    output-file))))


(define (compile-programs str-compiler-cmd str-extra-options output-file)
  (do ((i 1 (+ i 1))) ((> i nr-of-tests))
    (if (not (member i modules))
	(let ((unit-type
	       (if (member i scripts) 'script 'proper-program)))
	  (compile-unit i unit-type str-compiler-cmd str-extra-options
			output-file)))))


(define (main args)
  (init-theme-d-config)
  (let* ((str-compiler-cmd0 "")
	 (split? #f)
	 (str-extra-options1 (getenv "EXTRA_COMP_OPTIONS"))
	 (str-extra-options
	  (if str-extra-options1 str-extra-options1 ""))
	 (argd1 (make-hrecord <argument-descriptor> #\b #t
			      (lambda (str-option-arg)
				(set! str-compiler-cmd0 str-option-arg))))
	 (argd2 (make-hrecord <argument-descriptor> #\s #t
			      (lambda (str-option-arg)
				(set! str-compiler-cmd0 str-option-arg)
				(set! split? #t))))
	 (l-arg-descs (list argd1 argd2))
	 (handle-proper-args
	  (lambda (l-proper-args)
	    (if (> (length l-proper-args) 0)
		(begin
		  (display "Extra arguments in command line. Exiting.\n")
		  (exit 1)))
	    #f))
	 (l-args-without-cmd (cdr args)))
    (parse-command-line l-args-without-cmd l-arg-descs handle-proper-args)
    (let ((str-compiler-cmd
	   (if (not (string-null? str-compiler-cmd0))
	       (string-append
		(if split? "run-split-theme-d-program " "run-theme-d-program ")
		str-compiler-cmd0)
	       (get-theme-d-config-var 'compiler-path))))
      (call-with-output-file "compile-tests.log"
	(lambda (output-file)
	  (compile-components str-compiler-cmd str-extra-options
			      'interface output-file)
	  (compile-components str-compiler-cmd str-extra-options
			      'body output-file)
	  (compile-programs str-compiler-cmd str-extra-options
			    output-file))))))
