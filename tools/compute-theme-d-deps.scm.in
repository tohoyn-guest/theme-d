#!@theme_d_guile@ \
-e main -s
!#

;; A script to compute Theme-D module dependencies.

;; Copyright (C) 2024 Tommi Höynälänmaa

;; Distributed under GNU LGPL.

(use-modules (srfi srfi-1)
             (rnrs lists)
             (rnrs io ports)
             (th-scheme-utilities stdutils))

(define (process-toplevel-expression sx l-s-lib)
  (if (and (list? sx) (not-null? sx)
           (memq (car sx) '(import import-and-reexport use)))
    (filter (lambda (sx-module)
              (and (list? sx-module)
                   (not-null? sx-module)
                   (equal? (drop-right sx-module 1) l-s-lib)))
            (cdr sx))
    '()))

(define (process-all-expressions l-sx l-s-lib)
  (apply append
    (map (lambda (sx) (process-toplevel-expression sx l-s-lib)) l-sx)))

(define (print-output-deps str-name str-ext l-sx-deps)
  (display (string-append str-name "." str-ext " :"))
  ;; A module body depends on its interface.
  (if (string=? str-ext "tcb")
    (begin
     (display " \\\n  ")
     (display (string-append str-name ".tci"))))
  (for-each
   (lambda (l-s-dep)
     (if (and (list? l-s-dep) (not-null? l-s-dep) (for-all symbol? l-s-dep))
       (begin
        (display " \\\n  ")
        (display (string-append (symbol->string (last l-s-dep)) ".tci")))
       (begin
        (display "warning: invalid module " (standard-error-port))
        (display l-s-dep (standard-error-port))
        (newline (standard-error-port)))))
   l-sx-deps)
  (newline)
  (newline))

(define (get-filename-without-ext filename)
  (let ((index (string-index filename #\.)))
    (if (eq? index #f)
      filename
      (string-take filename index))))

(define (parse-library str-lib)
  (map string->symbol (split-string str-lib #\space)))

(define (main args)
  (if (eqv? (length args) 3)
    (let* ((str-source (cadr args))
           (str-lib (caddr args))
           (l-s-lib (parse-library str-lib))
           (str-name (get-filename-without-ext str-source))
           (ip (open-input-file str-source))
           (l-sx (read-file ip)))
      (close-input-port ip)
      (if (and (list? l-sx)
               (eqv? (length l-sx) 1)
               (list? (car l-sx))
               (>= (length (car l-sx)) 2)
               (memq (caar l-sx)
                     '(define-interface
                        define-body
                        define-proper-program
                        define-script)))
        (begin
         (let ((str-ext
                (case (caar l-sx)
                  ((define-interface) "tci")
                  ((define-body) "tcb")
                  ((define-proper-program) "tcp")
                  ((define-script) "tcs")
                  (else
                   (display "Internal error.\n" (standard-error-port))
                   (exit 1))))
               (l-deps (process-all-expressions (cddar l-sx) l-s-lib)))
           (print-output-deps str-name str-ext l-deps)
           (exit 0)))
        (begin
         (display "Unrecognized translation unit syntax.\n"
                  (standard-error-port))
         (exit 1))))
    (begin
     (display "Invalid number of arguments.\n" (standard-error-port))
     (exit 1))))

