
SRCDIR = ../../src/theme-d-in-theme-d
VPATH = $(SRCDIR)

MODULE_PATH = ..:

ifdef USE_SPLIT
TDC = run-split-theme-d-program ../../build1/theme-d-in-theme-d/theme-d-compile-b.build
TDL = run-split-theme-d-program ../../build1/theme-d-in-theme-d/theme-d-link-b.build
else
TDC = run-theme-d-program ../../build1/theme-d-in-theme-d/theme-d-compile-b.go
TDL = run-theme-d-program ../../build1/theme-d-in-theme-d/theme-d-link-b.go
endif

include $(SRCDIR)/bootstrap.mk
