
TDC ?= theme-d-compile
TDL ?= theme-d-link
MODULE_PATH ?= ..:

#RUNTIME_PRETTY_BACKTRACE := --runtime-pretty-backtrace
RUNTIME_PRETTY_BACKTRACE :=
#MODULE_DEBUG_OUTPUT := --module-debug-output
MODULE_DEBUG_OUTPUT :=

.PHONY : all clean modules split-compiler split-linker

PROGRAMS := theme-d-compile-b theme-d-link-b

COMMON_MODULES := \
  address-allocation \
  address-environment address-table binder builtins cloning \
  common \
  common-procedure-utilities \
  configuration constructors cycles debug entities \
  errors-common expression-rebinding \
  expression-translation expressions hash-tables instances \
  translator-keywords \
  param-cache param-cache-opt parametrized-definitions \
  parametrized-instances pcode-common representation \
  special-procedures symbol-table target-object-printing \
  time translation-common type-system type-translation var-names \
  platform-specific statprof-work parameters

COMPILER_MODULES := \
  compilation-errors compilation-utilities compiler-core-def \
  compilation1 compilation2 compile-unit compiler-constructors \
  fields \
  interface-pcode-reading \
  letrec-compilation letrec-env \
  macro-base macro-base2 macro-config macro-language macros \
  phase2-compilation primitive-expansion

ALL_COMPILER_MODULES := $(COMMON_MODULES) $(COMPILER_MODULES)

LINKER_MODULES := \
  linker-core-def linker-errors implementation-pcode-reading \
  link-program link-program-split linker-instantiation \
  scheme0-target-compilation \
  scheme-target-common scheme-target-compilation scheme-target-objects \
  stripping target-compilation-common tree-il-target-compilation

ALL_LINKER_MODULES := $(COMMON_MODULES) $(LINKER_MODULES)

ALL_MODULES := $(COMMON_MODULES) $(COMPILER_MODULES) $(LINKER_MODULES)

all : theme-d-compile-b.go theme-d-link-b.go

modules : \
  $(patsubst %,%.tci,$(ALL_MODULES)) $(patsubst %,%.tcb,$(ALL_MODULES)) \
  theme-d-compile-b.tcp theme-d-link-b.tcp

clean :
	-rm -f *.tci
	-rm -f *.tcb
	-rm -f *.tcp
	-rm -f *.go
	-rm -r theme-d-compile-b.build
	-rm -r theme-d-link-b.build

theme-d-compile-b.go : theme-d-compile-b.tcp \
  $(patsubst %,%.tci,$(ALL_COMPILER_MODULES)) \
  $(patsubst %,%.tcb,$(ALL_COMPILER_MODULES))
	$(TDL) -m $(MODULE_PATH) --backtrace -l 2 \
	--duplicates="merge-generics replace warn last" \
	$(EXTRA_LINK_OPTIONS) \
	$(MODULE_DEBUG_OUTPUT) $(RUNTIME_PRETTY_BACKTRACE) \
	-x "(srfi srfi-19)" -x "(theme-d util time-wrapper)" \
	-x "(theme-d util statprof-work2)" $<

theme-d-compile-b.build : theme-d-compile-b.tcp \
  $(patsubst %,%.tci,$(ALL_COMPILER_MODULES)) \
  $(patsubst %,%.tcb,$(ALL_COMPILER_MODULES))
	-rm -r theme-d-compile-b.build
	$(TDL) -m $(MODULE_PATH) --split --backtrace -l 2 \
	--duplicates="merge-generics replace warn last" \
	$(EXTRA_LINK_OPTIONS) \
	$(MODULE_DEBUG_OUTPUT) $(RUNTIME_PRETTY_BACKTRACE) \
	-x "(srfi srfi-19)" -x "(theme-d util time-wrapper)" \
	-x "(theme-d util statprof-work2)" $<

theme-d-link-b.go : theme-d-link-b.tcp \
  $(patsubst %,%.tci,$(ALL_LINKER_MODULES)) \
  $(patsubst %,%.tcb,$(ALL_LINKER_MODULES))
	$(TDL) -m $(MODULE_PATH) --backtrace -l 2 \
	--duplicates="merge-generics replace warn last" \
	$(EXTRA_LINK_OPTIONS) \
	$(MODULE_DEBUG_OUTPUT) $(RUNTIME_PRETTY_BACKTRACE) \
	-x "(srfi srfi-19)" -x "(theme-d util time-wrapper)" \
	-x "(theme-d util statprof-work2)" $<

theme-d-link-b.build : theme-d-link-b.tcp \
  $(patsubst %,%.tci,$(ALL_LINKER_MODULES)) \
  $(patsubst %,%.tcb,$(ALL_LINKER_MODULES))
	-rm -r theme-d-link-b.build
	$(TDL) -m $(MODULE_PATH) --split --backtrace -l 2 \
	--duplicates="merge-generics replace warn last" \
	$(EXTRA_LINK_OPTIONS) \
	$(MODULE_DEBUG_OUTPUT) $(RUNTIME_PRETTY_BACKTRACE) \
	-x "(srfi srfi-19)" -x "(theme-d util time-wrapper)" \
	-x "(theme-d util statprof-work2)" $<

%.tci : %.thi
	$(TDC) -m $(MODULE_PATH) $(EXTRA_COMP_OPTIONS) -o $@ $<

%.tcb : %.thb
	$(TDC) -m $(MODULE_PATH) $(EXTRA_COMP_OPTIONS) -o $@ $<

%.tcp : %.thp
	$(TDC) -m $(MODULE_PATH) $(EXTRA_COMP_OPTIONS) -o $@ $<

include ../../src/theme-d-in-theme-d/deps.mk
