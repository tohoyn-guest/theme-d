
;; Copyright (C) 2018 Tommi Höynälänmaa

(define-body (theme-d-in-theme-d cycles)
  
  (import (standard-library list-utilities)
          (theme-d-in-theme-d type-system)
          (theme-d-in-theme-d expressions)
          (theme-d-in-theme-d cloning)
          (theme-d-in-theme-d common)
          (theme-d-in-theme-d pcode-common))
  
  (import (theme-d-in-theme-d debug))
  
  (prelink-body (theme-d-in-theme-d representation))
  
  (define-mutable gl-i-cycle-counter <integer> 0)
  
  (define-simple-proc make-cycle-hash-table-with-size
      (((i-size <integer>)) <cycle-hash-table> pure)
    ((param-proc-instance make-objectq-hash-table-ws <target-object>)
     i-size))
  
  (define gl-l-tc-cycle-fields
    (list
     (make-field 'address gl-tc-object 'public 'hidden #f null)
     (make-field 'to-contents gl-tc-object 'public 'hidden #f null)))
  
  (define gl-tc-cycle
    (make-builtin-target-class '<cycle> gl-tc-object gl-l-tc-cycle-fields
                               #f #t #f 'public #t 'public #t #t))
  
  (define is-t-cycle? (make-t-predicate0 gl-tc-cycle))
  
  (define-simple-proc make-cycle
      (((address (:maybe <address>)) (to <target-object>)) <target-object> pure)
    (create <target-object>
      gl-tc-cycle
      #f
      #f
      null
      #f
      #f
      (list
       (mcons1 'address address)
       (mcons1 'to-contents to))
      null
      null))
  
  (define-simple-proc is-tc-pair1?
      (((to <target-object>)) <boolean> pure)
    ;; We don't need atomic objects here.
    (equal-objects? (get-entity-type to) gl-tc-pair))
  
  (define-simple-proc detect-cycles-target-object
      (((binder <binder>) (to <target-object>) (ht-cycles <cycle-hash-table>)
                          (l-visited <list>))
       <none> nonpure)
    (cond
     ((equal-objects? to gl-tc-class) null)
     ((is-tc-pair1? to)
      (d2wl 'cycles "detect-cycles-target-object/pair-class")
      (let ((l-new-visited (cons to l-visited)))
        (detect-cycles binder (tt-car to) ht-cycles l-new-visited)
        (detect-cycles binder (tt-cdr to) ht-cycles l-new-visited)))
     ((and (not (is-t-primitive-object? to))
           (is-known-target-object? to)
           ;; We don't need atomic objects here.
           (is-tc-pair1? (get-entity-type to)))
      (let ((l-new-visited (cons to l-visited)))
        (detect-cycles binder (target-field-ref to 'first) ht-cycles
                       l-new-visited)
        (detect-cycles binder (target-field-ref to 'second) ht-cycles
                       l-new-visited)))
     ((is-t-param-class-instance? to)
      
      ;; TBR
      (d2wi 'debug-cycles3 "param class instance ")
      (d2wl 'debug-cycles3 (debug-get-string to))
      
      (let* ((l-new-visited (cons to l-visited))
             (det (lambda (((x2 <object>)) <none> nonpure)
                    (detect-cycles binder x2 ht-cycles l-new-visited))))
        (det (target-field-ref to 'cl-superclass))
        
        ;; TBR
        (d2wli 'debug-cycles3 "param class instance/1")
        (d2wli 'debug-cycles3
               (length
                (cast <target-object-list>
                      (target-field-ref to 'l-all-fields))))
        
        (for-each1 (lambda (((to-fld <target-object>)) <none> nonpure)
                     (det (target-field-ref to-fld 'type)))
                   (cast <target-object-list>
                         (target-field-ref to 'l-all-fields)))
        
        ;; TBR
        (d2wli 'debug-cycles3 "param class instance/2")
        
        (for-each1 det
                   (cast <target-object-list>
                         (target-field-ref to 'l-tvar-values)))
        
        ;; TBR
        (d2wli 'debug-cycles3 "param class instance/3")))
     
     (else
      (let* ((l-subexprs (get-subexpressions to))
             (l-new-visited (cons to l-visited)))
        (d2wli 'detect-cycles "detect-cycles/subexprs-1")
        (d2wli 'detect-cycles (length l-subexprs))
        (detect-cycles binder (field-ref to 'tt-type) ht-cycles
                       l-new-visited)
        (d2wli 'detect-cycles "detect-cycles/subexprs-2")
        (let-mutable ((i-counter <integer> 0))
          (for-each1 (lambda (((x2 <object>)) <none> nonpure)
                       (d2wli 'detect-cycles "detect-cycles/subexprs-3")
                       (set! i-counter (+ i-counter 1))
                       (d2wli 'detect-cycles i-counter)
                       (detect-cycles binder x2 ht-cycles l-new-visited)
                       (d2wli 'detect-cycles "detect-cycles/subexprs-4"))
                     l-subexprs))))))
    
  (define-simple-proc detect-cycles-ngp
      (((binder <binder>) (ngp <target-object>)
                          (ht-cycles <cycle-hash-table>) (l-visited <list>))
       <none> nonpure)
    (assert (is-normal-gen-proc? ngp))
    (let ((l-method-types
           (map1 (lambda (((method <method-object>)) <target-object> pure)
                   ;; We don't need atomic objects here.
                   (get-entity-type (car method)))
                 (cast <method-object-list>
                       (target-field-ref ngp 'l-methods))))
          (det (lambda (((to <target-object>)) <none> nonpure)
                 (detect-cycles binder to ht-cycles l-visited))))
      ;; We don't need atomic objects here.
      (det (get-entity-type ngp))
      (for-each1 det l-method-types)))
  
  (define-simple-proc detect-cycles
      (((binder <binder>) (x <object>) (ht-cycles <cycle-hash-table>)
                          (l-visited <list>))
       <none> nonpure)
    
    (let ((i-old-indent gl-i-indent))
      (set! gl-i-indent (+ gl-i-indent 1))
      (set! gl-i-counter28 (+ gl-i-counter28 1))
      (d2wli 'detect-cycles "detect-cycles ENTER")
      (d2wli 'detect-cycles gl-i-counter28)
      (d2wli 'detect-cycles
             (if (is-instance? x <entity>)
                 (field-ref (cast <normal-class> (class-of x)) 'str-name)
                 "?"))
      (d2wli 'detect-cycles
             (match-type x
               ((ent <entity>)  (debug-get-string (get-entity-type ent)))
               (else "?")))
      (d2wli 'detect-cycles
             (match-type x
               ((ent <entity>)  (debug-get-string ent))
               (else "?")))
      
      ;; TBR
      ;; (d2wi 'debug-cycles3 "detect-cycles ")
      ;; (let ((cl (class-of x)))
      ;;   (match-type cl
      ;; 	((cl1 <normal-class>)
      ;; 	 (d2wc 'debug-cycles3 (field-ref cl1 'str-name)))
      ;; 	((:pair)
      ;; 	 (d2wc 'debug-cycles3 "(:pair ...)"))))
      ;; (d2wc 'debug-cycles3 " ")
      ;; (d2wl 'debug-cycles3
      ;; 	  (match-type x
      ;; 	    ((ent <entity>) (debug-get-string (get-entity-type ent)))
      ;; 	    (else "?")))
      ;; (d2wc 'debug-cycles3 " ")
      ;; (set! gl-i-counter30 (+ gl-i-counter30 1))
      ;; (d2wl 'debug-cycles3 gl-i-counter30)
      
      ;; TBR
      ;; (set! gl-i-cycle-counter (+ gl-i-cycle-counter 1))
      ;; (d2wi 'cycles "detect-cycles ")
      ;; (d2wc 'cycles gl-i-cycle-counter)
      ;; (let ((cl (class-of x)))
      ;;   (match-type cl
      ;; 	((cl1 <normal-class>) (d2wc 'cycles (field-ref cl1 'str-name)))
      ;; 	((p :pair) (d2wc 'cycles "/:pair/"))
      ;; 	((gp :gen-proc) (d2wc 'cycles "/:gen-proc/"))
      ;; 	(else (d2wc 'cycles "?"))))
      ;; (d2wc 'cycles " ")
      ;; (d2wc 'cycles x)
      ;; (d2wc 'cycles " ")
      ;; (if (is-instance? x <entity>)
      ;; 	(begin
      ;; 	  (d2wc 'cycles (debug-get-string x))
      ;; 	  (d2wc 'cycles " ")
      ;; 	  (d2wc 'cycles
      ;; 		(debug-get-string
      ;; 		 (get-entity-type (cast <entity> x)))))
      ;; 	(d2wc 'cycles "?"))
      ;; (d2-newline 'cycles)
      ;; (d2wi 'debug7 "hash: ")
      ;; (d2wc 'debug7 (hashq x 10000000))
      ;; (d2-newline 'debug7)
      
      ;; TBR
      ;; (d2wli 'debug14 "detect-cycles")
      ;; (d2wli 'debug14 (debug-get-string x))
      ;; (set! gl-i-counter30 (+ gl-i-counter30 1))
      ;; (d2wli 'debug14 gl-i-counter30)
      
      (cond
       ((hash-exists? ht-cycles x) null)
       ((member-objects? x l-visited)
        (let* ((proc-allocate (field-ref binder 'proc-allocate))
               (address (proc-allocate 'cycle-1 #f))
               (to (make-incomplete-object-with-address
                    address gl-tc-object #f))
               (to-cycle2 (make-cycle address to)))
          (d2wli 'detect-cycles "cycle detected")
          (hash-set! ht-cycles x to-cycle2)))
       ((member-objects? x gl-l-builtin-types) null)
       (else
        (match-type x
          ((<null>) null)
          ((p <pair>)
           (let ((l-new-visited (cons x l-visited)))
             (detect-cycles binder (car p) ht-cycles l-new-visited)
             (detect-cycles binder (cdr p) ht-cycles l-new-visited)))
          ((to <target-object>)
           (detect-cycles-target-object binder to ht-cycles l-visited))
          ((<normal-variable>) null)
          ((expr <proc-appl>)
           (let* ((l-new-visited (cons x l-visited))
                  (det (lambda (((x2 <object>)) <none> nonpure)
                         (detect-cycles binder x2 ht-cycles l-new-visited))))
             (d2wli 'detect-cycles "detect-cycles/proc-appl")
             (d2wli 'detect-cycles
                   (theme-compile-address0
                    (field-ref (field-ref expr 'ent-proc) 'address)))
             (d2wli 'detect-cycles "detect-cycles/proc-appl-1")
             (det (field-ref expr 'tt-type))
             (d2wli 'detect-cycles "detect-cycles/proc-appl-2")
             (det (field-ref expr 'ent-proc))
             (d2wli 'detect-cycles "detect-cycles/proc-appl-3")
             (match-type (field-ref expr 'l-arglist)
               ((ent-args <entity>)
                (det ent-args))
               ((l-ent-args <entity-list>)
                (for-each1 det l-ent-args)))
             (d2wli 'detect-cycles "detect-cycles/proc-appl-4")
             (for-each1 det (field-ref expr 'l-params))
             (d2wli 'detect-cycles "detect-cycles/proc-appl-5")
             (for-each1 det (field-ref expr 'l-static-arg-types))
             (d2wli 'detect-cycles "detect-cycles/proc-appl-6")))
          ((expr <ngp-declaration>)
           (let ((ngp (field-ref expr 'ngp))
                 (l-new-visited (cons expr l-visited)))
             (detect-cycles binder (field-ref expr 'tt-primary-method)
                            ht-cycles l-new-visited)
             (detect-cycles-ngp binder ngp ht-cycles l-new-visited)))
          ((expr <ngp-definition>)
           (let ((ngp (field-ref expr 'ngp))
                 (l-new-visited (cons expr l-visited)))
             (detect-cycles-ngp binder ngp ht-cycles l-new-visited)))
          ((expr <procedure-expression>)
           (let* ((l-new-visited (cons x l-visited))
                  (det (lambda (((x2 <object>)) <none> nonpure)
                         (detect-cycles binder x2 ht-cycles l-new-visited)))
                  ;; Variables can't be atomic objects.
                  (l-arg-types (map1 get-entity-type
                                     (field-ref expr 'l-arg-variables))))
             (det (field-ref expr 'tt-type))
             (for-each1 det (field-ref expr 'l-arg-descs))
             (for-each1 det l-arg-types)
             (det (field-ref expr 'tt-result))
             (det (field-ref expr 'ent-body))))
          ((ent <entity>)
           (if (equal-objects? (class-of ent) <variable-definition>)
               ;; Subclasses of <variable-definition> are not handled here.
               (let ((expr (cast <variable-definition> ent))
                     (l-new-visited (cons ent l-visited)))
                 ;; The following call is probably erroneous. The type of a
                 ;; variable definition is gl-tt-none.
                 ;; Maybe we should have ent instead of expr here.
                 (detect-cycles binder (get-entity-type expr) ht-cycles
                                l-new-visited)
                 (detect-cycles binder (field-ref expr 'tt-decl) ht-cycles
                                l-new-visited)
                 (detect-cycles binder (field-ref expr 'ent-value-expr)
                                ht-cycles
                                l-new-visited))
               (let* ((l-subexprs (get-subexpressions ent))
                      (l-new-visited (cons ent l-visited)))
                 (d2wli 'detect-cycles "detect-cycles/subexprs-1")
                 (d2wli 'detect-cycles (length l-subexprs))
                 (detect-cycles binder (field-ref ent 'tt-type) ht-cycles
                                l-new-visited)
                 (d2wli 'detect-cycles "detect-cycles/subexprs-2")
                 (let-mutable ((i-counter <integer> 0))
                   (for-each1 (lambda (((x2 <object>)) <none> nonpure)
                                (d2wli 'detect-cycles "detect-cycles/subexprs-3")
                                (set! i-counter (+ i-counter 1))
                                (d2wli 'detect-cycles i-counter)
                                (detect-cycles binder x2 ht-cycles l-new-visited)
                                (d2wli 'detect-cycles "detect-cycles/subexprs-4"))
                              l-subexprs)))))
           (else
           (raise-simple 'detect-cycles:invalid-argument)))))
      (d2wli 'detect-cycles "detect-cycles EXIT")
      (set! gl-i-indent i-old-indent)))
  
  (define-simple-proc make-cycle-object
      (((to <target-object>)) <target-object> pure)
    ;; The class of an object is never a unit type.
    (make-incomplete-object (get-entity-type to) #f))
  
  (define-simple-proc update-cycle-object!
      (((to-sgt <target-object>) (to-contents <target-object>)) <none> nonpure)
    (set-object1! to-sgt to-contents)
    (field-set! to-sgt 'address (field-ref to-contents 'address))))
