
;; Copyright (C) 2017 Tommi Höynälänmaa

(define-interface (theme-d-in-theme-d common)
  
  (import (standard-library core)
          (standard-library text-file-io)
          (standard-library object-string-conversion)
          (standard-library mutable-pair))
  
  (reexport <source-expr>)
  (reexport <source-atom>)
  (reexport <source-expr-list>)
  
  (reexport string->integer)
  
  (define <sx-list> (:uniform-list <source-expr>))
  
  (define <nonempty-sx-list> (:nonempty-uniform-list <source-expr>))
  
  (define <source-expr1> <object>)
  
  (define <sx-list1> <list>)
  
  (define <nonempty-sx-list1> <nonempty-list>)

  (declare get-meas-count (:procedure () <integer> pure))

  (declare get-meas-state (:procedure () <boolean> pure))

  (declare set-meas-state! (:procedure (<boolean>) <none> nonpure))

  (declare raise1 (:procedure (<rte-exception-kind> <rte-exception-info>)
                              <none> (pure never-returns)))
  
  (declare raise2 (:procedure (<rte-exception-kind>
                               (rest (:pair <symbol> <object>)))
                              <none> (pure never-returns)))

  (declare map-cons
    (:param-proc (%type1 %type2)
                 ((:uniform-list %type1) (:uniform-list %type2))
                 (:uniform-list (:pair %type1 %type2))
                 pure))
  
  (declare list2
    (:param-proc (%type1 %type2)
                 (%type1 %type2)
                 (:tuple %type1 %type2)
                 pure))
  
  (declare-param-method apply-append (%types)
    ((type-loop %type %types (:uniform-list %type)))
    (:uniform-list (:union (splice %types)))
    pure)
  
  ;; A module name is either a nonempty list of symbols or symbol 'builtins.
  ;; The latter is allowed only in Theme-D internal definitions.
  ;; If a user module name is a single symbol it is converted to a
  ;; list with a single element.
  (define <module-name> (:union (:nonempty-uniform-list <symbol>) <symbol>))
  
  (define <general-module-name>
    (:union <module-name> <boolean> <null>))
  
  (declare is-module-name? (:simple-proc (<object>) <boolean> pure))
  
  (declare module-name=? (:procedure (<module-name> <module-name>)
                                     <boolean> pure))
  
  (declare general-module-name=?
    (:simple-proc (<general-module-name> <general-module-name>)
                  <boolean> pure))
  
  (declare get-file-name-from-list (:simple-proc
                                    (<string> (:uniform-list <symbol>))
                                    <string>
                                    pure))
  
  (declare get-file-name-from-list-and-ext
    (:simple-proc
     (<string> (:uniform-list <symbol>) <string>)
     <string>
     pure))
  
  (declare get-actual-module-name
    (:simple-proc ((:union <module-name> <symbol>)) <module-name> pure))
  
  (declare search-file
    (:simple-proc ((:uniform-list <string>) <module-name> <string>)
                  (:alt-maybe <string>) pure))
  
  (declare get-pcode-suffix (:simple-proc (<symbol>) <string> pure))
  
  (declare get-source-code-suffix (:simple-proc (<symbol>) <string> pure))
  
  (declare atom? (:simple-proc (<object>) <boolean> pure))
  
  (declare get-filename-extension (:simple-proc (<string>) <string> pure))
  
  (declare get-filename-without-ext (:simple-proc (<string>) <string> pure))
  
  (declare get-filename-without-path (:simple-proc (<string>) <string> pure))
  
  (declare parse-search-path (:simple-proc (<string> <string>)
                                           (:uniform-list <string>)
                                           pure))
  
  (declare get-integer-sequence (:simple-proc (<integer> <integer>)
                                              (:uniform-list <integer>)
                                              pure))
  
  (define-param-logical-type :equality-predicate (%type)
    (:procedure (%type %type) <boolean> pure))
  
  (define-param-class :assoc-list-object
    (parameters %key %value)
    (inheritance-access hidden)
    (fields
     (al-contents (:alist %key %value) public public)
     (pc-eq (:equality-predicate %key) public public)))
  
  (declare make-alo
    (:param-proc (%key %value)
                 ((:alist %key %value) (:equality-predicate %key))
                 (:assoc-list-object %key %value)
                 pure))
  
  (declare alo-fetch
    (:param-proc (%key %value)
                 ((:assoc-list-object %key %value)
                  %key)
                 (:maybe (:pair %key %value))
                 pure))
  
  (declare alo-add-binding-weak!
    (:param-proc (%key %value)
                 ((:assoc-list-object %key %value)
                  %key
                  %value)
                 <none>
                 nonpure))
  
  (declare alo-add-binding-pair-weak!
    (:param-proc (%key %value)
                 ((:assoc-list-object %key %value)
                  (:pair %key %value))
                 <none>
                 nonpure))
  
  (declare alo-delete!
    (:param-proc (%key %value)
                 ((:assoc-list-object %key %value)
                  %key)
                 <none>
                 nonpure))
  
  (declare alo-get-contents
    (:param-proc (%key %value)
                 ((:assoc-list-object %key %value))
                 (:alist %key %value)
                 pure))
  
  (declare alo-set-contents!
    (:param-proc (%key %value)
                 ((:assoc-list-object %key %value)
                  (:alist %key %value))
                 <none>
                 nonpure))
  
  (declare equal-pairs? (:simple-proc (<pair> <pair>) <boolean> pure))
  
  (declare mutable-vector-map-nonpure1
    (:param-proc (%type1 %type2)
                 ((:procedure (%type1) %type2 nonpure)
                  (:mutable-vector %type1))
                 (:mutable-vector %type2)
                 nonpure))
  
  (declare mutable-vector-count
    (:param-proc (%type)
                 ((:procedure (%type) <boolean> pure)
                  (:mutable-vector %type))
                 <integer> pure))
  
  (declare list->mutable-vector
    (:param-proc (%type)
                 ((:uniform-list %type))
                 (:mutable-vector %type)
                 pure))
  
  (declare lset<=
    (:param-proc (%type)
                 ((:uniform-list %type) (:uniform-list %type))
                 <boolean> pure))
  
  (declare get-indexed-arg-name
    (:simple-proc (<integer> <string>) <symbol> pure))
  
  (declare get-indexed-arg-names
    (:simple-proc (<integer> <string>) (:uniform-list <symbol>) pure))
    
  (declare mcons1 (:simple-proc (<symbol> <object>) (:mpair <symbol> <object>)
                                pure))
  
  (declare general-search
    (:param-proc (%type1 %type2)
                 (%type1
                  (:uniform-list %type2)
                  (:procedure (%type1 %type2) <boolean> pure))
                 <integer>
                 pure))
  
  (declare find-nonpure
    (:param-proc (%type %default)
                 ((:procedure (%type) <boolean> nonpure)
                  (:uniform-list %type)
                  %default)
                 (:union %type %default)
                 nonpure))
  
  (declare delete-nonpure
    (:param-proc (%type)
                 (%type
                  (:uniform-list %type)
                  (:procedure (%type %type) <boolean> nonpure))
                 (:uniform-list %type)
                 nonpure))
  
  (declare delete-object
    (:param-proc (%type)
                 (%type
                  (:uniform-list %type))
                 (:uniform-list %type)
                 pure))
  
  (declare member-general-nonpure?
    (:param-proc (%type)
                 (%type
                  (:uniform-list %type)
                  (:procedure (%type %type) <boolean> nonpure))
                 <boolean>
                 nonpure))
  
  ;; The result type must have <boolean> as its subtype.
  (declare-method fold2-with-break 
    (:param-proc (%type1 %type2 %result)
                 ((:procedure (%type1 %type2 %result) %result pure)
                  %result
                  (:uniform-list %type1)
                  (:uniform-list %type2))
                 %result pure))
  
  (declare write-sx
    (:simple-proc (<output-port> <source-expr>) <none> nonpure))
  
  (declare write-line-sx
    (:simple-proc (<output-port> <source-expr>) <none> nonpure))
  
  (declare display-sx
    (:simple-proc (<output-port> <source-expr>) <none> nonpure))
  
  ;; We don't consider logging as a side effect.
  (declare backtrace
    (:simple-proc () <none> pure))
  
  (define-syntax map-tuple-pick
    (syntax-rules ()
      ((_ l type index)
       (map1 (param-lambda (%type) (((l %type))
                                    type pure)
               (tuple-ref l index))
             l))))
  
  (define-syntax iterate-dynamic-list
    (syntax-rules ()
      ((_ (iter-var l) body1 ...)
       (letrec ((loop
                 (:simple-proc (<object>) <none> nonpure)
                 (lambda (((l1 <object>)) <none> nonpure)
                   (if (not-null? l1)
                       (let ((iter-var (d-car l1)))
                         body1 ...
                         (loop (d-cdr l1)))))))
         (loop l)))))

  (define-syntax iterate-3-lists
    (syntax-rules ()
      ((_ ((iter-var1 elem-type1 l1)
           (iter-var2 elem-type2 l2)
           (iter-var3 elem-type3 l3))
          body1 ...)
       (letrec ((loop
                 (:simple-proc ((:uniform-list elem-type1)
                                (:uniform-list elem-type2)
                                (:uniform-list elem-type3)) <none> nonpure)
                 (lambda (((l1b (:uniform-list elem-type1))
                           (l2b (:uniform-list elem-type2))
                           (l3b (:uniform-list elem-type3)))
                          <none> nonpure)
                   (match-type l1b
                     ((<null>) null)
                     ((l1c (:nonempty-uniform-list elem-type1))
                      (match-type l2b
                        ((<null>) null)
                        ((l2c (:nonempty-uniform-list elem-type2))
                         (match-type l3b
                           ((<null>) null)
                           ((l3c (:nonempty-uniform-list elem-type3))
                            (let ((iter-var1 (car l1c))
                                  (iter-var2 (car l2c))
                                  (iter-var3 (car l3c)))
                              body1 ...
                              (loop (cdr l1c) (cdr l2c) (cdr l3c))))))))))))
         (loop l1 l2 l3))))))



