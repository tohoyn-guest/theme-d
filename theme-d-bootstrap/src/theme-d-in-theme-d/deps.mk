test3.tcp : \
  test2.tci

theme-d-compile-b.tcp : \
  statprof-work.tci \
  parameters.tci \
  compile-unit.tci \
  common.tci \
  debug.tci \
  target-object-printing.tci \
  configuration.tci

theme-d-link-b.tcp : \
  link-program.tci \
  link-program-split.tci \
  target-object-printing.tci \
  configuration.tci \
  parameters.tci \
  common.tci \
  statprof-work.tci \
  debug.tci

address-allocation.tci :

address-environment.tci : \
  address-table.tci \
  entities.tci \
  symbol-table.tci \
  hash-tables.tci \
  binder.tci

address-table.tci : \
  hash-tables.tci \
  entities.tci \
  translator-keywords.tci

binder.tci : \
  entities.tci \
  expressions.tci \
  param-cache.tci \
  param-cache-opt.tci \
  hash-tables.tci

builtins.tci : \
  symbol-table.tci

c1.tci :

cloning.tci : \
  entities.tci \
  binder.tci \
  expressions.tci

common-procedure-utilities.tci : \
  binder.tci \
  entities.tci \
  expressions.tci \
  address-environment.tci \
  instances.tci \
  parametrized-definitions.tci

common.tci :

compilation-errors.tci : \
  compiler-core-def.tci

compilation-utilities.tci : \
  common.tci \
  entities.tci \
  expressions.tci \
  compiler-core-def.tci \
  symbol-table.tci

compilation1.tci : \
  entities.tci \
  compiler-core-def.tci \
  common.tci \
  symbol-table.tci \
  expressions.tci

compilation2.tci : \
  entities.tci \
  common.tci \
  compiler-core-def.tci \
  symbol-table.tci

compile-unit.tci :

compiler-constructors.tci : \
  compiler-core-def.tci \
  symbol-table.tci \
  common.tci \
  entities.tci \
  expressions.tci

compiler-core-def.tci : \
  common.tci \
  translator-keywords.tci \
  entities.tci \
  address-environment.tci \
  symbol-table.tci \
  binder.tci \
  hash-tables.tci \
  letrec-env.tci

configuration.tci :

constructors.tci : \
  entities.tci \
  binder.tci \
  expressions.tci

cycles.tci : \
  hash-tables.tci \
  entities.tci \
  binder.tci \
  representation.tci

debug.tci :

entities.tci : \
  common.tci

errors-common.tci : \
  entities.tci

expression-rebinding.tci : \
  entities.tci \
  expressions.tci \
  binder.tci

expression-translation.tci : \
  entities.tci \
  expressions.tci \
  binder.tci

expressions.tci : \
  common.tci \
  entities.tci

fields.tci : \
  compiler-core-def.tci \
  entities.tci \
  expressions.tci

hash-tables.tci : \
  entities.tci \
  expressions.tci \
  common.tci

implementation-pcode-reading.tci : \
  linker-core-def.tci \
  common.tci \
  entities.tci \
  address-environment.tci

instances.tci : \
  entities.tci

interface-pcode-reading.tci : \
  common.tci \
  compiler-core-def.tci \
  entities.tci

letrec-compilation.tci : \
  letrec-env.tci \
  compiler-core-def.tci \
  common.tci \
  entities.tci \
  expressions.tci \
  symbol-table.tci

letrec-env.tci : \
  entities.tci

link-program-split.tci : \
  linker-core-def.tci \
  common.tci

link-program.tci : \
  linker-core-def.tci \
  common.tci \
  symbol-table.tci \
  target-compilation-common.tci

linker-core-def.tci : \
  entities.tci \
  expressions.tci \
  hash-tables.tci \
  param-cache.tci \
  cycles.tci \
  binder.tci \
  common.tci \
  address-environment.tci \
  instances.tci

linker-errors.tci : \
  linker-core-def.tci

linker-instantiation.tci : \
  entities.tci \
  instances.tci \
  linker-core-def.tci

macro-base.tci : \
  common.tci \
  hash-tables.tci \
  entities.tci \
  platform-specific.tci

macro-base2.tci : \
  common.tci \
  macro-base.tci

macro-config.tci :

macro-language.tci :

macros.tci : \
  macro-base.tci \
  macro-base2.tci

param-cache-opt.tci : \
  entities.tci

param-cache.tci : \
  entities.tci

parameters.tci :

parametrized-definitions.tci : \
  entities.tci \
  binder.tci \
  instances.tci

parametrized-instances.tci : \
  common.tci \
  entities.tci \
  binder.tci

pcode-common.tci : \
  common.tci \
  entities.tci

phase2-compilation.tci : \
  compiler-core-def.tci \
  symbol-table.tci \
  entities.tci

platform-specific.tci :

primitive-expansion.tci : \
  macro-base.tci

representation.tci : \
  common.tci \
  entities.tci \
  expressions.tci

s1.tci : \
  linker-core-def.tci \
  entities.tci \
  target-compilation-common.tci

scheme-target-common.tci : \
  target-compilation-common.tci \
  entities.tci \
  expressions.tci \
  linker-core-def.tci \
  instances.tci \
  linker-instantiation.tci

scheme-target-compilation.tci : \
  linker-core-def.tci \
  entities.tci \
  target-compilation-common.tci

scheme-target-objects.tci : \
  entities.tci \
  target-compilation-common.tci \
  linker-core-def.tci \
  expressions.tci

scheme0-target-compilation.tci : \
  linker-core-def.tci \
  entities.tci \
  target-compilation-common.tci \
  linker-instantiation.tci

special-procedures.tci : \
  entities.tci \
  expressions.tci \
  binder.tci

statprof-work.tci :

stripping.tci : \
  entities.tci \
  linker-core-def.tci \
  expressions.tci \
  hash-tables.tci

symbol-table.tci : \
  common.tci \
  hash-tables.tci \
  entities.tci

target-compilation-common.tci : \
  entities.tci \
  linker-core-def.tci \
  binder.tci \
  instances.tci \
  hash-tables.tci \
  expressions.tci

target-object-printing.tci :

test1.tci : \
  entities.tci \
  binder.tci \
  expressions.tci

test2.tci :

time.tci :

translation-common.tci : \
  common.tci \
  entities.tci \
  binder.tci \
  expressions.tci

translator-keywords.tci : \
  entities.tci \
  symbol-table.tci

tree-il-target-compilation.tci : \
  linker-core-def.tci \
  entities.tci \
  target-compilation-common.tci

type-system.tci : \
  entities.tci \
  binder.tci \
  expressions.tci

type-translation.tci : \
  entities.tci \
  binder.tci \
  instances.tci

var-names.tci : \
  hash-tables.tci

address-allocation.tcb : \
  address-allocation.tci

address-environment.tcb : \
  address-environment.tci \
  type-system.tci \
  representation.tci \
  common.tci

address-table.tcb : \
  address-table.tci \
  common.tci

binder.tcb : \
  binder.tci \
  cloning.tci

builtins.tcb : \
  builtins.tci \
  representation.tci \
  special-procedures.tci \
  entities.tci

cloning.tcb : \
  cloning.tci \
  common.tci \
  representation.tci \
  expressions.tci \
  cycles.tci \
  expression-translation.tci \
  type-translation.tci \
  translation-common.tci \
  common-procedure-utilities.tci \
  expression-rebinding.tci \
  type-system.tci \
  constructors.tci \
  parametrized-instances.tci \
  special-procedures.tci \
  hash-tables.tci \
  debug.tci

common-procedure-utilities.tcb : \
  common-procedure-utilities.tci \
  type-translation.tci \
  representation.tci \
  cloning.tci \
  type-system.tci \
  common.tci \
  debug.tci

common.tcb : \
  common.tci \
  parameters.tci

compilation-errors.tcb : \
  compilation-errors.tci \
  errors-common.tci \
  macros.tci \
  common.tci \
  hash-tables.tci \
  entities.tci \
  target-object-printing.tci \
  var-names.tci \
  representation.tci \
  platform-specific.tci

compilation-utilities.tcb : \
  compilation-utilities.tci \
  representation.tci \
  translator-keywords.tci \
  type-system.tci \
  translation-common.tci \
  hash-tables.tci \
  address-environment.tci \
  common.tci \
  pcode-common.tci \
  debug.tci

compilation1.tcb : \
  compilation1.tci \
  compilation2.tci \
  translator-keywords.tci \
  representation.tci \
  letrec-compilation.tci \
  compilation-utilities.tci \
  type-system.tci \
  constructors.tci \
  compiler-constructors.tci \
  type-translation.tci \
  translation-common.tci \
  common-procedure-utilities.tci \
  letrec-env.tci \
  cloning.tci \
  expression-translation.tci \
  special-procedures.tci \
  fields.tci \
  hash-tables.tci \
  pcode-common.tci \
  debug.tci

compilation2.tcb : \
  compilation2.tci \
  compilation1.tci \
  expressions.tci \
  representation.tci \
  type-system.tci \
  compilation-utilities.tci \
  constructors.tci \
  compiler-constructors.tci \
  type-translation.tci \
  expression-translation.tci \
  hash-tables.tci \
  common-procedure-utilities.tci \
  translator-keywords.tci \
  translation-common.tci \
  cloning.tci \
  parametrized-instances.tci \
  address-environment.tci \
  address-table.tci \
  interface-pcode-reading.tci \
  macros.tci \
  pcode-common.tci \
  common.tci \
  address-allocation.tci \
  binder.tci \
  parameters.tci \
  debug.tci \
  time.tci

compile-unit.tcb : \
  compile-unit.tci \
  compiler-core-def.tci \
  parameters.tci \
  entities.tci \
  address-allocation.tci \
  param-cache.tci \
  param-cache-opt.tci \
  symbol-table.tci \
  address-environment.tci \
  binder.tci \
  translator-keywords.tci \
  builtins.tci \
  hash-tables.tci \
  compilation2.tci \
  common.tci \
  pcode-common.tci \
  phase2-compilation.tci \
  representation.tci \
  macros.tci \
  compilation-errors.tci \
  macro-base.tci \
  cycles.tci \
  debug.tci

compiler-constructors.tcb : \
  compiler-constructors.tci \
  binder.tci \
  constructors.tci \
  common.tci \
  type-translation.tci \
  type-system.tci \
  representation.tci \
  compilation1.tci \
  compilation-utilities.tci \
  compiler-core-def.tci \
  common-procedure-utilities.tci \
  parametrized-instances.tci \
  parametrized-definitions.tci \
  cloning.tci \
  debug.tci

compiler-core-def.tcb : \
  compiler-core-def.tci \
  address-allocation.tci \
  debug.tci

configuration.tcb : \
  configuration.tci

constructors.tcb : \
  constructors.tci \
  type-translation.tci \
  representation.tci \
  type-system.tci \
  cloning.tci \
  common.tci \
  parametrized-definitions.tci \
  hash-tables.tci \
  debug.tci

cycles.tcb : \
  cycles.tci \
  type-system.tci \
  expressions.tci \
  cloning.tci \
  common.tci \
  pcode-common.tci \
  debug.tci

debug.tcb : \
  debug.tci \
  target-object-printing.tci

entities.tcb : \
  entities.tci \
  representation.tci

errors-common.tcb : \
  errors-common.tci \
  target-object-printing.tci \
  translator-keywords.tci \
  expressions.tci \
  var-names.tci \
  type-translation.tci \
  representation.tci

expression-rebinding.tcb : \
  expression-rebinding.tci \
  expression-translation.tci \
  representation.tci

expression-translation.tcb : \
  expression-translation.tci \
  translation-common.tci \
  common.tci \
  representation.tci \
  type-system.tci \
  type-translation.tci \
  common-procedure-utilities.tci \
  cloning.tci \
  parametrized-definitions.tci \
  special-procedures.tci \
  pcode-common.tci \
  target-object-printing.tci \
  debug.tci

expressions.tcb : \
  expressions.tci \
  representation.tci

fields.tcb : \
  fields.tci \
  common.tci \
  type-system.tci \
  special-procedures.tci \
  representation.tci \
  hash-tables.tci \
  debug.tci

hash-tables.tcb : \
  hash-tables.tci

implementation-pcode-reading.tcb : \
  implementation-pcode-reading.tci \
  binder.tci \
  representation.tci \
  expressions.tci \
  type-system.tci \
  type-translation.tci \
  translator-keywords.tci \
  parametrized-instances.tci \
  expression-translation.tci \
  common-procedure-utilities.tci \
  constructors.tci \
  cloning.tci \
  translation-common.tci \
  hash-tables.tci \
  errors-common.tci \
  pcode-common.tci \
  debug.tci

instances.tcb : \
  instances.tci

interface-pcode-reading.tcb : \
  interface-pcode-reading.tci \
  parameters.tci \
  entities.tci \
  address-environment.tci \
  symbol-table.tci \
  hash-tables.tci \
  macros.tci \
  type-translation.tci \
  type-system.tci \
  expressions.tci \
  constructors.tci \
  translator-keywords.tci \
  parametrized-instances.tci \
  representation.tci \
  expression-translation.tci \
  common-procedure-utilities.tci \
  compilation-utilities.tci \
  pcode-common.tci \
  common.tci \
  debug.tci

letrec-compilation.tcb : \
  letrec-compilation.tci \
  compilation1.tci \
  type-system.tci \
  representation.tci \
  binder.tci \
  common.tci

letrec-env.tcb : \
  letrec-env.tci

link-program-1.tcb : \
  link-program-1.tci \
  parameters.tci \
  debug.tci \
  symbol-table.tci \
  param-cache.tci \
  param-cache-opt.tci \
  hash-tables.tci \
  cycles.tci \
  implementation-pcode-reading.tci \
  target-compilation-common.tci \
  entities.tci \
  expressions.tci \
  binder.tci \
  address-environment.tci \
  scheme-target-common.tci \
  scheme0-target-compilation.tci \
  scheme-target-compilation.tci \
  tree-il-target-compilation.tci \
  translation-common.tci \
  special-procedures.tci \
  stripping.tci \
  linker-instantiation.tci \
  translator-keywords.tci \
  builtins.tci \
  configuration.tci \
  linker-errors.tci \
  time.tci

link-program-split-1.tcb : \
  link-program-split-1.tci \
  link-program.tci \
  stripping.tci \
  implementation-pcode-reading.tci \
  translation-common.tci \
  configuration.tci \
  tree-il-target-compilation.tci \
  entities.tci \
  target-compilation-common.tci \
  scheme-target-common.tci \
  address-environment.tci \
  hash-tables.tci \
  linker-instantiation.tci \
  parameters.tci \
  symbol-table.tci \
  translator-keywords.tci \
  builtins.tci \
  linker-errors.tci \
  debug.tci

link-program-split.tcb : \
  link-program-split.tci \
  link-program.tci \
  stripping.tci \
  implementation-pcode-reading.tci \
  translation-common.tci \
  configuration.tci \
  tree-il-target-compilation.tci \
  entities.tci \
  target-compilation-common.tci \
  scheme-target-common.tci \
  address-environment.tci \
  hash-tables.tci \
  linker-instantiation.tci \
  parameters.tci \
  symbol-table.tci \
  translator-keywords.tci \
  builtins.tci \
  linker-errors.tci \
  debug.tci

link-program.tcb : \
  link-program.tci \
  parameters.tci \
  debug.tci \
  symbol-table.tci \
  param-cache.tci \
  param-cache-opt.tci \
  hash-tables.tci \
  cycles.tci \
  implementation-pcode-reading.tci \
  target-compilation-common.tci \
  entities.tci \
  expressions.tci \
  binder.tci \
  address-environment.tci \
  scheme-target-common.tci \
  scheme0-target-compilation.tci \
  scheme-target-compilation.tci \
  tree-il-target-compilation.tci \
  translation-common.tci \
  special-procedures.tci \
  stripping.tci \
  linker-instantiation.tci \
  translator-keywords.tci \
  builtins.tci \
  configuration.tci \
  linker-errors.tci \
  time.tci

linker-core-def.tcb : \
  linker-core-def.tci \
  address-allocation.tci \
  debug.tci

linker-errors.tcb : \
  linker-errors.tci \
  errors-common.tci \
  target-object-printing.tci \
  representation.tci \
  entities.tci \
  expressions.tci \
  common.tci \
  instances.tci

linker-instantiation.tcb : \
  linker-instantiation.tci \
  parametrized-definitions.tci \
  cloning.tci \
  binder.tci \
  representation.tci \
  expressions.tci \
  stripping.tci \
  hash-tables.tci \
  type-system.tci \
  debug.tci

macro-base.tcb : \
  macro-base.tci \
  macro-config.tci \
  macro-language.tci \
  primitive-expansion.tci \
  macro-base2.tci \
  var-names.tci \
  translator-keywords.tci \
  debug.tci

macro-base2.tcb : \
  macro-base2.tci \
  macro-language.tci \
  primitive-expansion.tci \
  entities.tci \
  translator-keywords.tci \
  hash-tables.tci \
  debug.tci

macro-config.tcb : \
  macro-config.tci

macro-language.tcb : \
  macro-language.tci \
  macro-base.tci \
  macro-base2.tci \
  common.tci \
  hash-tables.tci \
  debug.tci

macros.tcb : \
  macros.tci

param-cache-opt.tcb : \
  param-cache-opt.tci \
  common.tci \
  representation.tci

param-cache.tcb : \
  param-cache.tci \
  common.tci \
  representation.tci

parameters.tcb : \
  parameters.tci

parametrized-definitions.tcb : \
  parametrized-definitions.tci \
  common.tci \
  representation.tci \
  expressions.tci \
  type-translation.tci \
  expression-translation.tci \
  cloning.tci \
  type-system.tci \
  cycles.tci \
  common-procedure-utilities.tci \
  param-cache.tci \
  param-cache-opt.tci \
  special-procedures.tci \
  parametrized-instances.tci \
  translation-common.tci \
  hash-tables.tci \
  translator-keywords.tci \
  constructors.tci \
  debug.tci \
  pcode-common.tci \
  target-object-printing.tci

parametrized-instances.tcb : \
  parametrized-instances.tci \
  parametrized-definitions.tci \
  translator-keywords.tci \
  translation-common.tci \
  representation.tci \
  constructors.tci \
  type-system.tci \
  common-procedure-utilities.tci \
  expressions.tci \
  param-cache.tci \
  type-translation.tci \
  debug.tci

pcode-common.tcb : \
  pcode-common.tci \
  representation.tci

phase2-compilation.tcb : \
  phase2-compilation.tci \
  common.tci \
  pcode-common.tci \
  representation.tci \
  type-system.tci \
  expressions.tci \
  cycles.tci \
  hash-tables.tci \
  time.tci \
  debug.tci

platform-specific-guile.tcb : \
  platform-specific-guile.tci

platform-specific-racket.tcb : \
  platform-specific-racket.tci

platform-specific.tcb : \
  platform-specific.tci

primitive-expansion.tcb : \
  primitive-expansion.tci \
  macro-base2.tci \
  common.tci \
  translator-keywords.tci \
  debug.tci

representation.tcb : \
  representation.tci \
  translator-keywords.tci \
  debug.tci

s1.tcb : \
  s1.tci \
  scheme-target-common.tci \
  scheme-target-objects.tci \
  expressions.tci \
  type-system.tci \
  representation.tci \
  hash-tables.tci \
  common.tci \
  stripping.tci \
  special-procedures.tci \
  param-cache.tci \
  debug.tci

scheme-target-common.tcb : \
  scheme-target-common.tci \
  target-compilation-common.tci \
  scheme-target-objects.tci \
  type-translation.tci \
  type-system.tci \
  common.tci \
  stripping.tci \
  pcode-common.tci \
  representation.tci \
  cycles.tci \
  parametrized-definitions.tci \
  hash-tables.tci \
  debug.tci

scheme-target-compilation.tcb : \
  scheme-target-compilation.tci \
  scheme-target-common.tci \
  scheme-target-objects.tci \
  expressions.tci \
  type-system.tci \
  representation.tci \
  hash-tables.tci \
  common.tci \
  stripping.tci \
  special-procedures.tci \
  param-cache.tci \
  debug.tci

scheme-target-objects.tcb : \
  scheme-target-objects.tci \
  scheme-target-common.tci \
  common.tci \
  representation.tci \
  cloning.tci \
  type-system.tci \
  cycles.tci \
  hash-tables.tci \
  pcode-common.tci \
  debug.tci

scheme0-target-compilation-1.tcb : \
  scheme0-target-compilation-1.tci \
  common.tci \
  scheme-target-common.tci \
  scheme-target-objects.tci \
  representation.tci \
  expressions.tci \
  common-procedure-utilities.tci \
  stripping.tci \
  type-system.tci \
  special-procedures.tci \
  param-cache.tci \
  target-object-printing.tci \
  debug.tci

scheme0-target-compilation.tcb : \
  scheme0-target-compilation.tci \
  scheme-target-common.tci \
  scheme-target-objects.tci \
  expressions.tci \
  type-system.tci \
  representation.tci \
  hash-tables.tci \
  common.tci \
  stripping.tci \
  special-procedures.tci \
  param-cache.tci \
  debug.tci

special-procedures.tcb : \
  special-procedures.tci \
  representation.tci \
  type-translation.tci \
  type-system.tci \
  cloning.tci \
  common.tci \
  expression-translation.tci \
  debug.tci

statprof-work.tcb : \
  statprof-work.tci

stripping.tcb : \
  stripping.tci \
  representation.tci \
  cloning.tci \
  debug.tci

symbol-table.tcb : \
  symbol-table.tci

target-compilation-common.tcb : \
  target-compilation-common.tci \
  representation.tci \
  common.tci \
  cloning.tci \
  type-system.tci \
  special-procedures.tci \
  address-environment.tci \
  address-table.tci

target-object-printing.tcb : \
  target-object-printing.tci \
  entities.tci \
  representation.tci \
  common.tci \
  type-system.tci \
  var-names.tci \
  debug.tci

test1.tcb : \
  test1.tci \
  debug.tci

test2.tcb : \
  test2.tci

time.tcb : \
  time.tci

translation-common.tcb : \
  translation-common.tci \
  type-translation.tci \
  type-system.tci \
  representation.tci

translator-keywords.tcb : \
  translator-keywords.tci \
  representation.tci

tree-il-target-compilation.tcb : \
  tree-il-target-compilation.tci \
  representation.tci \
  parametrized-definitions.tci \
  binder.tci \
  type-system.tci \
  hash-tables.tci \
  expressions.tci \
  common.tci \
  type-translation.tci \
  param-cache.tci \
  stripping.tci \
  special-procedures.tci \
  cloning.tci \
  cycles.tci \
  instances.tci \
  linker-instantiation.tci \
  debug.tci

type-system.tcb : \
  type-system.tci \
  representation.tci \
  parametrized-definitions.tci \
  common.tci \
  cloning.tci \
  common-procedure-utilities.tci \
  type-translation.tci \
  cycles.tci \
  address-allocation.tci \
  expression-translation.tci \
  translation-common.tci \
  pcode-common.tci \
  debug.tci \
  target-object-printing.tci

type-translation.tcb : \
  type-translation.tci \
  representation.tci \
  type-system.tci \
  cloning.tci \
  common-procedure-utilities.tci \
  constructors.tci \
  parametrized-instances.tci \
  parametrized-definitions.tci \
  param-cache.tci \
  common.tci \
  debug.tci

var-names.tcb : \
  var-names.tci

