#!/bin/sh

for fl in *.thp ; do
  ../../../tools/compute-theme-d-deps.scm $fl theme-d-in-theme-d
done

for fl in *.thi ; do
  ../../../tools/compute-theme-d-deps.scm $fl theme-d-in-theme-d
done

for fl in *.thb ; do
  ../../../tools/compute-theme-d-deps.scm $fl theme-d-in-theme-d
done
