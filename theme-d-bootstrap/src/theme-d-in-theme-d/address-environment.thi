
;; Copyright (C) 2017, 2022 Tommi Höynälänmaa

(define-interface (theme-d-in-theme-d address-environment)

  (import (standard-library core)
	  (theme-d-in-theme-d address-table)
	  (theme-d-in-theme-d entities)
	  (theme-d-in-theme-d symbol-table)
	  (theme-d-in-theme-d hash-tables)
	  (theme-d-in-theme-d binder))

  (define-class <address-environment>
    (fields
     (address-env-parent (:maybe <address-environment>) public public)
     (ht-table (:maybe <address-table>) public public)
     (ht-bindings (:maybe (:address-hash-table <reference>)) public public)))

  (declare construct-address-environment
	   (:procedure ((:maybe <address-environment>) <integer>)
		       <address-environment> pure))

  (declare make-address-environment
	   (:procedure (<environment>) <address-environment> pure))

  (declare address-env-get-item
	   (:procedure (<address-environment> <address>)
		       (:alt-maybe <reference>)
		       pure))

  (declare address-env-address-exists?
	   (:procedure (<address-environment> <address>) <boolean> pure))

  (declare address-env-add-binding!
	   (:procedure (<address-environment> <reference>) <none> nonpure))

  (declare address-env-add-binding2!
	   (:procedure (<address-environment> <address> <reference>)
		       <none> nonpure))

  (declare address-env-bind-object!
	   (:procedure (<address-environment> <binder> <boolean>
					      <target-object>)
		       <target-object> nonpure))

  (declare address-env-bind-variable!
	   (:procedure (<address-environment> <binder> <boolean>
					      <normal-variable>)
		       <normal-variable> nonpure))

  (declare construct-local-address-env
	   (:procedure (<address-environment>
			(:uniform-list <reference>))
		       <address-environment> pure))

  (declare construct-local-address-env2
	   (:procedure (<address-environment>
			(:uniform-list (:pair <address> <reference>)))
		       <address-environment> pure)))
