
;; Copyright (C) 2022 Tommi Höynälänmaa

(define-body (theme-d-in-theme-d linker-errors)
  
  
  (import (standard-library files)
          (standard-library string-utilities)
          (standard-library list-utilities)
          (standard-library console-io)
          (standard-library object-string-conversion)
          (standard-library system)
          (theme-d-in-theme-d errors-common)
          (theme-d-in-theme-d target-object-printing)
          (theme-d-in-theme-d representation)
          (theme-d-in-theme-d entities)
          (theme-d-in-theme-d expressions)
          (theme-d-in-theme-d common)
          (theme-d-in-theme-d instances))
  
  
  (define-simple-proc delete-target-files
      (((linker <linker>)) <none> nonpure)
    (let ((op-interm (field-ref linker 'file-interm)))
      ;; It's ok for the following expression to match nothing.
      (match-type-weak op-interm
        ((op1 <output-port>)
         (close-output-port op1)))
      (let ((str-interm-filename
             (field-ref linker 'str-interm-filename)))
        (if (not (string-empty? str-interm-filename))
            (begin
             (guard-without-result
               (exc (else
                     (console-display-line
                      "Error cleaning intermediate file.")))
               (if (file-exists? str-interm-filename)
                   (delete-file str-interm-filename)))))))
    (let ((str-target-filename
           (field-ref linker 'str-target-filename)))
      (if (not (string-empty? str-target-filename))
          (begin
           (guard-without-result
             (exc (else
                   (console-display-line
                    "Error cleaning target file.")))
             (if (file-exists? str-target-filename)
                 (delete-file str-target-filename))))))
    (field-set! linker 'str-interm-filename "")
    (field-set! linker 'file-interm null)
    (field-set! linker 'str-target-filename ""))
  
  
  (define-simple-proc convert-module-to-text
      (((mn <general-module-name>)) <string> pure)
    (match-type mn
      ((<null>) "()")
      ((<boolean>) "()")
      ((s <symbol>)
       (string-append "(" (symbol->string s) ")"))
      ((l (:uniform-list <symbol>))
       (string-append
        "("
        (join-strings-with-sep
         (map1 symbol->string l)
         " ")
        ")"))))
  
  
  (define-simple-proc make-message
      (((parts (splice (:uniform-list (:alt-maybe <string>))))) <string> pure)
    (let ((actual-parts
           (cast (:uniform-list <string>)
                 (filter
                  (lambda (((str (:alt-maybe <string>))) <boolean> pure)
                    (if-object str #t #f))
                  parts))))
      (join-strings-with-sep actual-parts " ")))
  
  
  (define-simple-proc get-expr-text
      (((expr <source-expr>)) (:alt-maybe <string>) pure)
    (match-type expr
      ((<null>) #f)
      ((l <source-expr-list>)
       (if (>= (length l) 1)
           (symbol->string (cast <symbol> (uniform-list-ref l 0)))
           #f))
      (else #f)))
  
  
  (define-simple-proc get-pcode-toplevel-expr-text
      (((expr <source-expr>)) (:alt-maybe <string>) pure)
    (match-type expr
      ((<null>) #f)
      ((l <source-expr-list>)
       (let ((i-len (length l)))
         (cond
          ((= i-len 1)
           (string-append "expression of type "
                          (symbol->string
                           (cast <symbol> (uniform-list-ref l 0)))))
          ((>= i-len 2)
           (if (equal? (uniform-list-ref l 0) 'general-variable)
               (string-append
                "variable "
                (symbol->string (cast <symbol> (uniform-list-ref l 1))))
               #f))
          (else #f))))
      (else #f)))
  
  
  ;; There is no ltype here unlike T-in-S.
  (define-simple-proc get-instance-text
      (((instance <instance>)) <string> pure)
    (match-type instance
      ((<class-instance>) "an instance of a parametrized class")
      ((<proc-instance>) "an instance of a parametrized procedure")
      ;; There is <raw-proc-instance> in T-in-S.
      ((<raw-proc-instance>) "a raw procedure instance")
      (else "?")))
  
  
  (define-simple-proc proc-inst-known?
      (((linker <linker>) (ent <procedure-expression>)) <boolean> pure)
    (and
     (equal-objects?
      ent
      (field-ref (field-ref linker 'binder-instantiation) 'ent-cur-proc))
     (not (null? (field-ref (field-ref linker 'binder-instantiation)
                            's-cur-toplevel)))))
  
  
  (define-simple-proc get-proc-inst-str2
      (((linker <linker>) (s-kind <symbol>) (al-info <rte-exception-info>))
       <string> pure)
    (let ((expr
           (cdr (cast (:pair <symbol> <procedure-expression>)
                      (assoc 'expr-appl al-info #f)))))
      (if (not (proc-inst-known? linker expr))
          (let* ((l-arg-names (field-ref expr 'l-arg-names))
                 (l-str-arg-names (map1 symbol->string l-arg-names))
                 (str-args (join-strings-with-sep l-str-arg-names " "))
                 (to-result-type (field-ref expr 'tt-result))
                 (str-result-type (target-object-as-string
                                   to-result-type)))
            (string-append
             " while binding procedure with arguments ("
             str-args
             ") and result type "
             str-result-type))
          (let ((s-proc-name
                 (cdr (cast (:pair <symbol> (:maybe <symbol>))
                            (assoc 's-proc-name al-info #f)))))
            (if (symbol? s-proc-name)
                (string-append
                 " while binding procedure "
                 (symbol->string (cast <symbol> s-proc-name)))
                "")))))
  
  
  (define-simple-proc get-error-text
      (((linker <linker>) (s-kind <symbol>) (al-info <rte-exception-info>))
       <string> pure)
    (case s-kind
      ((did-not-deduce-all-tvars-2)
       (let ((s-proc-name
              (cdr (cast (:pair <symbol> (:maybe <symbol>))
                         (assoc 'proc-name al-info null)))))
         (match-type s-proc-name
           ((<null>)
            (string-append
             "Failed to deduce all type variables in an "
             "application of a procedure"))
           ((s <symbol>)
            (string-append
             "Failed to deduce all type variables in an "
             "application of procedure "
             (symbol->string s))))))
      ((target-field-ref:field-does-not-exist)
       (let ((to (cdr (cast (:pair <symbol> <target-object>)
                            (assoc 'to al-info null))))
             (s-field (cdr (cast (:pair <symbol> <symbol>)
                                 (assoc 's-field al-info null)))))
         (string-append
          "Field "
          (symbol->string s-field)
          " does not exist in class "
          ;; We don't want atomic objects here.
          (target-object-as-string (get-entity-type to))
          ".")))
      ((do-bind-type-vars00:invalid-input)
       (let ((x (cdr (cast (:pair <symbol> <object>)
                           (assoc 'object al-info null)))))
         (string-append
          "invalid procedure input "
          (target-object-as-string x))))
      ((procedure-purity-mismatch-2)
       (string-append
        "Purity mismatch"
        (get-proc-inst-str2 linker s-kind al-info)))
      ((type-mismatch-in-proc-appl-1 type-mismatch-in-proc-appl-2)
       (let ((s-proc-name (cdr (cast (:pair <symbol> (:maybe <symbol>))
                                     (assoc 's-proc-name al-info #f)))))
         (match-type s-proc-name
           ((<null>)
            "Argument type mismatch in an application of a procedure")
           ((s <symbol>)
            (string-append
             "Argument type mismatch in an application of procedure "
             (symbol->string s))))))
      ((generic-static-dispatch-error-1)
       (let* ((gen-proc
               (cdr (cast (:pair <symbol> <target-object>)
                          (assoc 'to-gen-proc al-info #f))))
              (addr (field-ref gen-proc 'address)))
         (match-type addr
           ((<null>)
            "Error in the static dispatch of a generic procedure")
           ((addr1 <address>)
            (string-append
             "Error in the static dispatch of generic procedure "
             (symbol->string
              (cast <symbol> (field-ref addr1 's-source-name))))))))
      ((noncovariant-method-definition
        noncovariant-method-declaration)
       (get-noncov-method-error-text s-kind al-info))
      ((return-attr-mismatch)
       (string-append
        "Return attribute mismatch"
        (get-proc-inst-str2 linker s-kind al-info)))
      ;; Global variables don't need get-var-orig-name.
      ((cannot-declare-existing-variable-3)
       (string-append
        "Trying to declare an existing variable "
        (symbol->string (cdr (cast (:pair <symbol> <symbol>)
                                   (assoc 's-name al-info #f))))
        "."))
      ((field-set!:type-mismatch)
       (let ((s-field-name
              (cdr (cast (:pair <symbol> <symbol>)
                         (assoc 's-field-name al-info #f)))))
         (string-append
          "Type mismatch while setting field "
          (symbol->string s-field-name))))
      ((main-procedure-not-defined)
       "Main procedure not defined")
      ((cyclic-prelink-dependency)
       (let ((mn
              (cdr (cast (:pair <symbol> <module-name>)
                         (assoc 'module-name al-info #f)))))
         (string-append
          "Cyclic prelink dependency with module "
          (convert-module-to-text mn)
          ".")))
      ((type-mismatch-in-param-proc-appl-2)
       (let ((s-proc-name (cdr (cast (:pair <symbol> (:maybe <symbol>))
                                     (assoc 's-proc-name al-info #f)))))
         (match-type s-proc-name
           ((s1 <symbol>)
            (string-append
             "Type mismatch when calling parametrized procedure "
             (symbol->string s1)))
           (else "Type mismatch when calling a parametrized procedure"))))
      (else
       (string-append "Error " (symbol->string s-kind)))))
  
  
  (define-simple-proc make-pcode-reading-error-message
      (((linker <linker>) (s-kind <symbol>) (al-info <rte-exception-info>))
       <string> pure)
    (let* ((error-text (get-error-text linker s-kind al-info))
           (i-len (string-length error-text)))
      (if (and (>= i-len 1)
               (member? (string-ref error-text (- i-len 1))
                        '(#\. #\newline)))
          error-text
          (let ((module (field-ref linker 'mn-current-module))
                (expr (field-ref linker 'sexpr-current))
                (toplevel-expr (field-ref linker 'sexpr-current-toplevel)))
            (let* ((module-text
                    (if (not-null? module)
                        (if (equal? s-kind 'main-procedure-not-defined)
                            (string-append
                             "in program "
                             (convert-module-to-text module))
                            (string-append
                             "from module "
                             (convert-module-to-text module)))
                        #f))
                   (whole-expr-text
                    (cond
                     ((equal? s-kind 'main-procedure-not-defined)
                      #f)
                     ((equal? expr toplevel-expr)
                      (if (not-null? expr)
                          (let ((toplevel-expr-text
                                 (get-pcode-toplevel-expr-text
                                  toplevel-expr)))
                            (if-object
                              toplevel-expr-text
                              (string-append
                               "while reading toplevel "
                               (cast <string> toplevel-expr-text))
                              (if-object module-text "while reading" #f)))
                          (if-object module-text "while reading" #f)))
                     (else
                      (let ((expr-text (get-expr-text expr))
                            (toplevel-expr-text
                             (get-pcode-toplevel-expr-text toplevel-expr)))
                        (cond-object
                         ((and-object expr-text toplevel-expr-text)
                          (string-append
                           "while reading expression of type "
                           (cast <string> expr-text)
                           " from toplevel "
                           (cast <string> toplevel-expr-text)))
                         ((and-object (not-object expr-text)
                                      toplevel-expr-text)
                          (string-append
                           "while reading toplevel "
                           (cast <string> toplevel-expr-text)))
                         ((and-object expr-text
                                      (not-object toplevel-expr-text))
                          (string-append
                           "while reading expression of type "
                           (cast <string> expr-text)))
                         (else
                          (if-object module-text "while reading" #f))))))))
              (string-append
               (make-message error-text whole-expr-text module-text) "."))))))
  
  
  (define-simple-proc make-target-compilation-error-message
      (((linker <linker>) (s-kind <symbol>) (al-info <rte-exception-info>))
       <string> pure)
    (let* ((error-text (get-error-text linker s-kind al-info))
           (i-len (string-length error-text)))
      (if (and (>= i-len 1)
               (member? (string-ref error-text (- i-len 1))
                        '(#\. #\newline)))
          error-text
          (let ((module (field-ref linker 'mn-current-module))
                (repr (field-ref linker 'ent-current))
                (toplevel-repr (field-ref linker 'ent-current-toplevel)))
            (let ((module-text
                   (if (not-null? module)
                       (string-append
                        "from module "
                        (convert-module-to-text module))
                       #f))
                  (whole-repr-text
                   (if (equal? repr toplevel-repr)
                       (let ((toplevel-repr-text
                              (match-type toplevel-repr
                                ((<null>) #f)
                                ((ent <entity>)
                                 (get-repr-text ent #t)))))
                         (if-object toplevel-repr-text
                           (string-append
                            "while compiling "
                            (cast <string> toplevel-repr-text))
                           "while compiling"))
                       (let ((repr-text
                              (match-type repr
                                ((<null>) #f)
                                ((ent <entity>)
                                 (get-repr-text ent #f))))
                             (toplevel-repr-text
                              (match-type toplevel-repr
                                ((<null>) #f)
                                ((ent <entity>)
                                 (get-repr-text ent #t)))))
                         (cond-object
                          ((and-object repr-text toplevel-repr-text)
                           (string-append
                            "while compiling "
                            (cast <string> repr-text)
                            " from "
                            (cast <string> toplevel-repr-text)))
                          ((and-object (not-object repr-text)
                                       toplevel-repr-text)
                           (string-append
                            "while compiling "
                            (cast <string> toplevel-repr-text)))
                          ((and-object repr-text
                                       (not-object toplevel-repr-text))
                           (string-append
                            "while compiling "
                            (cast <string> repr-text)))
                          (else "during compilation"))))))
              (string-append
               (make-message
                error-text whole-repr-text module-text) "."))))))
  
  
  (define-simple-proc make-instantiation-error-message
      (((linker <linker>) (s-kind <symbol>) (al-info <rte-exception-info>))
       <string> pure)
    "Error in parametrized entity instantiation.")
  
  
  (define-simple-proc make-coverage-error-message
      (((linker <linker>) (s-kind <symbol>) (al-info <rte-exception-info>))
       <string> pure)
    "Error in coverage analysis.")
  
  
  (define-simple-proc make-binding-error-message
      (((linker <linker>) (s-kind <symbol>) (al-info <rte-exception-info>))
       <string> pure)
    (let* ((error-text (get-error-text linker s-kind al-info))
           (i-len (string-length error-text)))
      (if (and (>= i-len 1)
               (member? (string-ref error-text (- i-len 1))
                        '(#\. #\newline)))
          error-text
          (let* ((module (field-ref linker 'mn-current-module))
                 (repr (field-ref linker 'ent-current-to-bind))
                 ;; We use the same field current-toplevel-repr
                 ;; for both target compilation and binding.
                 (toplevel-repr (field-ref linker 'ent-current-toplevel))
                 (while-text?
                  (not (member? s-kind
                                '(proc-inst-error proc-appl-inst-error))))
                 (str-while-text
                  (if while-text?
                      "while binding instances in "
                      "in ")))
            (let* ((module-text
                    (if (not-null? module)
                        (string-append
                         "in module "
                         (convert-module-to-text module))
                        #f))
                   (str-proc-text
                    (let ((s-cur (field-ref
                                  (field-ref linker 'binder-instantiation)
                                  's-cur-toplevel)))
                      (if (and (not (null? s-cur))
                               (not
                                (and (equal? s-kind 'return-attr-mismatch)
                                     (cdr
                                      (cast (:pair <symbol> <boolean>)
                                            (assoc 'toplevel? al-info #f))))))
                          (string-append "in procedure "
                                         (symbol->string
                                          (cast <symbol> s-cur)))
                          "")))
                   (whole-repr-text
                    (cond
                     ((and (equal? s-kind 'return-attr-mismatch)
                           (cdr
                            (cast (:pair <symbol> <boolean>)
                                  (assoc 'toplevel? al-info #f))))
                      #f)
                     ((equal? s-kind 'procedure-purity-mismatch-2)
                      #f)
                     ((not (string-empty? str-proc-text))
                      str-proc-text)
                     ((equal? repr toplevel-repr)
                      (let ((toplevel-repr-text
                             (match-type toplevel-repr
                               ((<null>) #f)
                               ((ent <entity>)
                                (get-repr-text ent #t)))))
                        (string-append
                         str-while-text
                         (if-object toplevel-repr-text
                           (cast <string> toplevel-repr-text)
                           ""))))
                     (else
                      (let* ((repr-text
                              (match-type repr
                                ((<null>) #f)
                                ((ent <entity>)
                                 (get-repr-text ent #f))))
                             (toplevel-repr-text
                              (match-type toplevel-repr
                                ((<null>) #f)
                                ((ent <entity>)
                                 (get-repr-text ent #t)))))
                        (cond-object
                         ((and-object repr-text toplevel-repr-text)
                          (string-append
                           str-while-text
                           (cast <string> repr-text)
                           " arising from "
                           (cast <string> toplevel-repr-text)))
                         ((and-object
                           (not-object repr-text) toplevel-repr-text)
                          (string-append
                           (if while-text?
                               "while binding instances arising from "
                               "arising from ")
                           (cast <string> toplevel-repr-text)))
                         ((and-object
                           repr-text (not-object toplevel-repr-text))
                          (string-append
                           str-while-text
                           (cast <string> repr-text)))
                         (else "while binding instances")))))))
              (string-append
               (make-message
                error-text whole-repr-text module-text) "."))))))
  
  
  (define-simple-proc make-other-error-message
      (((linker <linker>) (s-kind <symbol>) (al-info <rte-exception-info>))
       <string> pure)
    (let* ((error-text (get-error-text linker s-kind al-info))
           (i-len (string-length error-text)))
      (if (and (>= i-len 1)
               (member? (string-ref error-text (- i-len 1))
                        '(#\. #\newline)))
          error-text
          (string-append error-text "."))))
  
  
  (define-simple-proc display-extra-info
      (((linker <linker>) (s-kind <symbol>) (al-info <rte-exception-info>))
       <none> nonpure)
    (case s-kind
      ((type-mismatch-in-proc-appl type-mismatch-in-param-proc-appl-2)
       (let ((tt-actual (cdr (cast (:pair <symbol> <target-object>)
                                   (assoc 'tt-actual al-info #f))))
             (tt-declared (cdr (cast (:pair <symbol> <target-object>)
                                     (assoc 'tt-declared al-info #f)))))
         (console-display "Actual type: ")
         (console-display-line (target-object-as-string tt-actual))
         (console-display "Declared type: ")
         (console-display-line (target-object-as-string tt-declared))))
      ((did-not-deduce-all-tvars-2)
       (let ((actual-type
              (cdr (cast <pair>
                         (assoc 'actual-type al-info null))))
             (declared-type
              (cdr (cast <pair>
                         (assoc 'declared-type al-info null))))
             (bindings
              (cast (:alist <target-object> <target-object>)
                    (cdr
                     (cast <pair> (assoc 'bindings al-info null)))))
             (l-needed
              (cdr (cast <pair> (assoc 'needed al-info null)))))
         (console-display "Actual type: ")
         (console-display (target-object-as-string actual-type))
         (console-newline)
         (console-display "Declared type: ")
         (console-display (target-object-as-string declared-type))
         (console-newline)
         (display-bindings bindings)
         (console-display "Needed type variables: ")
         (console-display (target-object-as-string l-needed))
         (console-newline)))
      ((target-field-ref:undefined-fields)
       (console-display "Trying to access field ")
       (console-display-line
        (cdr (cast (:pair <symbol> <symbol>)
                   (assoc 's-field al-info #f)))))
      ((field-set!:type-mismatch)
       (let ((tt-value
              (cdr (cast (:pair <symbol> <target-object>)
                         (assoc 'tt-value al-info #f))))
             (tt-field
              (cdr (cast (:pair <symbol> <target-object>)
                         (assoc 'tt-field al-info #f)))))
         (console-display "Actual value type: ")
         (console-display (target-object-as-string tt-value))
         (console-newline)
         (console-display "Field type: ")
         (console-display (target-object-as-string tt-field))
         (console-newline)))))
  
  
  (define-simple-proc handle-linker-error
      (((linker <linker>) (x-exc <object>)) <none>
                                            (force-pure never-returns))
    (delete-target-files linker)
    ;; No need to use match-type-strong here (?).
    (match-type x-exc
      ((cnd <condition>)
       (if (rte-exception? cnd)
           (let* ((s-kind (get-rte-exception-kind cnd))
                  (tmp1 (begin (console-display-line "HEP") (console-display-line s-kind) #f))
                  (al-info (cast <rte-exception-info>
                                 (get-rte-exception-info cnd)))
                  (str-message
                   (case (field-ref linker 's-state)
                     ((pcode-reading)
                      (make-pcode-reading-error-message linker s-kind al-info))
                     ((target-compilation)
                      (make-target-compilation-error-message
                       linker s-kind al-info))
                     ((final-compilation)
                      "Final compilation of the program failed.")
                     ((instantiation)
                      (make-instantiation-error-message linker s-kind al-info))
                     ((coverage-analysis)
                      (make-coverage-error-message linker s-kind al-info))
                     ((binding)
                      (make-binding-error-message linker s-kind al-info))
                     ((initial)
                      (make-other-error-message linker s-kind al-info))
                     (else
                      (if (null? (field-ref linker 's-state))
                          (make-other-error-message linker s-kind al-info)
                          "Internal error in error handling.")))))
             (console-display-line str-message)
             (if (field-ref linker 'verbose-errors?)
                 (display-extra-info linker s-kind al-info))
             ;; (exit 1))
             (raise cnd))
           (raise cnd)))
      ((s <symbol>)
       (console-display "Error: ")
       (console-display s)
       (console-newline)
       (raise-simple s))
      (else (raise x-exc)))))
