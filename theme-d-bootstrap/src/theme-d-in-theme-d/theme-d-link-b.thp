
;; Copyright (C) 2022 Tommi Höynälänmaa

(define-proper-program (theme-d-in-theme-d theme-d-link-b)
  
  (import (standard-library core)
          (standard-library command-line-parser)
          (standard-library object-string-conversion)
          (standard-library list-utilities)
          (standard-library string-utilities)
          (standard-library console-io)
          (standard-library text-file-io)
          (standard-library system)
          (theme-d-in-theme-d link-program)
          (theme-d-in-theme-d link-program-split)
          (theme-d-in-theme-d target-object-printing)
          (theme-d-in-theme-d configuration)
          (theme-d-in-theme-d parameters)
          (theme-d-in-theme-d common)
          (theme-d-in-theme-d statprof-work)
          (theme-d-in-theme-d debug))
  
  
  ;; The following declaration is there for debugging.
  (prevent-stripping target-object-as-string)
  
  
  (define print-vgp-statistics
    (prim-proc print-vgp-statistics () <none> nonpure))
  
  
  (define print-param-proc-statistics
    (prim-proc print-param-proc-statistics () <none> nonpure))
  
  
  (define-simple-proc get-unit-name
      (((unit-filename <string>)) (:nonempty-uniform-list <symbol>)
                                  nonpure)
    (let* ((file (open-input-file unit-filename))
           (name
            (cast (:nonempty-uniform-list <symbol>)
                  (read file))))
      (close-input-port file)
      name))
  
  
  (define-simple-proc main
      (((l-args (:uniform-list <string>))) <integer> nonpure)
    (let-mutable ((show-version? <boolean> #f)
                  ;; Pretty printing is not supported.
                  (pretty-print? <boolean> #f)
                  (module-search-path0 <string> "")
                  (source-filename <string> "")
                  (interm-filename <string> "")
                  (target-filename <string> "")
                  ;; (str-introsp-filename <string> "")
                  (message-level <integer> 1)
                  (s-intermediate-language0 <symbol> 'tree-il)
                  (s-intermediate-language (:maybe <symbol>) null)
                  (l-str-custom-modules (:uniform-list <string>) null)
                  ;; (l-str-custom-relative-modules (:uniform-list <string>) null)
                  (str-split-dir0 <string> "")
                  (str-split-basename <string> "target")
                  (i-guile-opt-level <integer> gl-i-default-opt-level)
                  (str-extra-guild-options <string> "")
                  (optimize? <boolean> #t)
                  (optimize0? <boolean> #t)
                  (final-compilation? <boolean> #t)
                  (keep-intermediate? <boolean> #f)
                  (link-to-cache? <boolean> #f)
                  (all-assertions? <boolean> #t)
                  (factorize? <boolean> #t)
                  (strip? <boolean> #t)
                  ;; pretty-print? not defined
                  (verbose-errors? <boolean> #t)
                  (verbose-typechecks? <boolean> #f)
                  (verbose-unlinked-procedures? <boolean> #t)
                  (backtrace? <boolean> #f)
                  (runtime-pretty-backtrace? <boolean> #f)
                  (module-debug-output? <boolean> #f)
                  (show-inst-number? <boolean> #f)
                  (inline-constructors? <boolean> #t)
                  (default-proc-arg-opt? <boolean> #t)
                  (split? <boolean> #f)
                  (optimize-pair-classes? <boolean> #t)
                  (check-all-primitives? <boolean> #f)
                  (l-s-dupl (:uniform-list <symbol>) gl-l-s-default-dupl)
                  (profile-flat? <boolean> #f)
                  (profile-tree? <boolean> #f)
                  (print-vgp-stat? <boolean> #f)
                  (print-pp-stat? <boolean> #f))
      (set! gl-s-mode 'linker)
      (init-theme-d-config)
      (let* ((str-guile-version
              (cast <string> (get-theme-d-config-var 'guile-version)))
             (argd1
              (create <argument-descriptor> #\m #t
                (lambda (((str-option-arg <string>)) <none> nonpure)
                  (set! module-search-path0 str-option-arg))))
             (argd2
              (create <argument-descriptor> "module-path" #t
                (lambda (((str-option-arg <string>)) <none> nonpure)
                  (set! module-search-path0 str-option-arg))))
             (argd3
              (create <argument-descriptor> #\o #t
                (lambda (((str-option-arg <string>)) <none> nonpure)
                  (set! target-filename str-option-arg))))
             (argd4
              (create <argument-descriptor> "output" #t
                (lambda (((str-option-arg <string>)) <none> nonpure)
                  (set! target-filename str-option-arg))))
             (argd5
              (create <argument-descriptor> #\l #t
                (lambda (((str-option-arg <string>)) <none> nonpure)
                  (set! message-level (string->integer str-option-arg)))))
             (argd6
              (create <argument-descriptor> "message-level" #t
                (lambda (((str-option-arg <string>)) <none> nonpure)
                  (set! message-level (string->integer str-option-arg)))))
             (argd7
              (create <argument-descriptor> "intermediate-language" #t
                (lambda (((str-option-arg <string>)) <none> nonpure)
                  (set! s-intermediate-language0
                    (string->symbol str-option-arg)))))
             (argd8
              (create <argument-descriptor> #\i #t
                (lambda (((str-option-arg <string>)) <none> nonpure)
                  (set! s-intermediate-language0
                    (string->symbol str-option-arg)))))
             (argd9
              (create <argument-descriptor> "no-final-compilation" #f
                (lambda (((str-option-arg <string>)) <none> nonpure)
                  (set! final-compilation? #f))))
             (argd10
              (create <argument-descriptor> "no-weak-assertions" #f
                (lambda (((str-option-arg <string>)) <none> nonpure)
                  (set! all-assertions? #f))))
             (argd11
              (create <argument-descriptor> "no-factorization" #f
                (lambda (((str-option-arg <string>)) <none> nonpure)
                  (set! factorize? #f))))
             (argd12
              (create <argument-descriptor> "no-strip" #f
                (lambda (((str-option-arg <string>)) <none> nonpure)
                  (set! strip? #f))))
             (argd13
              (create <argument-descriptor> "no-verbose-errors" #f
                (lambda (((str-option-arg <string>)) <none> nonpure)
                  (set! verbose-errors? #f))))
             (argd14
              (create <argument-descriptor> "no-verbose-typechecks" #f
                (lambda (((str-option-arg <string>)) <none> nonpure)
                  (set! verbose-typechecks? #t))))
             (argd15
              (create <argument-descriptor> "backtrace" #f
                (lambda (((str-option-arg <string>)) <none> nonpure)
                  (set! backtrace? #t))))
             (argd16
              (create <argument-descriptor> "intermediate-file" #t
                (lambda (((str-option-arg <string>)) <none> nonpure)
                  (set! interm-filename str-option-arg))))
             (argd17
              (create <argument-descriptor> #\n #t
                (lambda (((str-option-arg <string>)) <none> nonpure)
                  (set! interm-filename str-option-arg))))
             (argd18
              (create <argument-descriptor> "keep-intermediate" #f
                (lambda (((str-option-arg <string>)) <none> nonpure)
                  (set! keep-intermediate? #t))))
             (argd19
              (create <argument-descriptor> "link-to-cache" #f
                (lambda (((str-option-arg <string>)) <none> nonpure)
                  (set! link-to-cache? #t))))
             (argd20
              (create <argument-descriptor> "no-unlinked-procedure-names" #f
                (lambda (((str-option-arg <string>)) <none> nonpure)
                  (set! verbose-unlinked-procedures? #f))))
             (argd21
              (create <argument-descriptor> "runtime-pretty-backtrace" #f
                (lambda (((str-option-arg <string>)) <none> nonpure)
                  (set! runtime-pretty-backtrace? #t))))
             (argd22
              (create <argument-descriptor> "module-debug-output" #f
                (lambda (((str-option-arg <string>)) <none> nonpure)
                  (set! module-debug-output? #t))))
             (argd23
              (create <argument-descriptor> "show-inst-number" #f
                (lambda (((str-option-arg <string>)) <none> nonpure)
                  (set! show-inst-number? #t))))
             (argd24
              (create <argument-descriptor> #\x #t
                (lambda (((str-option-arg <string>)) <none> nonpure)
                  (set! l-str-custom-modules
                    (append-uniform2 l-str-custom-modules
                                     (list str-option-arg))))))
             ;; -y, -z, and --introspection-file not implemented
             (argd25
              (create <argument-descriptor> "no-inline-constructors" #f
                (lambda (((str-option-arg <string>)) <none> nonpure)
                  (set! inline-constructors? #f))))
             (argd26
              (create <argument-descriptor> "no-proc-arg-opt" #f
                (lambda (((str-option-arg <string>)) <none> nonpure)
                  (set! default-proc-arg-opt? #f))))
             (argd27
              (create <argument-descriptor> "split" #f
                (lambda (((str-option-arg <string>)) <none> nonpure)
                  (set! split? #t))))
             (argd28
              (create <argument-descriptor> "split-dir" #t
                (lambda (((str-option-arg <string>)) <none> nonpure)
                  (set! str-split-dir0 str-option-arg))))
             (argd29
              (create <argument-descriptor> "split-basename" #t
                (lambda (((str-option-arg <string>)) <none> nonpure)
                  (set! str-split-basename str-option-arg))))
             (argd30
              (create <argument-descriptor> "no-optimization" #f
                (lambda (((str-option-arg <string>)) <none> nonpure)
                  (set! optimize0? #f))))
             (argd31
              (create <argument-descriptor> "duplicates" #t
                (lambda (((str-option-arg <string>)) <none> nonpure)
                  (set! l-s-dupl
                    (map1 string->symbol
                          (split-string str-option-arg #\space))))))
             (argd32
              (create <argument-descriptor> "no-pair-class-optimization" #f
                (lambda (((str-option-arg <string>)) <none> nonpure)
                  (set! optimize-pair-classes? #f))))
             (argd33
              (create <argument-descriptor> "check-all-primitives?" #f
                (lambda (((str-option-arg <string>)) <none> nonpure)
                  (set! check-all-primitives? #f))))
             (argd34
              (create <argument-descriptor> "guile-opt-level" #t
                (lambda (((str-option-arg <string>)) <none> nonpure)
                  (set! i-guile-opt-level
                    (string->integer str-option-arg)))))
             (argd35
              (create <argument-descriptor> "extra-guild-options" #t
                (lambda (((str-option-arg <string>)) <none> nonpure)
                  (set! str-extra-guild-options str-option-arg))))
             (argd36
              (create <argument-descriptor> "version" #f
                (lambda (((str-option-arg <string>)) <none> nonpure)
                  (set! show-version? #t))))
             (argd37
              (create <argument-descriptor> "profile-flat" #f
                (lambda (((str-option-arg <string>)) <none> nonpure)
                  (set! profile-flat? #t))))
             (argd38
              (create <argument-descriptor> "profile-tree" #f
                (lambda (((str-option-arg <string>)) <none> nonpure)
                  (set! profile-tree? #t))))
             (argd39
              (create <argument-descriptor> "print-vgp-stat" #f
                (lambda (((str-option-arg <string>)) <none> nonpure)
                  (set! print-vgp-stat? #t))))
             (argd40
              (create <argument-descriptor> "print-pp-stat" #f
                (lambda (((str-option-arg <string>)) <none> nonpure)
                  (set! print-pp-stat? #t))))
             (arg-descs (list argd1 argd2 argd3 argd4 argd5
                              argd6 argd7 argd8 argd9 argd10
                              argd11 argd12 argd13 argd14 argd15
                              argd16 argd17 argd18 argd19 argd20
                              argd21 argd22 argd23 argd24 argd25
                              argd26 argd27 argd28 argd29 argd30
                              argd31 argd32 argd33 argd34 argd35
                              argd36 argd37 argd38 argd39 argd40))
             (args-without-cmd (drop l-args 1))
             (proc-handle-proper-args
              (lambda (((l-proper-args (:uniform-list <string>)))
                       <none> nonpure)
                (if (= (length l-proper-args) 1)
                    (set! source-filename
                      (uniform-list-ref l-proper-args 0))
                    #f))))
        (parse-command-line args-without-cmd arg-descs proc-handle-proper-args)
        
        (if show-version?
            (begin
             (console-display-line "6.0.0")
             (exit 0)))
        
        (if (string-empty? source-filename)
            (begin
             (write-line-info "Missing source filename.")
             (exit 1)))
        (if (not (file-exists? source-filename))
            (begin
             (write-line-info "Source file does not exist.")
             (exit 1)))
        
        (set! gl-show-info? (not (= message-level 0)))
        (if (> message-level 0)
            (set! gl-i-debug-level (- message-level 1))
            (set! gl-i-debug-level 0))
        (let* ((str-theme-d-lib-dir
                (match-type (get-theme-d-config-var 'lib-dir)
                  ((str <string>) str)
                  (else (raise-simple 'error-in-configuration-file))))
               (l-module-search-path
                (if (string-empty? module-search-path0)
                    (list str-theme-d-lib-dir ".")
                    (parse-search-path module-search-path0
                                       str-theme-d-lib-dir)))
               (program-name
                (guard-nonpure
                  (exc
                   (else
                    (write-line-info
                     "Program file corrupted. Invalid program name.")
                    (static-cast <symbol> (exit 1))))
                  (get-unit-name source-filename)))
               (str-split-dir
                (if (string-empty? str-split-dir0)
                    (string-append
                     (get-filename-without-ext
                      (get-filename-without-path source-filename))
                     ".build")
                    str-split-dir0)))
          (if (not (is-module-name? program-name))
              (begin
               (write-line-info
                "Program file corrupted. Invalid program name.")
               (exit 1)))
          (if (not (member? s-intermediate-language0
                            (append-uniform2 (list 'tree-il 'guile 'guile0)
                                             gl-l-all-int-lang)))
              (begin
               (write-line-info "Unknown intermediate language.")
               (exit 1)))
          (if (< i-guile-opt-level 0)
              (begin
               (write-line-info "Invalid guile optimization level.")
               (exit 1)))
          (set! s-intermediate-language
            (cond
             ((member? s-intermediate-language0 gl-l-all-int-lang)
              s-intermediate-language0)
             ((equal? s-intermediate-language0 'tree-il)
              (cond
               ((string=? str-guile-version "3.0") 'tree-il-3.0)
               (else
                (write-line-info
                 "Invalid Guile version in the configuration file.")
                (exit 1))))
             ((or
               (equal? s-intermediate-language0 'guile)
               (equal? s-intermediate-language0 'guile0))
              (cond
               ((string=? str-guile-version "3.0") 'guile-3.0)
               (else
                (write-line-info
                 "Invalid Guile version in the configuration file.")
                (exit 1))))
             (else
              (write-line-info
               "Invalid intermediate language.")
              (exit 1))))
          (set! optimize?
            (and (not (eq? s-intermediate-language0 'guile0))
                 optimize0?))
          (if (and split?
                   (not (member? s-intermediate-language
                                 '(guile-3.0
                                   tree-il-3.0))))
              (begin
               (write-line-info
                "Splitting is not supported for the target platform.")
               (exit 1)))
          (dw1 "source filename: ")
          (dwl1 source-filename)
          (dw1 "intermediate filename (empty for default): ")
          (dwl1 interm-filename)
          (dw1 "target filename (empty for default): ")
          (dwl1 target-filename)
          (dw1 "module search path: ")
          (dwl1 l-module-search-path)
          (dw1 "intermediate language: ")
          (dwl1 s-intermediate-language)
          (dw1 "custom modules: ")
          (dwl1 l-str-custom-modules)
          (dw1 "extra guild options: ")
          (dwl1 str-extra-guild-options)
          (dw1 "split target: ")
          (dwl1 (if split? "yes" "no"))
          (if split?
              (begin
               (dw1 "target directory: ")
               (dwl1 str-split-dir)
               (dw1 "split basename: ")
               (dwl1 str-split-basename)))
          (dw1 "optimize code: ")
          (dwl1 (if optimize? "yes" "no"))
          (dw1 "optimize pair-classes: ")
          (dwl1 (if optimize-pair-classes? "yes" "no"))
          (dw1 "check all primitive procedure result types: ")
          (dwl1 (if check-all-primitives? "yes" "no"))
          (dw1 "guile optimization level: ")
          (dwl1 i-guile-opt-level)
          (dw1 "final compilation: ")
          (dwl1 (if final-compilation? "yes" "no"))
          (dw1 "keep intermediate: ")
          (dwl1 (if keep-intermediate? "yes" "no"))
          (dw1 "link to cache: ")
          (dwl1 (if link-to-cache? "yes" "no"))
          (dw1 "check all assertions: ")
          (dwl1 (if all-assertions? "yes" "no"))
          (dw1 "factorize out type expressions: ")
          (dwl1 (if factorize? "yes" "no"))
          (dw1 "strip unused code: ")
          (dwl1 (if strip? "yes" "no"))
          (dw1 "message level: ")
          (dwl1 message-level)
          ;; (dw1 "pretty print target code: ")
          ;; (dwl1 (if pretty-print? "yes" "no"))
          (dw1 "verbose error messages: ")
          (dwl1 (if verbose-errors? "yes" "no"))
          (dw1 "verbose typechecks: ")
          (dwl1 (if verbose-typechecks? "yes" "no"))
          (dw1 "report unlinked procedure names: ")
          (dwl1 (if verbose-unlinked-procedures? "yes" "no"))
          (dw1 "backtrace: ")
          (dwl1 (if backtrace? "yes" "no"))
          (dw1 "runtime pretty backtrace: ")
          (dwl1 (if runtime-pretty-backtrace? "yes" "no"))
          (dw1 "module debug output: ")
          (dwl1 (if module-debug-output? "yes" "no"))
          (dw1 "inline simple constructors: ")
          (dwl1 (if inline-constructors? "yes" "no"))
          (dw1 "optimize procedure arguments: ")
          (dwl1 (if default-proc-arg-opt? "yes" "no"))
          (dw1 "duplicate handlers: ")
          (dwl1 l-s-dupl)
          (dw1 "profile (flat): ")
          (dwl1 (if profile-flat? "yes" "no"))
          (dw1 "profile (tree): ")
          (dwl1 (if profile-tree? "yes" "no"))
          (dw1 "print vgp statistics: ")
          (dwl1 (if print-vgp-stat? "yes" "no"))
          (dw1 "print parametrized procedure statistics: ")
          (dwl1 (if print-pp-stat? "yes" "no"))
          (if (or profile-flat? profile-tree?)
              (begin
               (statprof-reset 0 50000 #f)
               (statprof-start)))
          (set-display-error-type! #f)
          (if (not split?)
              (theme-link-program program-name
                                  source-filename
                                  interm-filename
                                  target-filename
                                  l-module-search-path
                                  (cast <symbol> s-intermediate-language)
                                  l-str-custom-modules
                                  ;; l-str-custom-relative-modules
                                  ;; str-introsp-filename
                                  str-extra-guild-options
                                  i-guile-opt-level
                                  optimize?
                                  optimize-pair-classes?
                                  check-all-primitives?
                                  final-compilation?
                                  keep-intermediate?
                                  link-to-cache?
                                  all-assertions?
                                  factorize?
                                  strip?
                                  pretty-print?
                                  verbose-errors?
                                  verbose-typechecks?
                                  verbose-unlinked-procedures?
                                  backtrace?
                                  runtime-pretty-backtrace?
                                  module-debug-output?
                                  show-inst-number?
                                  inline-constructors?
                                  default-proc-arg-opt?
                                  l-s-dupl)
              (begin
               (if (file-exists? str-split-dir)
                   (console-display-line "warning: target directory exists")
                   (system (string-append "mkdir " str-split-dir)))
               (theme-link-program-split-new
                program-name
                source-filename
                interm-filename
                target-filename
                l-module-search-path
                (cast <symbol> s-intermediate-language)
                l-str-custom-modules
                ;; l-str-custom-relative-modules
                ;; str-introsp-filename
                str-extra-guild-options
                i-guile-opt-level
                optimize?
                optimize-pair-classes?
                check-all-primitives?
                final-compilation?
                keep-intermediate?
                link-to-cache?
                all-assertions?
                factorize?
                strip?
                pretty-print?
                verbose-errors?
                verbose-typechecks?
                verbose-unlinked-procedures?
                backtrace?
                runtime-pretty-backtrace?
                module-debug-output?
                show-inst-number?
                inline-constructors?
                default-proc-arg-opt?
                str-split-dir
                str-split-basename
                l-s-dupl)))
          (cond
           (profile-flat?
            (statprof-stop)
            (statprof-display))
           (profile-tree?
            (statprof-stop)
            (statprof-display-tree)))
          (if print-vgp-stat?
              (begin
               (dwl1 "vgp statistics:")
               (print-vgp-statistics)))
          (if print-pp-stat?
              (begin
               (dwl1 "param proc statistics:")
               (print-param-proc-statistics))))))
    0))
