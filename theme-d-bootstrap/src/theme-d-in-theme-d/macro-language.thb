
;; Copyright (C) 2020 Tommi Höynälänmaa

(define-body (theme-d-in-theme-d macro-language)
  
  (import (standard-library list-utilities)
          (standard-library dynamic-list)
          (standard-library real-math)
          (theme-d-in-theme-d macro-base)
          (theme-d-in-theme-d macro-base2)
          (theme-d-in-theme-d common)
          (theme-d-in-theme-d hash-tables)
          (theme-d-in-theme-d debug))
  
  (declare interpret-core0
    (:simple-proc (<object> <prim-env>) <object> nonpure))
  
  (define-simple-proc core-lambda-proc? (((x-expr <object>)) <boolean> pure)
    ;; Don't do syntax check here.
    (and (list? x-expr) (not-null? x-expr) (equal? (d-car x-expr) '$lambda)))
    ;; (match-type x-expr
    ;;   ((l <nonempty-list>)
    ;;    (equal-objects? (car l) '$lambda))
    ;;   (else #f)))
  
  (define-syntax define-core-proc1
    (syntax-rules ()
      ((_ proc-name impl-name type)
       (define-simple-proc proc-name (((l-args (rest <object>))) <object>
                                                                 nonpure)
         (if (and (pair? l-args) (null? (d-cdr l-args)))
             (impl-name (cast type (d-car l-args)))
             (syntax-violation (quote proc-name) "syntax error" l-args))))))
        ;;  (match-type l-args
        ;;    ((l1 (:tuple type))
        ;;     (impl-name (car l1)))
        ;;    (else
        ;;     (syntax-violation (quote proc-name) "syntax error" l-args)))))))
  
  (define-syntax define-core-proc2
    (syntax-rules ()
      ((_ proc-name impl-name type1 type2)
       (define-simple-proc proc-name (((l-args (rest <object>))) <object>
                                                                 nonpure)
         (if (equal? (length l-args) 2)
             (impl-name (cast type1 (d-car l-args))
                        (cast type2 (d-cadr l-args)))
             (syntax-violation (quote proc-name) "syntax error" l-args))))))
        ;;  (match-type l-args
        ;;    ((l1 (:tuple type1 type2))
        ;;     (impl-name (car l1) (cadr l1)))
        ;;    (else
        ;;     (syntax-violation (quote proc-name) "syntax error" l-args)))))))
  
  (define-syntax define-core-proc2-no-result
    (syntax-rules ()
      ((_ proc-name impl-name type1 type2)
       (define-simple-proc proc-name (((l-args (rest <object>))) <object>
                                                                 nonpure)
         (if (equal? (length l-args) 2)
             (begin
              (impl-name (cast type1 (d-car l-args))
                         (cast type2 (d-cadr l-args)))
              null)
             (syntax-violation (quote proc-name) "syntax error" l-args))))))
        ;;  (match-type l-args
        ;;    ((l1 (:tuple type1 type2))
        ;;     (impl-name (car l1) (cadr l1))
        ;;     null)
        ;;    (else
        ;;     (syntax-violation (quote proc-name) "syntax error" l-args)))))))
  
  (define-simple-proc $map0 (((proc <general-proc>)
                              (l-lists <object>))
                             (:uniform-list <object>)
                             nonpure)
    ;; (match-type l-lists
    ;;   ((l-lists1 (:uniform-list <nonempty-list>))
    ;;    (let ((cars (map-car l-lists1))
    ;;          (cdrs (map-cdr l-lists1)))
    ;;      (cons
    ;;       (apply-nonpure proc cars)
    ;;       ($map0 proc cdrs))))
    ;;   (else null)))
    (if (or (null? l-lists) (d-exists1? null? l-lists))
        null
        (let ((cars (d-map-car l-lists))
              (cdrs (d-map-cdr l-lists)))
          (cons
           (apply-nonpure proc cars)
           ($map0 proc cdrs)))))
           
  (define-simple-proc $cons (((l-args (rest <object>))) <object> nonpure)
    (if (and (list? l-args) (equal? (length l-args) 2))
        (cons (d-car l-args) (d-cadr l-args))
        (syntax-violation '$cons "syntax error" l-args)))
    ;; (match-type l-args
    ;;   ((l1 (:tuple <object> <object>))
    ;;    (cons (car l1) (cadr l1)))
    ;;   (else
    ;;    (syntax-violation '$cons "syntax error" l-args))))
  
  (define-simple-proc $car (((l-args (rest <object>))) <object> nonpure)
    (if (and (pair? l-args) (null? (d-cdr l-args)) (pair? (d-car l-args)))
        (d-caar l-args)
        (syntax-violation '$car "syntax error" l-args)))
     ;; (match-type l-args
    ;;   ((l1 (:tuple <pair>))
    ;;    (car (car l1)))
    ;;   (else
    ;;    (syntax-violation '$car "syntax error" l-args))))
  
  (define-simple-proc $cdr (((l-args (rest <object>))) <object> nonpure)
    (if (and (pair? l-args) (null? (d-cdr l-args)) (pair? (d-car l-args)))
        (d-cdar l-args)
        (syntax-violation '$cdr "syntax error" l-args)))
    ;; (match-type l-args
    ;;   ((l1 (:tuple <pair>))
    ;;    (cdr (car l1)))
    ;;   (else
    ;;    (syntax-violation '$cdr "syntax error" l-args))))
  
  (define-simple-proc $not (((l-args (rest <object>))) <object> nonpure)
    (if (and (pair? l-args) (null? (d-cdr l-args)))
        (not-object (d-car l-args))
        (syntax-violation '$not "syntax error" l-args)))
    ;; (match-type l-args
    ;;   ((l1 (:tuple <object>))
    ;;    (not-object (car l1)))
    ;;   (else
    ;;    (syntax-violation '$not "syntax error" l-args))))
  
  (define-simple-proc $pair? (((l-args (rest <object>))) <object> nonpure)
    (if (and (pair? l-args) (null? (d-cdr l-args)))
        (pair? (d-car l-args))
        (syntax-violation '$pair? "syntax error" l-args)))
    ;; (match-type l-args
    ;;   ((l1 (:tuple <object>))
    ;;    (pair? (car l1)))
    ;;   (else
    ;;    (syntax-violation '$pair "syntax error" l-args))))
  
  (define-simple-proc $null? (((l-args (rest <object>))) <object> nonpure)
    (if (and (pair? l-args) (null? (d-cdr l-args)))
        (null? (d-car l-args))
        (syntax-violation '$null? "syntax error" l-args)))
    ;; (match-type l-args
    ;;   ((l1 (:tuple <object>))
    ;;    (null? (car l1)))
    ;;   (else
    ;;    (syntax-violation '$null? "syntax error" l-args))))
  
  (define-simple-proc $list? (((l-args (rest <object>))) <object> nonpure)
    (if (and (pair? l-args) (null? (d-cdr l-args)))
        (list? (d-car l-args))
        (syntax-violation '$list? "syntax error" l-args)))
    ;; (match-type l-args
    ;;   ((l1 (:tuple <object>))
    ;;    (is-instance? (car l1) <list>))
    ;;   (else
    ;;    (syntax-violation '$list? "syntax error" l-args))))
  
  (define-simple-proc $list (((l-args (rest <object>))) <object> nonpure)
    (apply list l-args))
  
  (define-simple-proc $equal? (((l-args (rest <object>))) <object> nonpure)
    (if (and (list? l-args) (equal? (length l-args) 2))
        (equal? (d-car l-args) (d-cadr l-args))
        (syntax-violation '$equal? "syntax error" l-args)))
  ;;   (match-type l-args
  ;;     ((l1 (:tuple <object> <object>))
  ;;      (equal? (car l1) (cadr l1)))
  ;;     (else
  ;;      (syntax-violation '$equal? "syntax error" l-args))))

  (define-simple-proc $equal-objects? (((l-args (rest <object>))) <object> nonpure)
    (if (and (list? l-args) (equal? (length l-args) 2))
        (equal-objects? (d-car l-args) (d-cadr l-args))
        (syntax-violation '$equal-objects? "syntax error" l-args)))
  ;;   (match-type l-args
  ;;     ((l1 (:tuple <object> <object>))
  ;;      (equal? (car l1) (cadr l1)))
  ;;     (else
  ;;      (syntax-violation '$equal? "syntax error" l-args))))

  (define-simple-proc $equal-contents? (((l-args (rest <object>))) <object> nonpure)
    (if (and (list? l-args) (equal? (length l-args) 2))
        (equal-contents? (d-car l-args) (d-cadr l-args))
        (syntax-violation '$equal-contents? "syntax error" l-args)))
  ;;   (match-type l-args
  ;;     ((l1 (:tuple <object> <object>))
  ;;      (equal? (car l1) (cadr l1)))
  ;;     (else
  ;;      (syntax-violation '$equal? "syntax error" l-args))))

  (define-simple-proc $= (((l-args (rest <object>))) <object> nonpure)
    (if (null? l-args)
        (syntax-violation '$= "syntax error" l-args)
        (if (pair? l-args)
            (if (null? (d-cdr l-args))
                #t
                (let ((i-len (length l-args)))
                  (cond
                   ((equal? i-len 2)
                    (equal? (d-car l-args) (d-cadr l-args)))
                   ((> i-len 2)
                    (and-object (equal? (d-car l-args) (d-cadr l-args))
                                ($= (d-cdr l-args))))
                   (else
                    (syntax-violation '$= "internal error" l-args)))))
            (syntax-violation '$= "syntax error" l-args))))
    ;; (match-type l-args
    ;;   ((l1 (:tuple <object> <object>))
    ;;    (equal? (car l1) (cadr l1)))
    ;;   ((l2 (:nonempty-uniform-list <object>))
    ;;    (let ((x-head (car l2)))
    ;;      (let-mutable ((b-equal <boolean> #t))
    ;;        (iterate-list (x <object> (cdr l2))
    ;;          (if (not (equal? x x-head))
    ;;              (set! b-equal #f)))
    ;;        b-equal)))
    ;;   (else
    ;;    (syntax-violation '$= "syntax error" l-args))))
  
  (define-core-proc2 $>= >= <real-number> <real-number>)
  (define-core-proc2 $> > <real-number> <real-number>)
  (define-core-proc1 $length length <list>)
  (define-core-proc1 $reverse reverse <list>)
  (define-core-proc2 $+ + <real-number> <real-number>)
  (define-core-proc2 $- - <real-number> <real-number>)
  (define-core-proc1 $raise raise <object>)
  (define-core-proc1 $identifier? identifier? <object>)
  (define-core-proc2 $free-identifier=? free-identifier=?
                     <identifier> <identifier>)
  (define-core-proc2 $free=? free=? <object> <symbol>)
  (define-core-proc1 $invalid-form invalid-form <object>)
  (define-core-proc1 $generate-temporaries generate-temporaries <list>)
  (define-core-proc1 $expand expand <object>)
  (define-core-proc1 $id-free? id-free? <identifier>)
  (define-core-proc2-no-result $env-extend! env-extend!
                               (:alist <macro-key> <macro-binding>)
                               <macro-env>)
  
  (define-simple-proc $map (((l-args (rest <object>))) <object> nonpure)
    ;; (match-type l-args
    ;;   ((l1 (:pair <general-proc> (:uniform-list <list>)))
    ;;    ($map0 (car l1) (cdr l1)))
    ;;   ((l2 (:pair <object> (:uniform-list <list>)))
    ;;    (if (core-lambda-proc? (car l2))
    ;;        ($map0 (lambda (((l-args1 (rest <object>))) <object> nonpure)
    ;;                 (interpret-core (cons (car l2) l-args1)))
    ;;               (cdr l2))
    ;;        (syntax-violation '$map "invalid procedure" l-args)))
    ;;   (else
    ;;    (syntax-violation '$map "syntax error" l-args))))
    (assert (not-null? l-args))
    (match-type (d-car l-args)
      ((proc <general-proc>)
       ($map0 proc (d-cdr l-args)))
      ((x <object>)
       (if (core-lambda-proc? x)
           ($map0 (lambda (((l-args1 (rest <object>))) <object> nonpure)
                    (interpret-core (cons x l-args1)))
                  (d-cdr l-args))
           (syntax-violation '$map "invalid procedure" l-args)))))
      
  (define-simple-proc $call (((l-args (rest <object>))) <object> nonpure)
    (assert (not-null? l-args))
    (match-type (d-car l-args)
      ((proc <general-proc>)
       (apply-nonpure proc (drop l-args 1)))
      ((x <object>)
       (if (core-lambda-proc? x)
           (interpret-core l-args)
           (syntax-violation '$call "invalid procedure" l-args)))))
    ;; (match-type l-args
    ;;   ((l1 (:pair <general-proc> <list>))
    ;;    (apply-nonpure (car l1) (cdr l1)))
    ;;   ((l2 (:pair <object> <list>))      
    ;;    (if (core-lambda-proc? (car l2))
    ;;        (interpret-core (cons (car l2) (cdr l2)))
    ;;        (syntax-violation '$call "invalid procedure" l-args)))
    ;;   (else
    ;;    (syntax-violation '$call "syntax error" l-args))))
  
  (define-simple-proc $apply0 (((l-args (rest <object>))) <object> nonpure)
    (assert (not-null? l-args))
    (match-type (d-car l-args)
      ((proc <general-proc>)
       (apply-nonpure proc (cast <list> (d-cadr l-args))))
      ((x <object>)
       (if (core-lambda-proc? x)
           (interpret-core (cons x (d-cadr l-args)))
           (syntax-violation '$apply0 "invalid procedure" l-args)))))
    ;; (match-type l-args
    ;;   ((l1 (:tuple <general-proc> <list>))
    ;;    (apply-nonpure (car l1) (cadr l1)))
    ;;   ((l2 (:tuple <object> <list>))      
    ;;    (if (core-lambda-proc? (car l2))
    ;;        (interpret-core `(,(car l2) ,@(cadr l2)))
    ;;        (syntax-violation '$apply0 "invalid procedure" l-args)))
    ;;   (else
    ;;    (syntax-violation '$apply0 "syntax error" l-args))))
  
  (define-simple-proc $apply1 (((l-args (rest <object>))) <object> nonpure)
    (if (equal? (length l-args) 3)
        (match-type (d-car l-args)
          ((proc <general-proc>)
           (apply-nonpure proc (cons (d-cadr l-args) (cast <list> (d-caddr l-args)))))
          ((x <object>)
           (if (core-lambda-proc? x)
               (interpret-core
                (cons x
                      (cons (d-cadr l-args)
                            (d-caddr l-args))))
               (syntax-violation '$apply1 "invalid procedure" l-args))))
        (syntax-violation '$apply1 "syntax error" l-args)))
  ;; (match-type l-args
    ;;   ((l1 (:tuple <general-proc> <object> <list>))
    ;;    (apply-nonpure (car l1) (cons (cadr l1) (caddr l1))))
    ;;   ((l2 (:tuple <object> <object> <list>))      
    ;;    (if (core-lambda-proc? (car l2))
    ;;        (interpret-core `(,(car l2) ,(cadr l2) ,@(caddr l2)))
    ;;        (syntax-violation '$apply1 "invalid procedure" l-args)))
    ;;   (else
    ;;    (syntax-violation '$apply1 "syntax error" l-args))))
  
  (define-simple-proc make-apply-arglist (((l-args <list>)) <list> pure)
    (if (null? l-args)
        null
        (if (null? (d-cdr l-args))
            (cast <list> (d-car l-args))
            (cons (d-car l-args) (make-apply-arglist (drop l-args 1))))))
    ;; (match-type l-args
    ;;   ((<null>) null)
    ;;   ((l2 <nonempty-list>)
    ;;    (if (null? (cdr l2))
    ;;        ;; The last element of an apply argument list has to be a list.
    ;;        (cast <list> (car l2))
    ;;        (cons (car l2) (make-apply-arglist (cdr l2)))))))
  
  (define-simple-proc $apply (((l-args (rest <object>))) <object> nonpure)
    (if (not-null? l-args)
        ($apply0 (d-car l-args) (make-apply-arglist (drop l-args 1)))
        (syntax-violation '$apply "syntax error" l-args)))
    ;; (match-type l-args
    ;;   ((l1 <nonempty-list>)
    ;;    ($apply0 (car l1) (make-apply-arglist (cdr l1))))
    ;;   (else
    ;;    (syntax-violation '$apply "syntax error" l-args))))
  
  (define-simple-proc $for-all (((l-args (rest <object>))) <object> nonpure)
    (if (equal? (length l-args) 2)
        (let ((proc (d-car l-args))
              (l2 (d-cadr l-args)))
          (if (null? l2)
              #t
              (and-object ($call proc (d-car l2))
                          ($for-all proc (d-cdr l2)))))
        (syntax-violation '$for-all "syntax error" l-args)))
    ;; (match-type l-args
    ;;   ((l1 (:tuple <object> <list>))
    ;;    (match-type (cadr l1)
    ;;      ;; $for-all is #t for an empty argument list.
    ;;      ((<null>) #t)
    ;;      ((l2 <nonempty-list>)
    ;;       (and-object ($call (car l1) (car l2))
    ;;                   (apply-nonpure $for-all (list (car l1) (cdr l2)))))))
    ;;   (else
    ;;    (syntax-violation '$for-all "syntax error" l-args))))
  
  (define-simple-proc $map-while (((l-args (rest <object>))) <object> nonpure)
    (if (equal? (length l-args) 3)
        (let ((f (d-car l-args))
              (lst (d-cadr l-args))
              (k (d-caddr l-args)))
          (match-type lst
            ((<null>) ($call k null null))
            ((p <pair>)
             (let ((head ($call f (car p))))
               (if-object
                 head
                 ($map-while f
                             (cdr p)
                             (lambda (((l-args2 (rest <object>)))
                                      <object> nonpure)
                               (if (equal? (length l-args2) 2)
                                   ($call k (cons head (d-car l-args2))
                                          (d-cadr l-args2))
                                   (syntax-violation '$map-while
                                                     "internal syntax error"
                                                     l-args2))))
                 ;;    (match-type l-args2
                 ;;      ((l2 (:tuple <object> <object>))
                 ;;       ($call k (cons head (car l2))
                 ;;              (cadr l2)))
                 ;;      (else
                 ;;       (syntax-violation '$map-while
                 ;;                         "internal syntax error"
                 ;;                         l-args2)))))
                 ($call k null lst))))
            (else ($call k null lst))))
        (syntax-violation '$map-while
                          "syntax error"
                          l-args)))
  
  (define-simple-proc $append (((l-args (rest <object>))) <object> nonpure)
    (d-append0 l-args))
    ;; (match-type l-args
    ;;   ((l1 (:uniform-list <list>))
    ;;    (append-uniform0 l1))
    ;;   (else 
    ;;    (syntax-violation '$append "syntax error" l-args))))
  
  (define-simple-proc $syntax-violation (((l-args (rest <object>))) <object>
                                                                    nonpure)
    (match-type l-args
      ((l1 (:tuple <object> <string> <object>))
       (apply-nonpure syntax-violation l1)
       null)
      (else 
       (syntax-violation '$syntax-violation "syntax error" l-args))))
  
  (define-simple-proc $get-usage-env (((l-args (rest <object>))) <object>
                                                                 nonpure)
    *usage-env*)
  
  (define-simple-proc $id-name (((l-args (rest <object>))) <object>
                                                           nonpure)
    (match-type l-args
      ((l1 (:tuple <identifier>))
       (field-ref (car l1) 's-name))
      (else 
       (syntax-violation '$id-name "syntax error" l-args))))
  
  (define-simple-proc $make-global-mapping
      (((l-args (rest <object>))) <object> nonpure)
    (match-type l-args
      ((l1 (:tuple <symbol> <identifier> (:union <integer> <boolean>)))
       (make-global-mapping (car l1) (cadr l1) (caddr l1)))
      (else 
       (syntax-violation '$make-global-mapping "syntax error" l-args))))
  
  (define-simple-proc $dotted-memp
      (((l-args (rest <object>))) <object> nonpure)
    (match-type l-args
      ((l1 (:tuple <general-proc> <object>))
       (dotted-memp (car l1) (cadr l1)))
      ((l2 (:tuple <object> <object>))
       (if (core-lambda-proc? (car l2))
           (dotted-memp (lambda (((l-args1 (rest <object>))) <object> nonpure)
                          (interpret-core (cons (car l2) l-args1)))
                        (cadr l2))
           (syntax-violation '$dotted-memp "invalid procedure" l-args)))
      (else
       (syntax-violation '$dotted-memp "syntax error" l-args))))
  
  (define-simple-proc $dotted-map (((l-args (rest <object>))) <object> nonpure)
    (match-type l-args
      ((l1 (:tuple <general-proc> <object>))
       (dotted-map (car l1) (cadr l1)))
      ((l2 (:tuple <object> <object>))
       (if (core-lambda-proc? (car l2))
           (dotted-map (lambda (((l-args1 (rest <object>))) <object> nonpure)
                         (interpret-core (cons (car l2) l-args1)))
                       (cadr l2))
           (syntax-violation '$dotted-map "invalid procedure" l-args)))
      (else
       (syntax-violation '$dotted-map "syntax error" l-args))))
  
  (define-simple-proc $dotted-length (((l-args (rest <object>))) <object>
                                                                 nonpure)
    (match-type l-args
      ((l1 (:tuple <object>))
       (dotted-length (car l1)))
      (else
       (syntax-violation '$dotted-length "syntax error" l-args))))
  
  (define-simple-proc $dotted-butlast (((l-args (rest <object>))) <object>
                                                                  nonpure)
    (match-type l-args
      ((l1 (:tuple <list> <integer>))
       (dotted-butlast (car l1) (cadr l1)))
      (else
       (syntax-violation '$dotted-butlast "syntax error" l-args))))
  
  (define-simple-proc $dotted-last (((l-args (rest <object>))) <object>
                                                               nonpure)
    (match-type l-args
      ((l1 (:tuple <list> <integer>))
       (dotted-last (car l1) (cadr l1)))
      (else
       (syntax-violation '$dotted-last "syntax error" l-args))))
  
  (define-simple-proc $syntax-rename
      (((l-args (rest <object>))) <object> nonpure)
    (match-type l-args
      ((l1 (:tuple <symbol>
                   (:uniform-list <macro-color>)
                   (:uniform-list (:union <macro-env> <symbol>))
                   <integer>
                   (:alt-maybe (:uniform-list <symbol>))
                   (:maybe <module-name>)))
       (apply syntax-rename l1))
      (else
       (syntax-violation '$syntax-rename "syntax error" l-args))))
  
  (define-simple-proc $make-variable-transformer
      (((l-args (rest <object>))) <object> nonpure)
    (match-type l-args
      ((l1 (:tuple <syntax-object>))
       (make-variable-transformer (car l1)))
      (else
       (syntax-violation '$make-variable-transformer "syntax error" l-args))))
  
  (define l-core-procs
    `(($cons . ,$cons)
      ($car . ,$car)
      ($cdr . ,$cdr)
      ($not . ,$not)
      ($pair? . ,$pair?)
      ($null? . ,$null?)
      ($list? . ,$list?)
      ($list . ,$list)
      ($for-all . ,$for-all)
      ($map . ,$map)
      ($apply . ,$apply)
      ($apply0 . ,$apply0)
      ($apply1 . ,$apply1)
      ($equal? . ,$equal?)
      ($= . ,$=)
      ($>= . ,$>=)
      ($> . ,$>)
      ($length . ,$length)
      ($append . ,$append)
      ($reverse . ,$reverse)
      ($+ . ,$+)
      ($- . ,$-)
      ($dotted-memp . ,$dotted-memp)
      ($dotted-map . ,$dotted-map)
      ($dotted-length . ,$dotted-length)
      ($dotted-last . ,$dotted-last)
      ($dotted-butlast . ,$dotted-butlast)
      ($raise . ,$raise)
      ($identifier? . ,$identifier?)
      ($free-identifier=? . ,$free-identifier=?)
      ($free=? ,$free=?)
      ($syntax-rename . ,$syntax-rename)
      ($invalid-form . ,$invalid-form)
      ($map-while . ,$map-while)
      ($syntax-violation . ,$syntax-violation)
      ($generate-temporaries . ,$generate-temporaries)
      ($make-variable-transformer . ,$make-variable-transformer)
      ($get-usage-env . ,$get-usage-env)
      ($undefined . ,gl-undefined)))
  
  (define l-core-forms
    '($lambda $let if-object if begin set! quote))
  
  (define-simple-proc make-core-var (((x-value <object>))
                                     (:mutable-vector <object>)
                                     pure)
    (mutable-vector <object> x-value))
  
  (define-simple-proc core-var? (((x <object>)) <boolean> pure)
    (match-type x
      ((v (:mutable-vector <object>))
       (= (mutable-vector-length v) 1))
      (else #f)))
  
  (define-simple-proc set-core-var! (((x-var <object>) (x-value <object>))
                                     <none> nonpure)
    (match-type x-var
      ((v (:mutable-vector <object>))
       (assert (= (mutable-vector-length v) 1))
       (mutable-vector-set! v 0 x-value))
      (else (raise-simple 'invalid-core-variable))))
  
  (define-simple-proc get-core-var-value (((x-var <object>)) <object> pure)
    (match-type x-var
      ((v (:mutable-vector <object>))
       (assert (= (mutable-vector-length v) 1))
       (mutable-vector-ref v 0))
      (else (raise-simple 'invalid-core-variable))))
  
  (define-simple-proc interpret-sequence (((x-subexprs <object>)
                                           (al-env <prim-env>))
                                          <object> nonpure)
    (let-mutable ((x-result <object> null))
      (do ((x-cur <object> x-subexprs (d-cdr x-cur))) ((null? x-cur))
        (set! x-result (interpret-core0 (d-car x-cur) al-env)))
      x-result))
  
  (define-simple-proc interpret-lambda (((x-expr <object>)
                                         (al-env <prim-env>))
                                        <object> nonpure)
    (strong-assert (and (>= (d-length x-expr) 3)
                        (equal? (d-car x-expr) '$lambda)))
    (let ((x-arg-names (d-cadr x-expr)))
      (match-type x-arg-names
        ((l-arg-names (:uniform-list <symbol>))
         (let* ((l-new-variables (map1 make-core-var l-arg-names))
                (al-free-vars-new al-env)
                (x-body0 (d-cddr x-expr))
                (proc
                 (lambda (((l-args (rest <object>))) <object> nonpure)
                   (let ((al-env2
                          (append-uniform2
                           (map-cons l-arg-names
                                     (static-cast <list> (map1 make-core-var l-args)))
                           al-free-vars-new)))
                     (interpret-sequence x-body0 al-env2)))))
           proc))
        ((s-arg-name <symbol>)
         (let* ((x-new-variable (make-core-var s-arg-name))
                (al-free-vars-new al-env)
                (x-body0 (d-cddr x-expr))
                (proc
                 (lambda (((l-args (rest <object>))) <object> nonpure)
                   (let ((al-env2
                          (cons
                           (cons s-arg-name (make-core-var l-args))
                           al-free-vars-new)))
                     (interpret-sequence x-body0 al-env2)))))
           proc))
        (else (syntax-violation '$lambda "Syntax error" x-expr)))))
  
  (define-simple-proc interpret-let (((x-expr <object>)
                                      (al-env <prim-env>))
                                     <object> nonpure)
    (strong-assert (and (>= (d-length x-expr) 3)
                        (equal? (d-car x-expr) '$let)))
    (let ((x-bindings (d-cadr x-expr))
          (x-body (d-cddr x-expr)))
      (let ((x-var-names (d-map1 d-car x-bindings))
            (x-values (d-map1 d-cadr x-bindings)))
        (interpret-core0
         `(($lambda ,x-var-names ,@x-body) ,@x-values)
         al-env))))
  
  (define-simple-proc interpret-if-object (((x-expr <object>)
                                            (al-env <prim-env>))
                                           <object> nonpure)
    (let ((i-len (d-length x-expr)))
      (assert (or (= i-len 4) (= i-len 3)))
      (let ((x-cond (interpret-core0 (d-cadr x-expr) al-env)))
        (if-object x-cond
          (interpret-core0 (d-caddr x-expr) al-env)
          (if (= i-len 4)
              (interpret-core0 (d-cadddr x-expr) al-env)
              gl-undefined)))))
  
  (define-simple-proc interpret-if (((x-expr <object>)
                                     (al-env <prim-env>))
                                    <object> nonpure)
    (let ((i-len (d-length x-expr)))
      (assert (or (= i-len 4) (= i-len 3)))
      (let ((x-cond (interpret-core0 (d-cadr x-expr) al-env)))
        (match-type x-cond
          ((b-cond <boolean>)
           (if b-cond
               (interpret-core0 (d-caddr x-expr) al-env)
               (if (= i-len 4)
                   (interpret-core0 (d-cadddr x-expr) al-env)
                   gl-undefined)))
          (else 
           (syntax-violation 'if "if condition not boolean" x-expr))))))
  
  (define-simple-proc interpret-set (((x-expr <object>)
                                      (al-env <prim-env>))
                                     <object> nonpure)
    (assert (= (d-length x-expr) 3))
    (let* ((s-var-name (cast <symbol> (d-cadr x-expr)))
           (p-binding (assoc-objects s-var-name al-env #f)))
      (match-type p-binding
        ((b <boolean>)
         (assert (equal? b #f))
         (syntax-violation 'set! "Unbound identifier" x-expr))
        ((p (:pair <symbol> <object>))
         (let ((x-var (cdr p))
               (x-value
                (interpret-core0 (d-caddr x-expr) al-env)))
           (set-core-var! x-var x-value))
         null))))
  
  (define-simple-proc interpret-var-ref (((s-var-name <symbol>)
                                          (al-env <prim-env>))
                                         <object> nonpure)
    (match-type (assoc-objects s-var-name al-env #f)
      ((b <boolean>)
       (assert (equal? b #f))
       (syntax-violation #f "Unbound identifier" s-var-name))
      ((p (:pair <symbol> <object>))
       (let ((x-var (cdr p)))
         (if (core-var? x-var)
             (get-core-var-value x-var)
             x-var)))))
  
  (define-simple-proc cast12 (((x <object>)) <general-proc>
                                             pure)
    (cast <general-proc> x))
  
  (define-simple-proc is-instance1? (((x <object>)) <boolean>
                                                    pure)
    (is-instance? x <general-proc>))
  
  (define-simple-proc equal1? (((x1 <object>) (x2 <object>)) <boolean> pure)
    (let ((b-result (equal? x1 x2)))
      (force-pure-expr (d2wl 'xxx ""))
      b-result))

  (define-simple-proc interpret-core0-pair
      (((x-expr <object>) (al-env <prim-env>)) <object> nonpure)
    (if (list? x-expr)
        (let* ((x-head (d-car x-expr))
               (x-tail (d-cdr x-expr))
               (x-first (interpret-core0 x-head al-env))
               (x-expr1 (cons x-first x-tail)))
          (match-type x-first
            ((s-first <symbol>)
             (case s-first
               (($lambda) (interpret-lambda x-expr1 al-env))
               (($let) (interpret-let x-expr1 al-env))
               ((if-object) (interpret-if-object x-expr1 al-env))
               ((if) (interpret-if x-expr1 al-env))
               ((begin) (interpret-sequence x-tail al-env))
               ((set!) (interpret-set x-expr1 al-env))
               ((quote)
                (if (and (pair? x-tail) (null? (d-cdr x-tail)))
                    (d-car x-tail)
                    (syntax-violation 'quote "Syntax error"
                                      x-expr)))
               (else
                (syntax-violation #f "Unexpected symbol" x-first))))
            ((id-first <identifier>)
             (let* ((l-arguments
                     (d-map-nonpure1
                      (lambda (((x-subexpr <object>)) <object>
                                                      nonpure)
                        (interpret-core0 x-subexpr al-env))
                      x-tail))
                    (ht-syntax-source
                     (field-ref gl-expander 'ht-syntax-source))
                    ;; (x-old-orig
                    ;;  (match-type ht-syntax-source
                    ;;    ((<null>) #f)
                    ;;    ((ht (:object-hash-table <object>))
                    ;;     (hash-ref ht x-expr #f))))
                    (x-result2 (cons id-first l-arguments)))
               (match-type ht-syntax-source
                 ((<null>) #f)
                 ((ht (:object-hash-table <object>))
                  (let ((x-old-orig (hash-ref ht x-expr #f)))
                    (if-object x-old-orig
                      (hash-set! ht x-result2 x-old-orig)))))
               ;;    (if-object x-old-orig
               ;;      (hash-set!
               ;;       (cast (:object-hash-table <object>)
               ;;             ht-syntax-source)
               ;;       x-result2 x-old-orig))
               x-result2))
            ((<null>)
             (let* ((l-arguments
                     (d-map-nonpure1
                      (lambda (((x-subexpr <object>)) <object>
                                                      nonpure)
                        (interpret-core0 x-subexpr al-env))
                      x-tail))
                    (ht-syntax-source
                     (field-ref gl-expander 'ht-syntax-source))
                    ;; (x-old-orig
                    ;;  (match-type ht-syntax-source
                    ;;    ((<null>) #f)
                    ;;    ((ht (:object-hash-table <object>))
                    ;;     (hash-ref ht x-expr #f))))
                    (x-result2 (cons null l-arguments)))
            ;;    (if-object x-old-orig
            ;;      (hash-set!
            ;;       (cast (:object-hash-table <object>)
            ;;             ht-syntax-source)
            ;;       x-result2 x-old-orig))
               (match-type ht-syntax-source
                 ((<null>) #f)
                 ((ht (:object-hash-table <object>))
                  (let ((x-old-orig (hash-ref ht x-expr #f)))
                    (if-object x-old-orig
                      (hash-set! ht x-result2 x-old-orig)))))
               x-result2))
            ((x-first1 <general-proc>)
             (let ((l-arguments
                    (d-map-nonpure1
                     (lambda (((x-subexpr <object>)) <object>
                                                     nonpure)
                       (interpret-core0 x-subexpr al-env))
                     x-tail)))
               (apply-nonpure x-first1
                              l-arguments)))
            (else
             ;; (let ((x-new-first (interpret-core0 x-first al-env)))
             ;;   (if (equal1? x-new-first x-first)
             ;;       x-expr
             ;;       (let ((x-result
             ;; 	      (interpret-core0 (cons x-new-first
             ;; 				     (d-cdr x-expr))
             ;; 			       al-env)))
             ;; 	 x-result))))))
             x-expr)))
        (cons (interpret-core0 (d-car x-expr) al-env)
              (interpret-core0 (d-cdr x-expr) al-env))))
  
  (define-simple-proc interpret-core0
      (((x-expr <object>) (al-env <prim-env>)) <object> nonpure)
    (set! gl-i-call-counter1 (+ gl-i-call-counter1 1 ))
    (let ((i-old-indent gl-i-indent))
      (set! gl-i-indent (+ gl-i-indent 1))
      ;; (d2wi 'macros2 "interpret-core0 input: ")
      ;; (d2wl 'macros2 x-expr)
      (let ((x-result1
             (if (pair? x-expr)
                 (interpret-core0-pair x-expr al-env)
                 (match-type x-expr
                   ((<null>) x-expr)
                   ((<boolean>) x-expr)
                   ((s <symbol>)
                    (if (member-objects? s l-core-forms)
                        s
                        (interpret-var-ref s al-env)))
                   ((<integer>) x-expr)
                   ((<real>) x-expr)
                   ((<string>) x-expr)
                   ((<character>) x-expr)
                   ((<identifier>) x-expr)
                   ((<procedure>) x-expr)
                   ((<keyword>) x-expr)
                   (else
                    (syntax-violation #f "Core expression syntax error"
                                      x-expr))))))
        ;; (d2wi 'macros2 "interpret-core0 output: ")
        ;; (d2wl 'macros2 x-result1)
        ;; (set! gl-i-counter30 (+ gl-i-counter30 1))
        ;; (d2wl 'macros2 gl-i-counter30)
        ;; (if (= gl-i-counter30 363)
        ;;     (begin
        ;;       (d2wi 'macros2 "previous input:")
        ;;       (d2wi 'macros2 x-expr)))
        (set! gl-i-indent i-old-indent)
        x-result1)))
  
  (define-simple-proc interpret-core (((x-expr <object>))
                                      <object> nonpure)
    (interpret-core0 x-expr l-core-procs)))
