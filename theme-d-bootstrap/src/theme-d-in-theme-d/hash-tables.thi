
;; Copyright (C) 2017, 2021, 2024, 2025 Tommi Höynälänmaa

(define-interface (theme-d-in-theme-d hash-tables)

  (import (standard-library core)
	  (theme-d-in-theme-d entities)
	  (theme-d-in-theme-d expressions)
	  (theme-d-in-theme-d common))
  
  (import-and-reexport (standard-library hash-table-opt))

  (define-param-logical-type :address-hash-table (%value)
    (:hash-table <address> %value))

  (declare make-address-hash-table-ws
	   (:param-proc (%value) (<integer>)
			(:address-hash-table %value)
			pure))

  (declare address-hash (:simple-proc (<address> <integer>) <integer> pure))

  (declare make-objectq-hash-table
	   (:param-proc (%value) () (:object-hash-table %value) pure))

  (declare make-objectq-hash-table-ws
	   (:param-proc (%value) (<integer>) (:object-hash-table %value)
			pure))

  (define-param-logical-type :content-hash-table (%key %value)
    (:hash-table %key %value))

  (declare make-content-hash-table-ws
	   (:param-proc (%key %value) (<integer>)
			(:content-hash-table %key %value)
			pure))

  (define-param-logical-type :module-hash-table (%value)
    (:content-hash-table (:maybe <module-name>) %value))

  (declare make-module-hash-table-ws
	   (:param-proc (%value) (<integer>)
			(:module-hash-table %value)
			pure))

  (define-param-logical-type :sexpr-hash-table (%value)
    (:hash-table <source-expr> %value))

  (declare make-sexprq-hash-table-ws
	   (:param-proc (%value) (<integer>)
			(:sexpr-hash-table %value)
			pure))

  (declare make-fw-hash-table-ws
	   (:simple-proc (<integer>) (:hash-table <target-object> <address>)
			 pure))

  (declare make-opt-arg-hash-table-ws
	   (:simple-proc (<integer>)
			 (:hash-table <target-object> <procedure-expression>)
			 pure))

  (declare make-goops-class-hash-table-ws
	   (:simple-proc (<integer>)
			 (:hash-table <target-object> <symbol>)
			 pure))

  (declare hash-max-alist-length
    (:param-proc (%key %value) ((:hash-table %key %value)) <integer> pure))

  (declare display-ht-info
    (:param-proc (%key %value) (<string> (:hash-table %key %value))
    <none> nonpure))

  (declare display-ht-info1
    (:param-proc (%key %value) (<string> (:maybe (:hash-table %key %value)))
    <none> nonpure)))

