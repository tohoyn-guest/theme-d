
;; Copyright (C) 2017, 2025 Tommi Höynälänmaa

(define-interface (theme-d-in-theme-d entities)

  (import (standard-library core)
          (standard-library mutable-pair)
          (standard-library singleton)
          (theme-d-in-theme-d common))
  
  (declare <target-object> <normal-class>)

  (declare <address> <normal-class>)

  (define-class <entity>
    (fields
     (tt-type (:union <target-object> <boolean>) public public)
     (type-dispatched? <boolean> public public)
     (exact-type? <boolean> public public)
     (address (:maybe <address>) public public)))

  (define-class <expression>
    (superclass <entity>)
    (fields
     (pure? <boolean> public public)
     (need-revision? <boolean> public public)
     (to-value (:maybe <target-object>) public public)))

  (define-class <target-object>
    (superclass <entity>)
    (inheritance-access hidden)
    (fields
     (primitive? <boolean> public public)
     (incomplete? <boolean> public public)
     (al-field-values (:union (:mutable-alist <symbol> <object>) <boolean>)
              public public)
     (sx-prim-contents <source-expr> public public)
     (x-opt-contents <object> public public)))

  (define <target-object-list> (:uniform-list <target-object>))

  (define <target-object-alist> (:alist <target-object> <target-object>))

  (define <target-object-pair> (:pair <target-object> <target-object>))

  (define <nonempty-to-list> (:nonempty-uniform-list <target-object>))

  (define <entity-list> (:uniform-list <entity>))

  (declare is-target-object? (:simple-proc (<object>) <boolean> pure))
  
  (declare target-object=? (:procedure (<target-object> <target-object>)
                       <boolean> pure))

  (declare is-t-primitive-object? (:simple-proc (<object>) <boolean> pure))

  (declare is-primitive-target-object?
       (:simple-proc (<target-object>) <boolean> pure))

  (define-class <address>
    (attributes equal-by-value)
    (inheritance-access hidden)
    (fields
     (mn <general-module-name> public public)
     (i-number <integer> public public)
     (s-source-name (:maybe <symbol>) public public)
     (toplevel? <boolean> public public)
     (s-target-name (:maybe <symbol>) public public)))

  (declare i-address-number-builtin <integer>)

  (declare i-address-number-target <integer>)

  (declare i-address-number-keyword <integer>)

  (declare check-address? (:simple-proc (<address>) <boolean> pure))

  (declare check-address (:simple-proc (<address>) <none> pure))

  (declare address=? (:procedure (<address> <address>) <boolean> pure))

  (declare is-my-main-address? (:procedure (<address>) <boolean> pure))
  
  (declare alloc-builtin-loc (:simple-proc (<symbol>) <address> pure))

  (declare alloc-builtin-raw-loc (:simple-proc (<symbol>) <address> pure))

  (declare alloc-target-prim-loc (:simple-proc (<symbol>) <address> pure))

  (define-class <variable>
    (superclass <entity>))

  (define-class <normal-variable>
    (superclass <variable>)
    (inheritance-access hidden)
    (fields
     (read-only? <boolean> public public)
     (volatile? <boolean> public public)
     (forward-decl? <boolean> public public)
     (letrec-variable? <boolean> public public)
     (to-value (:maybe <target-object>) public public)
     (ent-value-expr (:union <expression> <target-object> <null>) public public)
     (prevent-export? <boolean> public public)
     (changed-in-inst? <boolean> public public)))

  (define <normal-variable-list> (:uniform-list <normal-variable>))
  
  (define <method-object> (:pair <target-object> <boolean>))

  (define <method-object-list> (:uniform-list <method-object>))
  
  (define <nonempty-method-object-list>
    (:nonempty-uniform-list <method-object>))

  (define <argument-list> (:union <entity> <entity-list>))

  (declare make-empty-to-alist-sgt
    (:procedure () (:singleton <target-object-alist>) pure))

  (declare is-method-object? (:simple-proc (<object>) <boolean> pure))
  
  (declare variable-addresses-equal?
       (:simple-proc (<variable> <variable>) <boolean> pure))

  (declare target-field-ref (:simple-proc (<target-object> <symbol>)
                      <object> pure))

  (declare target-field-set!
       (:simple-proc (<target-object> <symbol> <object>)
             <none> nonpure))

  (declare to-target-field-ref (:simple-proc (<target-object> <symbol>)
                         <target-object> pure))

  (declare to0-target-field-ref (:simple-proc (<target-object> <symbol>)
                          (:maybe <target-object>) pure))
  
  (declare boolean-target-field-ref (:simple-proc (<target-object> <symbol>)
                          <boolean> pure))

  (declare symbol-target-field-ref (:simple-proc (<target-object> <symbol>)
                         <symbol> pure))

  (declare string-target-field-ref (:simple-proc (<target-object> <symbol>)
                         <string> pure))

  (declare l-to-target-field-ref (:simple-proc (<target-object> <symbol>)
                           <target-object-list> pure))

  (declare make-normal-variable0
       (:simple-proc (<address> <target-object>
                    <boolean> <boolean> <boolean> <boolean>
                    <boolean> <boolean>
                    (:maybe <target-object>)
                    (:union <expression> <target-object> <null>)
                    <boolean> <boolean>)
             <normal-variable> pure))

  (declare make-normal-variable
       (:simple-proc (<address> <target-object>
                    <boolean> <boolean> <boolean>
                    (:maybe <target-object>)
                    <boolean>)
             <normal-variable> pure))

  (declare make-normal-variable1
       (:simple-proc (<address> <target-object>
                    <boolean> <boolean>
                    (:maybe <target-object>))
             <normal-variable> pure))

  (declare make-normal-variable2
       (:simple-proc (<address> <target-object>
                    <boolean> <boolean> <boolean>
                    (:maybe <target-object>)
                    (:union <expression> <target-object> <null>)
                    <boolean>)
             <normal-variable> pure))

  (declare make-normal-variable3
       (:simple-proc (<address> <target-object>
                    <boolean> <boolean> <boolean> <boolean>
                    (:maybe <target-object>)
                    (:union <expression> <target-object>
                        <null>))
             <normal-variable> pure))

  (declare make-normal-variable4
       (:simple-proc (<address> <target-object>
                    <boolean> <boolean> <boolean> <boolean>
                    (:maybe <target-object>)
                    <boolean>)
             <normal-variable> pure))

  (declare make-normal-variable5
       (:simple-proc (<address> <target-object>
                    <boolean> <boolean> <boolean>
                    (:maybe <target-object>))
             <normal-variable> pure))

  (declare make-normal-variable6
       (:simple-proc (<address> <target-object>
                    <boolean> <boolean> <boolean>
                    <boolean> <boolean>
                    (:maybe <target-object>)
                    (:union <expression> <target-object>
                        <null>))
             <normal-variable> pure))

  (declare make-normal-variable7
       (:simple-proc (<address> <target-object>
                    <boolean> <boolean> <boolean>
                    <boolean>
                    (:maybe <target-object>)
                    (:union <expression> <target-object>
                        <null>)
                    <boolean>)
             <normal-variable> pure))


  (declare make-normal-variable8
       (:simple-proc (<address> <target-object>
                    <boolean> <boolean> <boolean>
                    <boolean>
                    (:maybe <target-object>)
                    <boolean>)
             <normal-variable> pure))

  (declare make-normal-variable10
       (:simple-proc (<address> <target-object>
                    <boolean> <boolean> <boolean> <boolean>
                    <boolean>
                    (:maybe <target-object>)
                    <boolean>)
             <normal-variable> pure))

  (declare get-object-repr (:simple-proc (<entity>) <target-object> pure))

  (declare get-entity-value (:simple-proc (<entity>) (:maybe <target-object>)
                      pure))

  (declare get-and-check-value (:simple-proc (<entity>) <target-object>
                         pure))

  (declare is-pure-entity? (:simple-proc (<entity>) <boolean> pure))

  (declare is-known-object? (:simple-proc (<object>) <boolean> pure))

  (declare is-known-target-object?
       (:simple-proc (<target-object>) <boolean> pure))

  (declare is-known-pure-entity? (:simple-proc (<entity>) <boolean> pure))

  (declare is-forward-decl-entity? (:simple-proc (<entity>) <boolean> pure))

  (declare is-forward-decl? (:simple-proc (<normal-variable>) <boolean> pure))

  (declare entity-needs-revision?
       (:procedure (<entity>) <boolean> pure))

  (declare entity-type-dispatched? (:simple-proc (<entity>) <boolean> pure))

  (declare is-type0? (:simple-proc (<object>) <boolean> pure))

  (declare is-t-true? (:procedure (<entity>) <boolean> pure))

  (declare is-t-false? (:procedure (<entity>) <boolean> pure))

  (declare is-null-obj? (:procedure (<object>) <boolean> pure))

  (declare make-object-with-address (:simple-proc (<target-object>
                           (:maybe <address>))
                          <target-object> pure))

  (declare set-object! (:simple-proc (<target-object> <target-object>)
                     <none> nonpure))

  (declare set-object1! (:simple-proc (<target-object> <target-object>)
                      <none> nonpure))

  (declare clone-object (:simple-proc (<target-object>) <target-object> pure))

  (declare rebind-variable!
       (:simple-proc (<normal-variable> <normal-variable>) <none>
             nonpure))

  (declare arglist-for-all1?
    (:simple-proc ((:unary-predicate <entity>) <argument-list>)
                  <boolean> pure))

  (declare arglist-exists1?
    (:simple-proc ((:unary-predicate <entity>) <argument-list>)
                 <boolean> pure)))

