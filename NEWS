2025-02-08

Optimize apply expressions.

2025-01-31

Improved handling unit types as procedure argument types and components
of procedure argument types.

2025-01-02

New release 6.0.0. See ChangeLog.

2024-12-29

Added form static-type-of0 and special procedure type-of. See
sections 6.15.8 and 7.3.4 in the new language manual.

2024-12-26

Optimized the default constructors.

2024-12-23

Implemented the keyword based make for Theme-D classes. The old-style make has
been renamed to "create". For more information, see:

- Sections 4.14 and 6.16.2 of the new language manual.
- Section 6.1 of the new standard library reference (for create).
- Test programs test892.thp, test894.thp, and test895.thp.

2024-12-19

- Implemented type checking for GOOPS foreign class make expressions.
- Implemented GOOPS class attribute pure-make.
- Bug fixes.

2024-12-13

The prerelease 6.0.0~pre7 has been published. See

https://www.iki.fi/tohoyn/theme-d/download.html.

Changes to version 6.0.0~pre6:

- The bootstrapped compiler and linker work.

2024-12-08

Use eqv? as the default implementation of the equal-objects? predicate
for foreign classes. This can be changed by class attribute use-eq?.

2024-12-01

Fixed a bug in generic procedure dispatch in the BST.

Fixed a bug in the BST Tree-IL backend.

2024-11-30

Fixes to the BST linker.

More optimizations to match-type and match-type-strong.

2024-11-29

Optimized cycle detection in subtype checking and type deduction.

Optimized methods reverse and d-reverse in the standard library.

Added methods d-fold1 and d-fold-right1 into the standard library.

2024-11-28

Optimized match-type and match-type-strong.

2024-11-27

Fixed a bug in the equality predicate optimization.

2024-11-24

Optimized classes that are not atomic and not foreign GOOPS classes to use
eq? as the implementation for predicate equal-objects?.

2024-11-22

Improved the libthemedsupport library by using source code of Guile 3.0.10.

2024-11-22

Allow the use of translator keyword as symbols.

Allow the use of non-class types in forward declarations.

2024-11-20

Runtime type check for slot-ref.

Added methods union-of-lists and union-of-alists into the standard library.

2024-11-20

The prerelease 6.0.0~pre6 has been published. See

https://www.iki.fi/tohoyn/theme-d/download.html.

Changes to version 6.0.0~pre5:

- Fixed bugs with string unit types.
- Renewed the foreign function interface for GOOPS classes. See the Language
  Manual.

2024-11-18

Use optimization level 2 for the translator compilation by default.
Renamed configure option opt-compilation to xlat-opt-compilation.

Bug fixes for type equality in the RTE.

Detect unit types as final.

2024-11-17

The prerelease 6.0.0~pre5 has been published. See

https://www.iki.fi/tohoyn/theme-d/download.html.

Changes to version 6.0.0~pre4:
- Fixed a bug with string unit types.
- Added procedures fold1, fold-right1, delete-duplicates, and list-set-diff
  into the the standard library.

2024-11-17

Added methods fold1 and fold-right1 into module list-utilities.
Added example program fold.thp.

2024-11-16

- The prerelease 6.0.0~pre4 has been published. See

https://www.iki.fi/tohoyn/theme-d/download.html.

Changes to version 6.0.0~pre3:
- Fixed subexpression factorization.
- Fixed a bug with unit types in RTE.
- Fixed form debug-output.
- Renamed define-prim-class to define-foreign-prim-class.
- Use option --warn=shadowed-toplevel in the compilation of the translator and
  in the RTE. The option is also used in the target compilation of Theme-D
  programs.

- Renamed define-prim-class to define-foreign-prim-class.

- Fixed subexpression factorization.
- Fixed a bug with unit types in the RTE.
- Fixed form debug-output.

2024-11-15

Fixed split linking.

Improved script compute-theme-d-deps.scm and added a man page for it.

Fixed "make install" and added some directory options to configure.ac.

2024-11-14

Added figures presenting generic procedure class hierarchy into the language
manual.
 
Fixed equivalence predicate optimization.

The language manual has been updated.

Some fixes to procedure equal-types0? used in equality predicates.

2024-11-13

The prerelease 6.0.0~pre3 has been published. See

https://www.iki.fi/tohoyn/theme-d/download.html.

2024-11-13

Renamed some standard library modules:
  hash-table -> hash-table0
  hash-table2 -> hash-table
  hash-table2-opt -> hash-table-opt

2024-11-13

Removed metaclasses :value-vector and :mutable-value-vector and changed
instances of :vector and :mutable-vector to be equal by value. Note that you can
compare vectors as objects with predicate equal-objects?.

2024-11-13

* Changes
- Simplified the implementation of equality predicates.

2024-11-12

* Changes
- Updated the User Guide.
- Changed procedure dispatch-normal-method-w-params.
- Updated file test-info.scm

* Notes

- Guile 2.0, Guile 2.2, and Racket platforms are no longer supported.

2024-11-11

The prerelease 6.0.0~pre2 has been published in the devel branch of the
Theme-D SourceForge repository,

https://sourceforge.net/p/theme-d/code/ci/master/tree/

* Changes
- Fixed bugs with runtime type variable allocation.
- Implemented unit types.
- Removed the restriction that declaration and definition of a variable must
  have equal class sizes.
- Use GOOPS classes in the runtime environment.

* Notes

- The code for the type system is now shared between the translator and the
runtime environment.

- The bootstrapped environment does not work yet.

- See https://en.wikipedia.org/wiki/Unit_type for information about unit
  types. See also example programs orientation1.thp, orientation2.thp, and
  bits.thp.

2024-11-02

* Changes
- Updated the Tree-IL backend to be compatible with the new RTE.
- Updated the nonoptimized Guile backend to be compatible with the new RTE.

2024-11-01

* Changes
- Refactorized the runtime environment. Currently only the optimized Guile
  backend is compatible with the new RTE.
  
2024-10-30

* Notes:
- The runtime environment compiles but does not work.

* Changes
- Changed the runtime environment to use GOOPS objects instead of vectors
  for Theme-D objects.
- Changed the representation of Theme-D vectors to use GOOPS objects
  in the RTE.
- Checked the use of equality predicates in the RTE.
- Fixed the hash function for the RTE parameterized instance cache.

2024-10-29

* Notes
- A working version. Compiler and linker work but runtime environment does not.

* Changes
- Fixed bugs in runtime type variable allocation.
- Implemented unit types. See example programs orientation1.thp,
  orientation2.thp, and bits.thp.
  See also https://en.wikipedia.org/wiki/Unit_type.
