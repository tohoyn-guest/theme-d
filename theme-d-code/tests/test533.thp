;; -*-theme-d-*-

;; Copyright (C) 2014  Tommi Höynälänmaa
;; Distributed under GNU General Public License version 3,
;; see file doc/GPL-3.

;; Expected results: translation error
;;   sequence-x is not a parametrized procedure

(define-proper-program (tests test533)


  (import (standard-library core)
	  (standard-library list-utilities)
	  (standard-library string-utilities)
	  (standard-library console-io)
	  (standard-library object-string-conversion))


  (define-param-class :sequence
    (parameters %element))

  (define-param-virtual-method sequence-ref (%element)
    (((seq (:sequence %element)) (index <integer>)) %element pure)
    (raise 'abstract-method))

  (define-param-virtual-method sequence-length (%element)
    (((seq (:sequence %element))) <integer> pure)
    (raise 'abstract-method))

  (define-param-virtual-method sequence-x (%source)
    (((proc (:procedure (%source) <none> nonpure)) (seq (:sequence %source)))
     <none> nonpure)
    (raise 'abstract-method))


  (define-param-class :list-as-sequence
    (parameters %element)
    (superclass (:sequence %element))
    (fields
     (contents (:uniform-list %element) module hidden)))

  (define-param-virtual-method sequence-ref (%element)
    (((seq (:list-as-sequence %element)) (index <integer>)) %element pure)
    (uniform-list-ref (field-ref seq 'contents) index))

  (define-param-virtual-method sequence-length (%element)
    (((seq (:list-as-sequence %element))) <integer> pure)
    (length (field-ref seq 'contents)))

  (define-param-proc myproc (%type) (() <none> nonpure)
    (console-display-line "Hello"))

  (define-param-virtual-method sequence-x (%source)
    (((proc (:procedure (%source) <none> nonpure))
      (seq (:list-as-sequence %source)))
     <none> nonpure)
    (myproc))

  (define-param-proc display-sequence (%element)
  		     (((seq (:sequence %element))) <none> nonpure)
    (let ((len (sequence-length seq)))
      (do ((i <integer> 0 (+ i 1))) ((>= i len))
  	(console-display (sequence-ref seq i))
  	(console-newline))))

  
  (define-simple-proc my-proc (((n <integer>)) <string> pure)
    (string-append "*" (integer->string n) "*"))


  (define-main-proc (() <none> nonpure)
    (let* ((lst '(1 3 5 2 4))
	   (seq (create (:list-as-sequence <integer>) lst)))
      ((param-proc-instance sequence-x <integer>) myproc seq))))

