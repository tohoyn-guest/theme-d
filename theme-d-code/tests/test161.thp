;; -*-theme-d-*-

;; Copyright (C) 2014, 2024  Tommi Höynälänmaa
;; Distributed under GNU General Public License version 3,
;; see file doc/GPL-3.

;; Expected results: translation and running OK


(define-proper-program (tests test161)
  
  
  (import (standard-library core)
          (standard-library console-io))
  
  
  (define-param-proc-alt my-vectexists (%type1 %type2)
    (lambda (((proc (:procedure (%type1) %type2 nonpure))
              (v (:vector %type1)))
             (:vector %type2) nonpure)
      (let* ((len (vector-length v))
             (result (make-mutable-vector (:maybe %type2) len null)))
        (let-mutable ((i <integer> 0))
          (until ((>= i len)
                  (cast-vector
                   %type2
                   (cast-vector-metaclass result)))
            (mutable-vector-set!
             result i
             (proc (vector-ref v i)))
            (set! i (+ i 1)))))))
  
  
  (define my-func
    (lambda (((a <real>)) <real> pure)
      (+ a 1.5)))
  
  
  (define main
    (lambda (() <integer> nonpure)
      (let ((my-vec0 (make-mutable-vector <real> 5 0.0)))
        (mutable-vector-set! my-vec0 0 1.1)
        (mutable-vector-set! my-vec0 1 2.1)
        (mutable-vector-set! my-vec0 2 3.1)
        (mutable-vector-set! my-vec0 3 4.1)
        (mutable-vector-set! my-vec0 4 5.0)
        (let* ((my-vec (cast-vector-metaclass my-vec0))
               (result-vec (my-vectexists my-func my-vec))
               (len (vector-length result-vec)))
          (let-mutable ((j <integer> 0))
            (until ((>= j len))
              (console-display (vector-ref result-vec j))
              (console-newline)
              (set! j (+ j 1))))))
      0)))
