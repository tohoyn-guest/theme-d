;; -*-theme-d-*-

;; Copyright (C) 2015, 2024 Tommi Höynälänmaa
;; Distributed under GNU General Public License version 3,
;; see file doc/GPL-3.

;; Expected results: translation and running OK
;; Expected output: #f #t #f #t


(define-proper-program (tests test561)

  (import (standard-library core)
	  (standard-library console-io))

  (define-foreign-goops-class <a>
    (target-name <a>))

  (define make-a
    (prim-proc make-a (<integer>) <a> pure))

  (define get-x
    (prim-proc get-x (<a>) <integer> pure))

  (define set-x!
    (prim-proc set-x! (<a> <integer>) <none> nonpure))

  (define-foreign-goops-class <b>
    (target-name <b>)
    (superclass <a>))

  (define make-b
    (prim-proc make-b (<integer> <real>) <b> pure))

  (define get-y
    (prim-proc get-y (<b>) <real> pure))

  (define set-y!
    (prim-proc set-y! (<b> <real>) <none> nonpure))

  (define-simple-proc disp (((obj <object>)) <none> nonpure)
    (console-display obj)
    (console-newline))

  (define-main-proc (() <none> nonpure)
    (let ((a1 (make-a 10))
	  (a2 (make-a 10))
	  (b1 (make-b 5 -1.1))
	  (b2 (make-b 5 -1.1)))
      (disp (equal-objects? a1 a2))
      (disp (equal-objects? a1 a1))
      (disp (equal-objects? b1 b2))
      (disp (equal-objects? b1 b1)))))


