;; -*-theme-d-*-

;; Copyright (C) 2014, 2024  Tommi Höynälänmaa
;; Distributed under GNU General Public License version 3,
;; see file doc/GPL-3.

;; Expected results: translation and running OK


(define-proper-program (tests test223)


  (import (standard-library core)
	  (standard-library console-io))


  (define-foreign-prim-class <bytevector>
    (attributes equal-by-value checked)
    (member-pred bytevector?))
  

  (define make-bytevector
    (prim-proc make-bytevector (<integer> <integer>) <bytevector> (pure)))


  (define bytevector-length
    (prim-proc bytevector-length (<bytevector>) <integer> (pure)))


  (define bytevector-u8-ref
    (prim-proc bytevector-u8-ref (<bytevector> <integer>) <integer> (pure)))


  (define bytevector-u8-set!
    (prim-proc bytevector-u8-set! (<bytevector> <integer> <integer>) <none>
	       (nonpure)))


  (define-simple-proc display-bytevector (((bv <bytevector>)) <none> (nonpure))
    (let ((len (bytevector-length bv)))
      (do ((i <integer> 0 (+ i 1))) ((>= i len))
	(console-display (bytevector-u8-ref bv i))
	(console-newline))))


  (define main
    (lambda (() <none> nonpure)
      (let ((bv (make-bytevector 10 128)))
	(bytevector-u8-set! bv 5 20)
	(display-bytevector bv)))))
