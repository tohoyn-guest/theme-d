;; -*-theme-d-*-

;; Copyright (C) 2023 Tommi Höynälänmaa
;; Distributed under GNU General Public License version 3,
;; see file doc/GPL-3.

;; Expected results: translation and running OK


(define-proper-program (tests test843)

  (import (standard-library core)
	  (standard-library bvm-matrices)
	  (standard-library complex)
	  (standard-library console-io)
	  (tests numerical-test-env)
	  (tests matrix-test-env))

  (define-main-proc (() <none> nonpure)
    (let* ((cx0 (complex 0.0 0.0))
	   (cx1m1 (complex 1.0 -1.0))
	   (mx1 (bvm-make-matrix 3 3 (complex 2.0 1.5)))
	   (mx2
	    (bvm-matrix
	     (list
	      (list
	       (complex 1.0 -12.0) (complex 2.0 -11.0) (complex 3.0 -10.0))
	      (list
	       (complex 4.0 -9.0) (complex 5.0 -8.0) (complex 6.0 -7.0))
	      (list
	       (complex 7.0 -6.0) (complex 8.0 -5.0) (complex 9.0 -4.0))
	      (list
	       (complex 10.0 -3.0) (complex 11.0 -2.0) (complex 12.0 -1.0)))))
	   (mx3
	    (construct-complex-matrix
	     (bvm-matrix '((-2.0 1.0)
			   (3.5 1.5)
			   (-10.0 10.0)))
	     (bvm-matrix '((1.0 0.0)
			  (-3.5 1.0)
			  (0.0 1.0)))))
	   (mx4
	    (construct-complex-matrix
	     (bvm-matrix '((0.0 2.0)
			   (0.5 2.5)
			   (-1.0 9.0)))
	     (bvm-matrix '((10.0 1.0)
			  (0.0 -1.0)
			  (2.5 1.5)))))
	   (mx5 (bvm-copy mx1)))
      (bvm-fill! mx1 (complex 1.0 -2.0))
      (matrix-set! mx5 1 1 (complex 100.0 50.0))
      (report-matrix-test mx1 (bvm-make-matrix 3 3 (complex 1.0 -2.0)))
      (report-test (matrix-ref mx5 1 1) (complex 100.0 50.0))
      (report-test (number-of-rows mx3) 3)
      (report-test (number-of-columns mx3) 2)
      (report-matrix-test
       (+ mx3 mx4)
       (bvm-matrix (list
		    (list (complex -2.0 11.0) (complex 3.0 1.0))
		    (list (complex 4.0 -3.5) (complex 4.0 0.0))
		    (list (complex -11.0 2.5) (complex 19.0 2.5)))))
      (report-matrix-test
       (- mx3 mx4)
       (bvm-matrix (list
		    (list (complex -2.0 -9.0) (complex -1.0 -1.0))
		    (list (complex 3.0 -3.5) (complex -1.0 2.0))
		    (list (complex -9.0 -2.5) (complex 1.0 -0.5)))))
      (report-matrix-test
       (- mx3)
       (bvm-matrix (list
		    (list (complex 2.0 -1.0) (complex -1.0 0.0))
		    (list (complex -3.5 3.5) (complex -1.5 -1.0))
		    (list (complex 10.0 0.0) (complex -10.0 -1.0)))))
      (report-matrix-test
       (* mx2 mx3)
       (bvm-matrix
	(list
	 (list (complex -51.5 79.5) (complex 55.0 -123.5))
	 (list (complex -69.5 46.5) (complex 86.5 -80.0))
	 (list (complex -87.5 13.5) (complex 118.0 -36.5))
	 (list (complex -105.5 -19.5) (complex 149.5 7.0)))))
      (report-matrix-test
       (->complex-matrix mx4)
       mx4)
      (report-matrix-test
       (->single-complex-matrix mx4)
       (construct-complex-matrix
	(bvm-single-matrix '((0.0 2.0)
			     (0.5 2.5)
			     (-1.0 9.0)))
	(bvm-single-matrix '((10.0 1.0)
			     (0.0 -1.0)
			     (2.5 1.5)))))
      (report-boolean-test (= mx3 mx3) #t)
      (report-boolean-test (= mx3 mx4) #f)
      (report-matrix-test
       (* (complex 2.5 -1.5) mx3)
       (bvm-matrix
	(list
	 (list (complex -3.5 5.5) (complex 2.5 -1.5))
	 (list (complex 3.5 -14.0) (complex 5.25 0.25))
	 (list (complex -25.0 15.0) (complex 26.5 -12.5)))))
      (report-matrix-test
       (* mx3 (complex 2.5 -1.5))
       (bvm-matrix
	(list
	 (list (complex -3.5 5.5) (complex 2.5 -1.5))
	 (list (complex 3.5 -14.0) (complex 5.25 0.25))
	 (list (complex -25.0 15.0) (complex 26.5 -12.5)))))
      (report-matrix-test
       (transpose mx4)
       (bvm-matrix
	(list
	 (list (complex 0.0 10.0) (complex 0.5 0.0) (complex -1.0 2.5))
	 (list (complex 2.0 1.0) (complex 2.5 -1.0) (complex 9.0 1.5)))))
      (report-matrix-test (transpose mx1) mx1)
      (report-matrix-test
       (conj mx4)
       (bvm-matrix
	(list
	 (list (complex 0.0 -10.0) (complex 2.0 -1.0))
	 (list (complex 0.5 0.0) (complex 2.5 1.0))
	 (list (complex -1.0 -2.5) (complex 9.0 -1.5)))))
      (report-matrix-test
       (herm mx4)
       (bvm-matrix
	(list
	 (list (complex 0.0 -10.0) (complex 0.5 0.0) (complex -1.0 -2.5))
	 (list (complex 2.0 -1.0) (complex 2.5 1.0) (complex 9.0 -1.5)))))
      (report-matrix-test
       (herm mx1)
       (bvm-make-matrix 3 3 (complex 1.0 2.0)))
      (report-matrix-test
       (bvm-column-vector (list (complex 1.0 0.0)
				(complex 2.0 -1.0)
				(complex -3.0 10.0)))
       (bvm-matrix (list
		    (list (complex 1.0 0.0))
		    (list (complex 2.0 -1.0))
		    (list (complex -3.0 10.0)))))
      (report-matrix-test
       (bvm-row-vector (list (complex 1.0 0.0)
			     (complex 2.0 -1.0)
			     (complex -3.0 10.0)))
       (bvm-matrix
	(list
	 (list (complex 1.0 0.0) (complex 2.0 -1.0) (complex -3.0 10.0)))))
      (report-matrix-test
       (bvm-generate-matrix
	3
	4
	(lambda (((i1 <integer>) (i2 <integer>)) <complex> pure)
	  (if (= i1 i2) (complex 1.0 -1.0) (complex 0.0 0.0))))
       (bvm-matrix
	(list
	 (list (complex 1.0 -1.0) cx0 cx0 cx0)
	 (list cx0 (complex 1.0 -1.0) cx0 cx0)
	 (list cx0 cx0 (complex 1.0 -1.0) cx0))))
      (report-matrix-test
       (matrix-map c-square mx2)
       (bvm-matrix
	(list
	 (list (complex -143.0 -24.0) (complex -117.0 -44.0)
	       (complex -91.0 -60.0))
	 (list (complex -65.0 -72.0) (complex -39.0 -80.0)
	       (complex -13.0 -84.0))
	 (list (complex 13.0 -84.0) (complex 39.0 -80.0)
	       (complex 65.0 -72.0))
	 (list (complex 91.0 -60.0) (complex 117.0 -44.0)
	       (complex 143.0 -24.0)))))
      (report-matrix-test
       (matrix-map-w-ind
	(lambda (((i1 <integer>) (i2 <integer>) (cx-elem <complex>))
		 <complex> pure)
	  (if (= i1 i2) cx-elem cx1m1))
	mx2)
       (bvm-matrix
	(list
	 (list (complex 1.0 -12.0) cx1m1 cx1m1)
	 (list cx1m1 (complex 5.0 -8.0) cx1m1)
	 (list cx1m1 cx1m1 (complex 9.0 -4.0))
	 (list cx1m1 cx1m1 cx1m1))))
      (report-exception (+ mx2 mx3) 'bvm+:dim-mismatch)
      (report-exception (- mx2 mx3) 'bvm-:dim-mismatch)
      (report-exception (* mx3 mx4) 'bvm*:dim-mismatch)
      (report-exception
       (construct-complex-matrix
	(bvm-matrix '((1.0 2.0)))
	(bvm-matrix '((2.0) (3.0))))
       'construct-complex-matrix:dim-mismatch))))
