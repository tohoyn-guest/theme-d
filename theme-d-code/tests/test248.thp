;; -*-theme-d-*-

;; Copyright (C) 2014, 2024  Tommi Höynälänmaa
;; Distributed under GNU General Public License version 3,
;; see file doc/GPL-3.

;; Expected results: translation and running OK


(define-proper-program (tests test248)
  
  
  (import (standard-library core)
          (standard-library list-utilities)
          (standard-library matrix)
          (standard-library console-io))
  
  
  (define-param-proc my-gen-car (%type)
      (((lst (:uniform-list %type)))
       %type
       (pure))
    (if (null? lst)
        (raise 'my-gen-car:null)
        (car (cast (:nonempty-uniform-list %type) lst))))
  
  
  (define-param-proc my-gen-cdr (%type)
      (((lst (:uniform-list %type)))
       (:uniform-list %type)
       (pure))
    (if (null? lst)
        (raise 'my-gen-cdr:null)
        (cdr (cast (:nonempty-uniform-list %type) lst))))
  
  
  (define-param-proc my-matrix (%number)
      (((elements (:uniform-list (:uniform-list %number))))
       (:matrix %number)
       (force-pure))
    (let ((rows (length elements)))
      (assert (> rows 0))
      (let ((columns (length (my-gen-car elements))))
        ;;	(assert (for-all?
        ;;		 (lambda (((row (:uniform-list %number))) <boolean> (pure))
        ;;		   (= (length row) columns))
        ;;		 (cdr elements)))
        (let ((result (make-matrix rows columns (zero %number))))
          (do ((i-row <integer> 0 (+ i-row 1))
               (cur-elements (:uniform-list (:uniform-list %number))
                             elements (my-gen-cdr cur-elements)))
              ((>= i-row rows))
            (do ((i-column <integer> 0 (+ i-column 1))
                 (cur-elements2 (:uniform-list %number)
                                (my-gen-car cur-elements) (my-gen-cdr cur-elements2)))
                ((>= i-column columns))
              (matrix-set! result i-row i-column (my-gen-car cur-elements2))))
          result))))
  
  
  (define-param-proc display-matrix (%number)
      (((mx (:matrix %number)))
       <none>
       (nonpure))
    (let ((rows (field-ref mx 'rows))
          (columns (field-ref mx 'columns)))
      (do ((i1 <integer> 0 (+ i1 1))) ((>= i1 rows))
        (begin
         (do ((i2 <integer> 0 (+ i2 1))) ((>= i2 columns))
           (console-display (matrix-ref mx i1 i2))
           (if (< i2 (- columns 1))
               (console-display-string " ")))
         (console-newline)))))
  
  
  (define main
    (lambda (() <none> nonpure)
      (let ((mx1 (my-matrix
                  (static-cast
                   (:uniform-list (:uniform-list <real>))
                   (list
                    (list 1.0 0.0 2.0)
                    (list -1.5 2.5 0.0)
                    (list -3.0 -2.0 1.0)))))
            (mx2 (my-matrix
                  (static-cast
                   (:uniform-list (:uniform-list <real>))
                   (list
                    (list 0.0 0.0 -2.0)
                    (list 5.5 2.0 1.0)
                    (list 0.0 2.0 -1.0))))))
        (let ((result (* mx1 mx2)))
          (display-matrix result))))))
