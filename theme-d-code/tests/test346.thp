;; -*-theme-d-*-

;; Copyright (C) 2015, 2021, 2024 Tommi Höynälänmaa
;; Distributed under GNU General Public License version 3,
;; see file doc/GPL-3.

;; Expected results: translation and running OK


(define-proper-program (tests test346)


  (import (standard-library core)
	  (standard-library dynamic-list)
	  (standard-library console-io))


  (define-simple-method my-cons (((l (rest <object>))) <object> pure)
    (cons (d-list-ref l 0) (d-list-ref l 1)))


  (define-simple-method my-proc1 (((i <object>)) <object> pure)
    (let ((i-result (+ (cast <integer> i) 1)))
      i-result))


  (define-simple-method my-proc2 (((i <object>)) <object> nonpure)
    (let ((i-result (+ (cast <integer> i) 1)))
      (console-display-line i-result)
      i-result))


  (define-simple-method my-proc3 (((l (rest <object>)))
				<object>
				nonpure)
    (let ((pr-result (cons (d-list-ref l 0) (d-list-ref l 1))))
      (console-display-line pr-result)
      pr-result))


  (define-simple-method my-proc4 (((l (rest <object>)))
				<none>
				nonpure)
    (console-display (d-list-ref l 0))
    (console-display " ")
    (console-display-line (d-list-ref l 1)))


  (define-simple-method my-proc5 (((x1 <object>) (x2 <object>))
				<pair>
				nonpure)
    (let ((pr-result (cons x1 x2)))
      (console-display-line pr-result)
      pr-result))

  
  (define-main-proc (() <none> nonpure)
      (let ((l1 (d-list 1 2 3 4 5))
	    (l2 (d-list 'a 'b 'c 'd 'e))
	    (l3 (d-list "abc" "def"))
	    (l4 (d-list 3.0 4.0 5.0 6.0)))
	(console-display-line "*1*")
	(console-display-line (d-car l1))
	(console-display-line (d-cdr l2))
	(console-display-line (d-list-ref l1 2))
	(console-display-line (d-length l2))
	(console-display-line "*2*")
	(console-display-line (d-map1 my-proc1 l1))
	(console-display-line (d-map-nonpure1 my-proc2 l1))
	(d-for-each1 console-display-line l2)
	(console-display-line (d-map2 cons l1 l2))
	(console-display-line (d-map-nonpure2 my-proc5 l1 l2))
	(console-display-line (d-map my-cons l1 l2))
	(console-display-line (d-map-nonpure my-proc3 l1 l2))
	(d-for-each my-proc4 l1 l2)
	(d-for-each2 my-proc4 l1 l2)
	(console-display-line "*3*")
	(console-display-line (d-append l1 l2))
	(console-display-line (d-append l1 l2 l3 l4))
	(console-display-line (d-take l1 2))
	(console-display-line (d-take-right l2 3))
	(console-display-line (d-drop l1 2))
	(console-display-line (d-drop-right l2 3))
	(console-display-line (d-reverse l1))
    (console-display-line
     (d-fold1 (lambda (((x1 <object>) (x2 <object>)) <object> pure)
                (+ (cast <integer> x1) (cast <integer> x2)))
              0
              l1))
    (console-display-line
     (d-fold-right1 cons (cast <object> null) l1))
	(console-display-line (list? l1))
	(console-display-line (list? '(1 . 2)))
	(console-display-line "*4*")
	(console-display-line (d-caar '((1 2) (3 4))))
	(console-display-line (d-cadr '((1 2) (3 4))))
	(console-display-line (d-cdar '((1 2) (3 4))))
	(console-display-line (d-cddr '((1 2) (3 4))))
	(console-display-line (d-caddr l1))
	(console-display-line (d-cdddr l1))
	(console-display-line (d-cadddr l1)))))

