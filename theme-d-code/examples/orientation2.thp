;; -*-theme-*-

;; Copyright (C) 2024  Tommi Höynälänmaa
;; Distributed under GNU Lesser General Public License version 3,
;; see file doc/LGPL-3.

;; Expected results: translation and running OK


(define-proper-program (examples orientation2)
  
  (import (standard-library core)
          (standard-library console-io))
  
  (define <orientation>
    (:union (:unit-type 'horizontal)
            (:unit-type 'vertical)))
  
  (define-simple-virtual-method method1 (((orientation <orientation>)) <none> nonpure)
    (raise-simple 'abstract-method))
  
  (define-simple-virtual-method method1 (((orientation (:unit-type 'horizontal)))
                                 <none> nonpure)
    (console-display-line "*2*"))
  
  (define-simple-virtual-method method1 (((orientation (:unit-type 'vertical)))
                                 <none> nonpure)
    (console-display-line "*3*"))
  
  (define-simple-virtual-method method2 (((orientation <orientation>)) <none> nonpure)
    (method1 orientation))
  
  (define-main-proc (() <none> nonpure)
    (method1 'horizontal)
    (method1 'vertical)
    (method2 'horizontal)))
