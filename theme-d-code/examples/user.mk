
.PHONY : all clean compile

THEME_D_COMPILE ?= theme-d-compile
THEME_D_LINK ?= theme-d-link

EXTRA_COMP_OPTIONS ?=
EXTRA_LINK_OPTIONS ?=

ifdef PRETTY_PRINT
	EXTRA_COMP_OPTIONS += --pretty-print
	EXTRA_LINK_OPTIONS += --pretty-print
endif

ifdef NO_LINK_OPTIMIZATION
	EXTRA_LINK_OPTIONS += --no-factorization --no-strip
endif

ifndef INSTALL_DIR
  INSTALL_DIR := /tmp
endif

MODULES := \
  sequence list-as-sequence vector-as-sequence \
	sequence-sgn sequence-sgn-simple \
  sequence-list-impl sequence-vector-impl \
  assoc-sgn assoc-list-impl hash-table \
  module1 module2

PROGRAMS := hello-world sequence-test \
  sequence-sgn-test sequence-sgn-test2 \
  assoc-test assoc-test2 objects1 objects2 module-test1 \
  statprof-demo command-line-demo \
	orientation1 orientation2 \
	iterator-example nonpure-iterator-example \
	bits fold

INTERFACE_SRC := $(patsubst %,%.thi,$(MODULES))
BODY_SRC := $(patsubst %,%.thb,$(MODULES))
PROGRAM_SRC := $(patsubst %,%.thp,$(PROGRAMS))
INTERFACE_TARGET := $(patsubst %,%.tci,$(MODULES))
BODY_TARGET := $(patsubst %,%.tcb,$(MODULES))
COMP_PROGRAM_TARGET := $(patsubst %,%.tcp,$(PROGRAMS))
PROGRAM_TARGET := $(patsubst %,%.go,$(PROGRAMS))
PROGRAM_INTERMEDIATE := $(patsubst %,%.tree-il,$(PROGRAMS)) \
  $(patsubst %,%.scm,$(PROGRAMS)) 

MODULEPATH := -m ..:

all : $(PROGRAM_TARGET) $(COMP_PROGRAM_TARGET) \
  $(INTERFACE_TARGET) $(BODY_TARGET)

clean :
	-rm -f $(INTERFACE_TARGET)
	-rm -f $(BODY_TARGET)
	-rm -f $(COMP_PROGRAM_TARGET)
	-rm -f $(PROGRAM_TARGET)
	-rm -f $(PROGRAM_RACKET_TARGET)
	-rm -f $(PROGRAM_INTERMEDIATE)

compile : $(COMP_PROGRAM_TARGET) $(INTERFACE_TARGET) $(BODY_TARGET)

%.tcp : %.thp
	$(THEME_D_COMPILE) $(MODULEPATH) $(EXTRA_COMP_OPTIONS) -o $@ $<

%.tci : %.thi
	$(THEME_D_COMPILE) $(MODULEPATH) $(EXTRA_COMP_OPTIONS) -o $@ $<

%.tcb : %.thb
	$(THEME_D_COMPILE) $(MODULEPATH) $(EXTRA_COMP_OPTIONS) -o $@ $<

$(PROGRAM_TARGET): %.go : %.tcp
	$(THEME_D_LINK) $(MODULEPATH) $(EXTRA_LINK_OPTIONS) -o $@ $<

statprof-demo.go : statprof-demo.tcp
	$(THEME_D_LINK) $(MODULEPATH) $(EXTRA_LINK_OPTIONS) -x \
	"(statprof)" -o $@ $<

# Compiled body depends on the compiled interface.
%.tcb : %.tci

list-as-sequence.tci : sequence.tci

list-as-sequence.tcb : sequence.tci

vector-as-sequence.tci : sequence.tci

vector-as-sequence.tcb : sequence.tci

sequence-test.tcp : sequence.tci \
  list-as-sequence.tci vector-as-sequence.tci vector-as-sequence.tci

sequence-test.go sequence-test.rkt : sequence-test.tcp sequence.tcb \
  list-as-sequence.tcb vector-as-sequence.tcb

sequence-sgn-test.tcp : sequence-sgn.tci \
  sequence-list-impl.tci sequence-vector-impl.tci

sequence-sgn-test.go sequence-sgn-test.rkt : \
  sequence-sgn-test.tcp sequence-sgn.tcb sequence-list-impl.tcb \
  sequence-vector-impl.tcb

sequence-sgn-test2.tcp : sequence-sgn-simple.tci sequence-list-impl.tci

sequence-sgn-test2.go : sequence-sgn-simple.tcb sequence-list-impl.tcb

assoc-test.tcp : assoc-sgn.tci assoc-list-impl.tci hash-table.tci

assoc-test.go assoc-test.rkt : assoc-test.tcp assoc-sgn.tcb assoc-list-impl.tcb \
  hash-table.tcb

assoc-test2.tcp : assoc-sgn.tci hash-table.tci

assoc-test2.go assoc-test2.rkt : assoc-test.tcp assoc-sgn.tcb hash-table.tcb

module2.tci : module1.tci

module-test1.tcp : module2.tci

module-test1.go module-test1.rkt : module1.tcb module2.tcb
