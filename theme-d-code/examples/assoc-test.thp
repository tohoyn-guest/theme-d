;; -*-theme-*-

;; Copyright (C) 2015, 2024  Tommi Höynälänmaa
;; Distributed under GNU Lesser General Public License version 3,
;; see file doc/LGPL-3.


(define-proper-program (examples assoc-test)
  
  
  (import (standard-library core)
          (standard-library string-utilities)
          (standard-library basic-math)
          (standard-library console-io)
          (examples assoc-sgn)
          (examples assoc-list-impl)
          (examples hash-table))
  
  (define-param-method my-proc (%key %value)
      (((al (:assoc %key %value)) (obj-key %key))
       <none> nonpure)
    (console-display-line (gen-assoc al obj-key)))
  
  
  (define char->integer
    (unchecked-prim-proc char->integer (<character>) <integer> pure))
  
  
  (define-simple-method string-hash (((str <string>) (i-size <integer>))
                                     <integer> pure)
    (let ((i-len (string-length str)))
      (let-mutable ((i-sum <integer> 0))
        (do ((i <integer> 0 (+ i 1))) ((>= i i-len))
          (set! i-sum (+ i-sum (char->integer (string-ref str i)))))
        (remainder i-sum i-size))))
  
  
  (define-main-proc (() <none> nonpure)
    (let ((al (make-assoc-list
               (static-cast (:procedure (<symbol> <symbol>) <boolean> pure)
                            equal?)
               "")))
      (gen-assoc-set! al 'abc "def")
      (gen-assoc-set! al 'abc2 "def2")
      (gen-assoc-set! al 'abc3 "def3")
      (my-proc al 'abc))
    (let ((al2 (make-hash-table string-hash string=? 10 0.0)))
      (gen-assoc-set! al2 "abc" 1.2)
      (gen-assoc-set! al2 "abc2" 1.5)
      (gen-assoc-set! al2 "abc3" 2.0)
      (my-proc al2 "abc"))))
