#!/bin/sh

for prog in *.go ; do
    echo $prog ;
    ../../theme-d/runtime/run-theme-d-program.scm $prog ;
done
