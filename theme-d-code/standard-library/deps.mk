basic-math.tci : \
  core.tci

binary-file-io.tci : \
  core.tci \
  bytevector.tci \
  files.tci

bitwise-arithmetic.tci : \
  core.tci

bvm-diag-matrix.tci : \
  core.tci \
  bytevector.tci \
  math.tci \
  bvm-matrix-base.tci

bvm-matrices.tci : \
  core.tci \
  bvm-matrix-base.tci \
  bvm-diag-matrix.tci

bvm-matrix-base.tci : \
  core.tci \
  bytevector.tci \
  math.tci

bytevector.tci : \
  core.tci

command-line-parser.tci : \
  core.tci

complex.tci : \
  core.tci \
  rational.tci \
  real-math.tci \
  object-string-conversion.tci

console-io.tci : \
  core.tci \
  files.tci

core-forms2.tci : \
  core-forms.tci \
  platform-specific-impl.tci

core-forms.tci :

core.tci : \
  core-forms.tci \
  platform-specific-impl.tci \
  core-forms2.tci

dynamic-list.tci : \
  core.tci

extra-math.tci : \
  core.tci \
  math.tci

files.tci : \
  core.tci

goops-classes.tci : \
  core.tci

hash-table0.tci : \
  core.tci

hash-table-opt.tci : \
  core.tci

hash-table.tci : \
  core.tci

iterator.tci : \
  core.tci

list-utilities.tci : \
  core.tci

math.tci : \
  core.tci \
  basic-math.tci \
  rational.tci \
  real-math.tci \
  complex.tci

matrix.tci : \
  core.tci \
  complex.tci

mutable-pair.tci : \
  core.tci \
  object-string-conversion.tci

nonpure-iterator.tci : \
  core.tci

object-string-conversion.tci : \
  core.tci

posix-math.tci : \
  core.tci \
  math.tci

promise.tci : \
  core.tci

rational.tci : \
  core.tci \
  object-string-conversion.tci

real-math.tci : \
  core.tci \
  rational.tci

singleton.tci : \
  core.tci

statprof.tci : \
  core.tci

stream.tci : \
  core.tci \
  promise.tci \
  text-file-io.tci

string-utilities.tci : \
  core.tci

system.tci : \
  core.tci

test6.tci : \
  core.tci

text-file-io.tci : \
  core.tci \
  files.tci

basic-math.tcb : \
  basic-math.tci \
  bitwise-arithmetic.tci

binary-file-io.tcb : \
  binary-file-io.tci

bitwise-arithmetic.tcb : \
  bitwise-arithmetic.tci

bvm-diag-matrix.tcb : \
  bvm-diag-matrix.tci \
  list-utilities.tci \
  basic-math.tci

bvm-matrices.tcb : \
  bvm-matrices.tci

bvm-matrix-base.tcb : \
  bvm-matrix-base.tci \
  list-utilities.tci

bytevector.tcb : \
  bytevector.tci

command-line-parser-debug.tcb : \
  command-line-parser-debug.tci \
  string-utilities.tci \
  list-utilities.tci \
  console-io.tci

command-line-parser.tcb : \
  command-line-parser.tci \
  string-utilities.tci \
  list-utilities.tci

complex.tcb : \
  complex.tci \
  bitwise-arithmetic.tci \
  basic-math.tci \
  string-utilities.tci

console-io.tcb : \
  console-io.tci \
  text-file-io.tci

core-forms2.tcb : \
  core-forms2.tci

core-forms.tcb : \
  core-forms.tci

core.tcb : \
  core.tci

dynamic-list.tcb : \
  dynamic-list.tci \
  list-utilities.tci

extra-math.tcb : \
  extra-math.tci

files.tcb : \
  files.tci

goops-classes.tcb : \
  goops-classes.tci

hash-table0.tcb : \
  hash-table0.tci \
  list-utilities.tci

hash-table-opt.tcb : \
  hash-table-opt.tci \
  list-utilities.tci

hash-table.tcb : \
  hash-table.tci \
  list-utilities.tci

iterator.tcb : \
  iterator.tci

list-utilities.tcb : \
  list-utilities.tci \
  dynamic-list.tci

math.tcb : \
  math.tci

matrix.tcb : \
  matrix.tci \
  list-utilities.tci \
  basic-math.tci

mutable-pair.tcb : \
  mutable-pair.tci \
  string-utilities.tci

nonpure-iterator.tcb : \
  nonpure-iterator.tci

object-string-conversion.tcb : \
  object-string-conversion.tci \
  string-utilities.tci \
  list-utilities.tci

posix-math.tcb : \
  posix-math.tci

promise.tcb : \
  promise.tci

rational.tcb : \
  rational.tci \
  basic-math.tci \
  bitwise-arithmetic.tci \
  string-utilities.tci

real-math.tcb : \
  real-math.tci \
  basic-math.tci

singleton.tcb : \
  singleton.tci

statprof.tcb : \
  statprof.tci

stream.tcb : \
  stream.tci

string-utilities.tcb : \
  string-utilities.tci \
  list-utilities.tci

system.tcb : \
  system.tci

test6.tcb : \
  test6.tci

text-file-io.tcb : \
  text-file-io.tci \
  object-string-conversion.tci \
  list-utilities.tci \
  string-utilities.tci

