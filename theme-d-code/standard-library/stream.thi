;; -*-theme-d-*-

;; Copyright (C) 2016, 2021 Tommi Höynälänmaa
;; Distributed under GNU Lesser General Public License version 3,
;; see file doc/LGPL-3.

(define-interface (standard-library stream)


  (import (standard-library core)
	  (standard-library promise)
	  (standard-library text-file-io))


  (define-param-logical-type :stream (%type)
    (:union (:pair (:promise %type) (:promise (:stream %type))) <null>))


  (define-param-logical-type :nonempty-stream (%type)
    (:pair (:promise %type) (:promise (:stream %type))))


  (define-param-logical-type :nonpure-stream (%type)
    (:union (:pair (:nonpure-promise %type)
		   (:nonpure-promise (:nonpure-stream %type))) <null>))


  (define-param-logical-type :nonempty-nonpure-stream (%type)
    (:pair (:nonpure-promise %type)
	   (:nonpure-promise (:nonpure-stream %type))))


  (declare-param-method stream-next (%type) ((:stream %type))
			(:stream %type)
			pure)


  (declare-param-method stream-value (%type) ((:stream %type))
			%type
			pure)


  (declare-param-method stream-empty? (%type) ((:stream %type))
			<boolean>
			pure)


  (declare-param-method stream->list (%type) ((:stream %type))
			(:uniform-list %type)
			pure)


  (declare-param-method list->stream (%type) ((:uniform-list %type))
			(:stream %type)
			pure)


  (declare-param-method nonpure-stream-next (%type) ((:nonpure-stream %type))
			(:nonpure-stream %type)
			nonpure)


  (declare-param-method nonpure-stream-value (%type) ((:nonpure-stream %type))
			%type
			nonpure)


  (declare-param-method nonpure-stream-empty? (%type) ((:nonpure-stream %type))
			<boolean>
			pure)


  (declare-param-method nonpure-stream->list (%type) ((:nonpure-stream %type))
			(:uniform-list %type)
			nonpure)


  (declare-param-method stream-map (%type1 %type2)
			((:procedure (%type1) %type2 pure)
			 (:stream %type1))
			(:stream %type2)
			pure)


  (declare-param-method stream-map-nonpure (%type1 %type2)
			((:procedure (%type1) %type2 nonpure)
			 (:stream %type1))
			(:nonpure-stream %type2)
			nonpure)


  (declare-param-method stream-for-each (%type1)
			((:procedure (%type1) <none> nonpure)
			 (:stream %type1))
			<none>
			nonpure)


  (declare-param-method nonpure-stream-map (%type1 %type2)
			((:procedure (%type1) %type2 nonpure)
			 (:nonpure-stream %type1))
			(:nonpure-stream %type2)
			nonpure)


  (declare-param-method nonpure-stream-for-each
			(%type1)
			((:procedure (%type1) <none> nonpure)
			 (:nonpure-stream %type1))
			<none>
			nonpure)


  (declare-simple-method make-input-expr-stream (<input-port>)
			 (:nonpure-stream <object>)
			 nonpure))
