#!/usr/bin/bash

for fl in *.thi ; do
  if [[ $fl != platform-specific-impl* ]] then
    ../../tools/compute-theme-d-deps.scm $fl standard-library
  fi
done

for fl in *.thb ; do
  if [[ $fl != platform-specific-impl* ]] then
    ../../tools/compute-theme-d-deps.scm $fl standard-library
  fi
done
