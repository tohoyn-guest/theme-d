;; -*-theme-d-*-

;; Copyright (C) 2015, 2021 Tommi Höynälänmaa
;; Distributed under GNU Lesser General Public License version 3,
;; see file doc/LGPL-3.

(define-interface (standard-library dynamic-list)

  (import (standard-library core))

  (declare-method d-car (:simple-proc (<object>) <object> pure))

  (declare-method d-cdr (:simple-proc (<object>) <object> pure))

  (declare-method d-list (:simple-proc ((rest <object>)) <object> pure))

  (declare-method d-list-ref (:simple-proc (<object> <integer>) <object> pure))

  (declare-method d-map1
		  (:param-proc (%type)
			       ((:procedure (<object>) %type pure)
				<object>)
			       (:uniform-list %type)
			       pure))

  (declare-method d-map-nonpure1
		  (:param-proc (%type)
			       ((:procedure (<object>) %type nonpure)
				<object>)
			       (:uniform-list %type)
			       nonpure))

  (declare-method d-map2
		  (:param-proc (%type)
			       ((:procedure (<object> <object>) %type pure)
				<object> <object>)
			       (:uniform-list %type)
			       pure))

  (declare-method d-map-nonpure2
		  (:param-proc (%type)
			       ((:procedure (<object> <object>) %type nonpure)
				<object> <object>)
			       (:uniform-list %type)
			       nonpure))

  (declare-method d-map0
		  (:param-proc (%type)
			       ((:procedure ((rest <object>)) %type pure)
				<object>)
			       (:uniform-list %type)
			       pure))
  
  (declare-method d-map
		  (:param-proc (%type)
			       ((:procedure ((rest <object>)) %type pure)
				(rest <object>))
			       (:uniform-list %type)
			       pure))

  (declare-method d-map-nonpure0
		  (:param-proc (%type)
			       ((:procedure ((rest <object>)) %type nonpure)
				 <object>)
				(:uniform-list %type)
				nonpure))

  (declare-method d-map-nonpure
		  (:param-proc (%type)
			       ((:procedure ((rest <object>)) %type nonpure)
				 (rest <object>))
				(:uniform-list %type)
				nonpure))

  (declare-method d-for-each1 (:simple-proc ((:procedure (<object>) <none>
							 nonpure)
				    <object>)
				   <none>
				   nonpure))

  (declare-method d-for-each2
		  (:simple-proc
		   ((:procedure (<object> <object>) <none> nonpure)
		    <object> <object>)
		   <none> nonpure))

  (declare-method d-for-each0 (:simple-proc ((:procedure ((rest <object>))
							 <none>
							 nonpure)
					     <object>)
				  <none>
				  nonpure))

  (declare-method d-for-each (:simple-proc ((:procedure ((rest <object>)) <none>
					       nonpure)
				   (rest <object>))
				  <none>
				  nonpure))

  (declare-method d-map-car (:simple-proc (<object>) <list> pure))

  (declare-method d-map-cdr (:simple-proc (<object>) <list> pure))

  (declare-method d-for-all1?
		  (:simple-proc
		   ((:procedure (<object>) <boolean> pure)
		    <object>)
		   <boolean>
		   pure))
  
  (declare-method d-for-all-nonpure1?
		  (:simple-proc
		   ((:procedure (<object>) <boolean> nonpure)
		    <object>)
		   <boolean>
		   nonpure))
  
  (declare-method d-for-all2?
		  (:simple-proc
		   ((:procedure (<object> <object>) <boolean> pure)
		    <object> <object>)
		   <boolean>
		   pure))
  
  (declare-method d-for-all-nonpure2?
		  (:simple-proc
		   ((:procedure (<object> <object>) <boolean> nonpure)
		    <object> <object>)
		   <boolean>
		   nonpure))

  (declare-method d-for-all0?
		  (:simple-proc
		   ((:procedure ((rest <object>)) <boolean> pure)
		    <object>)
		   <boolean>
		   pure))
  
  (declare-method d-for-all?
		  (:simple-proc
		   ((:procedure ((rest <object>)) <boolean> pure)
		    (rest <object>))
		   <boolean>
		   pure))

  (declare-method d-for-all-nonpure0?
		  (:simple-proc
		   ((:procedure ((rest <object>)) <boolean> nonpure)
		    <object>)
		   <boolean>
		   nonpure))
  
  (declare-method d-for-all-nonpure?
		  (:simple-proc
		   ((:procedure ((rest <object>)) <boolean> nonpure)
		    (rest <object>))
		   <boolean>
		   nonpure))

  (declare-method d-exists1?
		  (:simple-proc
		   ((:procedure (<object>) <boolean> pure)
		    <object>)
		   <boolean>
		   pure))
  
  (declare-method d-exists-nonpure1?
		  (:simple-proc
		   ((:procedure (<object>) <boolean> nonpure)
		    <object>)
		   <boolean>
		   nonpure))
  
  (declare-method d-exists2?
		  (:simple-proc
		   ((:procedure (<object> <object>) <boolean> pure)
		    <object> <object>)
		   <boolean>
		   pure))
  
  (declare-method d-exists-nonpure2?
		  (:simple-proc
		   ((:procedure (<object> <object>) <boolean> nonpure)
		    <object> <object>)
		   <boolean>
		   nonpure))

  (declare-method d-exists0?
		  (:simple-proc
		   ((:procedure ((rest <object>)) <boolean> pure)
		    <object>)
		   <boolean>
		   pure))
  
  (declare-method d-exists?
		  (:simple-proc
		   ((:procedure ((rest <object>)) <boolean> pure)
		    (rest <object>))
		   <boolean>
		   pure))
  
  (declare-method d-exists-nonpure0?
		  (:simple-proc
		   ((:procedure ((rest <object>)) <boolean> nonpure)
		    <object>)
		   <boolean>
		   nonpure))
  
  (declare-method d-exists-nonpure?
		  (:simple-proc
		   ((:procedure ((rest <object>)) <boolean> nonpure)
		    (rest <object>))
		   <boolean>
		   nonpure))

  (declare-method d-append0 (:simple-proc (<object>) <object> pure))

  (declare-method d-append2 (:simple-proc (<object> <object>) <object> pure))
		  
  (declare-method d-append (:simple-proc ((rest <object>)) <object> pure))

  (declare-method d-length (:simple-proc (<object>) <integer> pure))

  (declare-method d-take (:simple-proc (<object> <integer>) <list> pure))

  (declare-method d-take-right (:simple-proc (<object> <integer>) <object>
					     pure))

  (declare-method d-drop (:simple-proc (<object> <integer>) <object> pure))

  (declare-method d-drop-right (:simple-proc (<object> <integer>) <list> pure))

  (declare-method d-reverse (:simple-proc (<object>) <object> pure))

  (declare-method list? (:simple-proc (<object>) <boolean> pure))

  (declare-method d-fold1
    (:param-proc (%result)
                 ((:procedure (<object> %result) %result pure)
                  %result
                  <object>)
                 %result pure))

  (declare-method d-fold-right1
    (:param-proc (%result)
                 ((:procedure (<object> %result) %result pure)
                  %result
                  <object>)
                 %result pure))

  (declare-method d-caar (:simple-proc (<object>) <object> pure))

  (declare-method d-cadr (:simple-proc (<object>) <object> pure))

  (declare-method d-cdar (:simple-proc (<object>) <object> pure))
  
  (declare-method d-cddr (:simple-proc (<object>) <object> pure))
  
  (declare-method d-caddr (:simple-proc (<object>) <object> pure))

  (declare-method d-cdddr (:simple-proc (<object>) <object> pure))

  (declare-method d-cadddr (:simple-proc (<object>) <object> pure)))

