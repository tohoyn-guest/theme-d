
MODULES = \
  platform-specific-impl \
  basic-math \
  binary-file-io \
  bitwise-arithmetic \
  bvm-diag-matrix \
  bvm-matrices \
  bvm-matrix-base \
  bytevector \
  command-line-parser \
  complex \
  console-io \
  core \
  core-forms \
  core-forms2 \
  dynamic-list \
  files \
  goops-classes \
  hash-table0 \
  hash-table \
  hash-table-opt \
  iterator \
  list-utilities \
  math \
  matrix \
  mutable-pair \
  nonpure-iterator \
  object-string-conversion \
  promise \
  rational \
  real-math \
  singleton \
  stream \
  string-utilities \
  text-file-io \
  statprof \
  system

ifdef EXTRA_MATH
  MODULES += extra-math
endif

ifdef POSIX_MATH
  MODULES += posix-math
endif

PLATFORM_INTF = platform-specific-impl-guile-3.0.thi
PLATFORM_BODY = platform-specific-impl-guile-3.0.thb

INTERFACE_TARGET = $(MODULES:%=%.tci)
BODY_TARGET = $(MODULES:%=%.tcb)

EXTRA_COMP_OPTIONS ?=
COMPILE ?= theme-d-compile 

%.tci : %.thi 
	$(COMPILE) -m .. -o $@ $(EXTRA_COMP_OPTIONS) $<

%.tcb : %.thb
	$(COMPILE) -m .. -o $@ $(EXTRA_COMP_OPTIONS) $<


.PHONY:  all clean

all: $(INTERFACE_TARGET) $(BODY_TARGET)

clean:
	-rm -f *.tc?
	-rm platform-specific-impl.thi
	-rm platform-specific-impl.thb

platform-specific-impl.thi: $(PLATFORM_INTF)
	ln -sf $(PLATFORM_INTF) platform-specific-impl.thi

platform-specific-impl.thb: $(PLATFORM_BODY)
	ln -sf $(PLATFORM_BODY) platform-specific-impl.thb

platform-specific-impl.tci : core-forms.tci

platform-specific-impl.tcb : platform-specific-impl.tci

include deps.mk
