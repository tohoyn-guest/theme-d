#!/bin/sh

if [ $# != 1 ] ; then
    echo "Invalid number of arguments." ;
    exit 1
fi

cp -avi user.mk deps.mk $1
cp -avi $(find . -name "*.th?" -type f) $1
    