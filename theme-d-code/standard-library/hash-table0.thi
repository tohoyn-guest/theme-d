;; -*-theme-d-*-

;; Copyright (C) 2016-2019, 2021, 2024  Tommi Höynälänmaa

(define-interface (standard-library hash-table0)

  (import (standard-library core))

  ;; Maybe we should search the contents of the hash tables in
  ;; "equal-contents?".
  (define-foreign-prim-class <raw-hash-table>
    (member-pred hash-table?))
  
  (define-param-logical-type :hash-proc (%key)
    (:procedure (%key <integer>) <integer> pure))

  (define-param-logical-type :simple-hash-proc (%key)
    (:simple-proc (%key <integer>) <integer> pure))

  (define-param-logical-type :assoc-proc (%key %value)
    (:procedure (%key (:alist %key %value)) (:alt-maybe (:pair %key %value))
		pure))

  (define-param-class :hash-table
    (parameters %key %value)
    (construct ((rawht1 <raw-hash-table>)
		(proc-hash1 (:hash-proc %key))
		(proc-assoc1 (:assoc-proc %key %value)))
	       ()
	       (pure))
    (fields
     (rawht <raw-hash-table> public module rawht1)
     (proc-hash (:hash-proc %key) public module proc-hash1)
     (proc-assoc (:assoc-proc %key %value) public module proc-assoc1)))
  
  (define-param-logical-type :object-hash-table (%value)
    (:hash-table <object> %value))

  (define-param-logical-type :string-hash-table (%value)
    (:hash-table <string> %value))

  (define-param-logical-type :symbol-hash-table (%value)
    (:hash-table <symbol> %value))

  (declare-param-method make-hash-table
			(%key %value)
			((:hash-proc %key) (:assoc-proc %key %value))
			(:hash-table %key %value) pure)

  (declare-param-method make-hash-table-with-size
			(%key %value)
			((:hash-proc %key) (:assoc-proc %key %value)
			 <integer>)
			(:hash-table %key %value) pure)

  (declare-param-method make-object-hash-table
			(%value) ((:maybe %value))
			(:object-hash-table %value) pure)

  (declare-param-method make-object-hash-table-with-size
			(%value) ((:maybe %value) <integer>)
			(:object-hash-table %value)
			pure)

  (declare-param-method make-string-hash-table
			(%value) ((:maybe %value))
			(:string-hash-table %value) pure)

  (declare-param-method make-string-hash-table-with-size
			(%value) ((:maybe %value) <integer>)
			(:string-hash-table %value)
			pure)

  (declare-param-method make-symbol-hash-table
			(%value) ((:maybe %value))
			(:symbol-hash-table %value) pure)

  (declare-param-method make-symbol-hash-table-with-size
			(%value) ((:maybe %value) <integer>)
			(:symbol-hash-table %value)
			pure)

  (declare-param-method hash-ref
			(%key %value %default)
			((:hash-table %key %value) %key %default)
			(:union %value %default) pure)

  (declare-param-method hash-exists?
			(%key %value) ((:hash-table %key %value) %key)
			<boolean> pure)

  (declare-param-method hash-set!
			(%key %value) ((:hash-table %key %value) %key %value)
			<none> nonpure)

  (declare-param-method hash-remove!
			(%key %value) ((:hash-table %key %value) %key)
			<none> nonpure)

  (declare-param-method hash-count-elements
			(%key %value) ((:hash-table %key %value)) <integer>
			pure)

  (declare-method hashq (:simple-hash-proc <object>))
  
  (declare-method hashv (:simple-hash-proc <object>))

  (declare-method hash-contents (:simple-hash-proc <object>))

  (declare-method object-hash (:simple-hash-proc <object>))

  (declare-method string-hash (:simple-hash-proc <string>))

  (declare-param-method assoc1
			(%type1 %type2)
			(%type1 (:alist %type1 %type2))
			(:union (:pair %type1 %type2) <boolean>)
			pure)

  (declare-param-method assoc-values1
			(%type1 %type2)
			(%type1 (:alist %type1 %type2))
			(:union (:pair %type1 %type2) <boolean>)
			pure)

  (declare-param-method assoc-objects1
			(%type1 %type2)
			(%type1 (:alist %type1 %type2))
			(:union (:pair %type1 %type2) <boolean>)
			pure)

  (declare-param-method assoc-contents1
			(%type1 %type2)
			(%type1 (:alist %type1 %type2))
			(:union (:pair %type1 %type2) <boolean>)
			pure)

  (declare-param-method hash-for-each
			(%key %value)
			((:procedure (%key %value) <none> nonpure)
			 (:hash-table %key %value))
			<none>
			nonpure)

  (declare-param-method hash-clear!
			(%key %value)
			((:hash-table %key %value))
			<none>
			nonpure))

