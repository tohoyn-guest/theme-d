;; -*-theme-d-*-

;; Copyright (C) 2020, 2021 Tommi Höynälänmaa
;; Distributed under GNU Lesser General Public License version 3,
;; see file doc/LGPL-3.

;; This module is imported by the core interface so we can't import it here.

(define-body (standard-library platform-specific-impl)
  
  (define raise
    (unchecked-prim-proc raise-exception (<object>) <none>
                         (pure never-returns)))
  
  (define make-prompt-tag
    (unchecked-prim-proc make-prompt-tag () <object> pure))
  
  (define abort-to-prompt2
    (unchecked-prim-proc abort-to-prompt
                         (<object>
                          (:procedure (<object>) <none> pure)
                          <object>)
                         <none>
                         (pure never-returns)))
  
  (define-param-proc-alt call-with-prompt (%result %handler)
    (prim-proc theme-call-with-prompt
               (<object>
                (:simple-proc () %result pure)
                (:simple-proc
                 (<object>
                  (:simple-proc (<object>) %handler pure)
                  <object>)
                 %handler
                 pure))
               (:union %result %handler)
               pure))
  
  (define abort-to-prompt2-nonpure
    (unchecked-prim-proc abort-to-prompt
                         (<object>
                          (:procedure (<object>) <none> nonpure)
                          <object>)
                         <none>
                         (never-returns nonpure)))
  
  (define-param-proc-alt call-with-prompt-nonpure (%result %handler)
    (prim-proc theme-call-with-prompt
               (<object>
                (:simple-proc () %result nonpure)
                (:simple-proc
                 (<object>
                  (:simple-proc (<object>) %handler nonpure)
                  <object>)
                 %handler
                 nonpure))
               (:union %result %handler)
               nonpure))
  
  (define-param-proc-alt call-with-prompt-no-result (%result %handler)
    (prim-proc theme-call-with-prompt
               (<object>
                (:simple-proc () %result nonpure)
                (:simple-proc
                 (<object>
                  (:simple-proc (<object>) %handler nonpure)
                  <object>)
                 %handler
                 nonpure))
               <none>
               nonpure)))

