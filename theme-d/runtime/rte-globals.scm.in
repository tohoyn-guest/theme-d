
;; Copyright (C) 2024 Tommi Höynälänmaa
;; Distributed under GNU Lesser General Public License version 3,
;; see file doc/LGPL-3.


(define-module (theme-d runtime rte-globals)
  #:duplicates (merge-generics replace last)
  @decl_f@)


(use-modules (rnrs))


;; *** Some global variables ***


(define-public gl-verbose-errors? #t)
(define-public gl-backtrace? #f)
(define-public gl-pretty-backtrace? #f)
(define-public gl-pretty-backtrace-support? #f)
(define-public gl-script? #f)
(define-public gl-enable-rte-exception-info? #f)
(define-public gl-display-error-type? #t)

(define-public gl-platform 'guile)

(define-public x-debug-object #f)

;; *** Setting global variables ***


(define-public (set-rte-exception-info! b)
  (set! gl-enable-rte-exception-info? b))


(define-public (set-verbose-errors! b)
  (set! gl-verbose-errors? b))


(define-public (set-backtrace! b)
  (set! gl-backtrace? b))


(define-public (set-pretty-backtrace! b)
  (if (and b (not gl-pretty-backtrace-support?))
    (raise 'pretty-backtrace-not-linked))
  (set! gl-pretty-backtrace? b))


(define-public (set-pretty-backtrace-support! b)
  (set! gl-pretty-backtrace-support? b))


(define-public (set-theme-d-script! b)
  (set! gl-script? b))


(define-public (set-display-error-type! b)
  (set! gl-display-error-type? b))
