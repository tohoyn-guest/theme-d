
;; Copyright (C) 2008-2013 Tommi Höynälänmaa
;; Distributed under GNU Lesser General Public License version 3,
;; see file doc/LGPL-3.

(library (theme-d runtime runtime0)

  (export gl-verbose-errors?
	  gl-pretty-backtrace?
	  gl-backtrace?
	  gl-script?
	  gl-enable-rte-exception-info?
	  my-error-exit-fwd
	  os-main
	  gl-l-command-line
	  prt-fwd
	  _i_call-proc-fwd
	  theme-class-of-fwd)

  (import (guile))
  
  (define gl-verbose-errors? #t)
  (define gl-backtrace? #f)
  (define gl-pretty-backtrace? #f)
  (define gl-script? #f)
  (define gl-enable-rte-exception-info? #f)

  (define my-error-exit-fwd '())
  (define os-main '())
  (define gl-l-command-line '())
  (define prt-fwd '())

  (define _i_call-proc-fwd '())
  (define theme-class-of-fwd '()))
