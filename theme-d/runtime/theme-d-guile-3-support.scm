
(define-module (theme-d runtime theme-d-guile-3-support)
  #:export (theme-call-with-prompt
            theme-get-rte))

(use-modules (rnrs exceptions))
(use-modules (rnrs conditions))
(use-modules (rnrs base))
(use-modules (rnrs lists))
(use-modules (theme-d runtime rte-base))
(use-modules (theme-d runtime runtime-theme-d-environment))

(define (theme-call-with-prompt tag thunk handler)
  (call-with-prompt tag
                    (lambda () (_i_call-simple-proc thunk '()))
                    (lambda (_ h v)
                      (let ((h1 (lambda (x) (_i_call-simple-proc h (list x)))))
                        (_i_call-simple-proc
                         handler (list _ h v))))))

(define (theme-get-rte exc)
  (let ((proc (exception-accessor &theme-d-condition (lambda (x) x))))
    (proc exc)))
