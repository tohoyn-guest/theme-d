#!/bin/bash

# A script to compute dependencies in directory theme-d/translator.
# Copyright (C) 2024 Tommi Höynälänmaa
# Distributed under GNU LGPL.


modules1=(rte-apply rte-base rte-debug rte-equality-predicates \
  rte-forward rte-globals rte-object rte-vectors rte-error-messages \
  runtime-theme-d-environment params)

modules2=(theme-d-stdlib-support theme-d-guile-3-support)

for fl in ${modules1[@]} ; do
  ../../tools/compute-deps.scm $fl.scm.in "theme-d runtime" ;
done

for fl in ${modules2[@]} ; do
  ../../tools/compute-deps.scm $fl.scm "theme-d runtime" ;
done
