#!@theme_d_guile@ \
-e __main -s
!#

;; Copyright (C) 2018, 2020 Tommi Höynälänmaa
;; Distributed under GNU Lesser General Public License version 3,
;; see file doc/LGPL-3.

(eval-when (expand load eval)
           (default-duplicate-binding-handler '(merge-generics replace last)))

(import (rnrs exceptions))
(import (rnrs conditions))
(import (rnrs base))
(import (rnrs lists))
(import (theme-d runtime rte-base))
(import (theme-d runtime rte-globals))
(import (theme-d runtime runtime-theme-d-environment))
(import (th-scheme-utilities parse-command-line))
(import (th-scheme-utilities stdutils))
(import (th-scheme-utilities hrecord))

(define gl-custom-files0 (getenv "THEME_D_CUSTOM_CODE"))

(define gl-custom-files (if (string? gl-custom-files0)
                          (split-string gl-custom-files0 #\:)
                          '()))

(for-each load-compiled gl-custom-files)

(define (__main args)
  (let* ((verbose-errors? #t)
         (backtrace? #f)
         (pretty-backtrace? #f)
         (show-version? #f)
         (argd1 (make-hrecord <argument-descriptor> "no-verbose-errors" #f
                              (lambda () (set! verbose-errors? #f))))
         (argd2 (make-hrecord <argument-descriptor> "backtrace" #f
                              (lambda () (set! backtrace? #t))))
         (argd3 (make-hrecord <argument-descriptor> "pretty-backtrace" #f
                              (lambda () (set! pretty-backtrace? #t))))
         (argd4 (make-hrecord <argument-descriptor> "version" #f
                              (lambda () (set! show-version? #t))))
         (args-without-cmd (cdr args))
         (arg-descs (list argd1 argd2 argd3 argd4))
         (proper-args '())
         (handle-proper-args (lambda (proper-args1)
                               (set! proper-args proper-args1))))
    (parse-command-line args-without-cmd arg-descs handle-proper-args)
    (if show-version?
      (begin
       (display "@package_version@")
       (newline)
       (exit 0)))
    (if (null? proper-args)
      (begin
       (display "No program name given.\n")
       (exit 1)))
    (theme-set-command-line! proper-args)
    (if (not backtrace?)
      (guard (exc (else (my-error-exit exc)))
             (load-compiled (car proper-args)))
      (load-compiled (car proper-args)))
    (if (not gl-script?)
      (begin
       (set-verbose-errors! verbose-errors?)
       (set-backtrace! backtrace?)
       (set-pretty-backtrace! pretty-backtrace?)
       (set-rte-exception-info! #t)
       (main proper-args)))))
