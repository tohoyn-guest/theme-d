
;; Copyright (C) 2019 Tommi Höynälänmaa
;; Distributed under GNU Lesser General Public License version 3,
;; see file doc/LGPL-3.

;; Support for primitives written in Scheme.


(define-module (theme-d runtime theme-d-support)
  #:export (is-real?
	    is-integer?
	    real->integer
	    r-sqrt
	    r-expt
	    r-sin
	    r-cos
	    r-tan
	    r-asin
	    r-acos
	    r-atan
	    r-atan2
	    r-exp
	    r-log
	    r-log10
	    r-sinh
	    r-cosh
	    r-tanh
	    r-asinh
	    r-acosh
	    r-atanh
	    r-round
	    r-truncate
	    r-floor
	    r-ceiling
	    fmod
	    r-remainder
	    fma
	    fast-fma?
	    fmin
	    fmax
	    fdim
	    r-exp2
	    r-expm1
	    r-log2
	    r-log1p
	    logb
	    ilogb
	    ilogb0
	    ilogbnan
	    r-cbrt
	    r-hypot
	    r-erf
	    r-erfc
	    r-lgamma
	    r-tgamma
	    r-nearbyint
	    rint
	    frexp
	    ldexp
	    modf
	    r-nextafter
	    r-copysign
	    fpclassify
	    fpclassify-nan
	    fpclassify-infinite
	    fpclassify-zero
	    fpclassify-subnormal
	    fpclassify-normal
	    r-isnormal?
	    r-signbit))

(eval-when (load)
	   (load-extension "libthemedsupport" "init_theme_d_support"))
