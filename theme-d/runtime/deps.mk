rte-apply.go : \
  params.go \
  theme-d-support.go \
  rte-object.go \
  rte-base.go \
  rte-forward.go \
  rte-debug.go

rte-base.go : \
  params.go \
  theme-d-support.go \
  rte-object.go \
  rte-globals.go \
  rte-forward.go \
  rte-debug.go

rte-debug.go : \
  rte-forward.go

rte-equality-predicates.go : \
  theme-d-support.go \
  rte-object.go \
  rte-base.go \
  rte-vectors.go \
  rte-forward.go \
  rte-debug.go

rte-forward.go :

rte-globals.go :

rte-object.go : \
  rte-forward.go \
  rte-debug.go

rte-vectors.go : \
  params.go \
  theme-d-support.go \
  rte-object.go \
  rte-base.go \
  rte-forward.go \
  rte-debug.go

rte-error-messages.go : \
  rte-object.go \
  rte-base.go \
  rte-forward.go \
  rte-debug.go

runtime-theme-d-environment.go : \
  params.go \
  theme-d-support.go \
  rte-object.go \
  rte-base.go \
  rte-vectors.go \
  rte-equality-predicates.go \
  rte-utilities.go \
  rte-type-system.go \
  rte-error-messages.go \
  rte-globals.go \
  rte-forward.go \
  rte-debug.go

params.go :

theme-d-stdlib-support.go : \
  theme-d-support.go \
  runtime-theme-d-environment.go \
  rte-base.go \
  rte-globals.go

theme-d-guile-3-support.go : \
  rte-base.go \
  runtime-theme-d-environment.go

