
;; Copyright (C) 2019 Tommi Höynälänmaa
;; Distributed under GNU Lesser General Public License version 3,
;; see file doc/LGPL-3.

;; Support for primitives written in Scheme.


(define-module (theme-d runtime theme-d-support)
  #:export (is-real?
	    is-integer?
	    real->integer
	    r-sqrt
	    r-expt
	    r-sin
	    r-cos
	    r-tan
	    r-asin
	    r-acos
	    r-atan
	    r-atan2
	    r-exp
	    r-log
	    r-log10
	    r-sinh
	    r-cosh
	    r-tanh
	    r-asinh
	    r-acosh
	    r-atanh
	    r-round
	    r-truncate
	    r-floor
	    r-ceiling))

(eval-when (load)
	   (load-extension "libthemedsupport" "init_theme_d_support"))
