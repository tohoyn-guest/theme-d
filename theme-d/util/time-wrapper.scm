
(define-module (theme-d util time-wrapper)
  #:export (time1? make-time1 time-nanosecond1 time-second1
		   copy-time1 current-time1 time-difference1 add-duration1))

(import (srfi srfi-19))

(define (time1? x) (time? x))

(define (make-time1 s-kind i-nanosec i-sec)
  (make-time s-kind i-nanosec i-sec))

(define (time-nanosecond1 time)
  (time-nanosecond time))

(define (time-second1 time)
  (time-second time))

(define (copy-time1 time)
  (copy-time time))

(define (current-time1) (current-time))

(define (time-difference1 time1 time2)
  (time-difference time1 time2))

(define (add-duration1 time1 time2)
  (add-duration time1 time2))
