#!/bin/sh

# A script to compute dependencies in directory theme-d/translator.
# Copyright (C) 2024 Tommi Höynälänmaa
# Distributed under GNU LGPL.

for fl in *.scm.in ; do
    if [ $fl != "theme-d-type-system1-header.scm.in" ] && \
			 [ $fl != "theme-d-utilities-header.scm.in" ] ; then
      ../../tools/compute-deps.scm $fl "theme-d translator" ;
    fi
done

../../tools/compute-deps.scm theme-d-type-system1-header.scm.in "theme-d translator" | \
  sed -e s/theme-d-type-system1-header.go/theme-d-type-system1.go/g

../../tools/compute-deps.scm theme-d-utilities-header.scm.in "theme-d translator" | \
  sed -e s/theme-d-utilities-header.go/theme-d-utilities.go/g
