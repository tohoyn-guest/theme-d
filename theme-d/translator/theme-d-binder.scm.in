;; Copyright (C) 2008-2013 Tommi Höynälänmaa
;; Distributed under GNU General Public License version 3,
;; see file doc/GPL-3.


;; *** Binder object ***


(define-module (theme-d translator theme-d-binder)
  @decl_t@
  @dupl@
  #:export (<binder>
	    is-binder?
	    binder-set-cycle!
	    binder-has-cycle?))

(import (rnrs exceptions)
	(srfi srfi-1)
	(th-scheme-utilities stdutils)
	(th-scheme-utilities hrecord))


(define-hrecord-type <binder> ()
  param-cache
  param-cache-opt
  allocate-variable
  type-check?
  optimize-copying?
  preserve-types?
  instantiation?
  make-instances?
  accept-incomplete?
  ;; The following field can be removed.
  fixing?
  default-proc-arg-opt?
  fixed-tvars
  ht-globals-by-address
  ht-method-decls
  decl-proc-instances
  visited-in-binding
  current-repr-to-bind
  tcomp-inside-param-proc?
  inside-param-proc?
  marker-table
  l-overwrite
  s-cur-toplevel
  expr-cur-proc
  optimize-raw-proc-inst?
  ht-raw-procs
  ht-param-class-init
  inline-constructors?
  ht-ctr-info
  ht-opt-arg)


(define is-binder? (get-hrecord-type-predicate <binder>))


(define (binder-set-cycle! binder repr)
  (hashq-set! (hfield-ref binder 'ht-all-cycles) repr #t))


(define (binder-has-cycle? binder repr)
  (hashq-ref (hfield-ref binder 'ht-all-cycles) repr))
