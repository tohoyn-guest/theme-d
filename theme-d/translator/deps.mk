theme-d-address-allocation.go : \
  theme-d-entities.go

theme-d-address-environment.go : \
  theme-d-forward-declarations.go \
  theme-d-address-table.go \
  theme-d-entities.go \
  theme-d-representation.go \
  theme-d-hash-tables.go \
  theme-d-symbol-table.go \
  theme-d-keywords.go \
  theme-d-type-system1.go \
  theme-d-pcode-common.go \
  theme-d-debug.go

theme-d-address-table.go : \
  theme-d-entities.go \
  theme-d-common.go

theme-d-binder.go :

theme-d-builtin-type-finalization.go : \
  theme-d-representation.go \
  theme-d-utilities.go \
  theme-d-type-translation.go

theme-d-builtins.go : \
  theme-d-representation.go \
  theme-d-entities.go \
  theme-d-symbol-table.go \
  theme-d-translation-common.go \
  theme-d-special-procedures.go

theme-d-common-procedure-utilities.go : \
  theme-d-type-system1.go \
  theme-d-forward-declarations.go \
  theme-d-entities.go \
  theme-d-expressions.go \
  theme-d-representation.go \
  theme-d-binder.go \
  theme-d-address-environment.go \
  theme-d-debug.go

theme-d-common.go :

theme-d-compilation-errors.go : \
  theme-d-macros.go \
  theme-d-var-names.go \
  theme-d-errors-common.go \
  theme-d-print-target-object.go \
  theme-d-file-handling.go \
  theme-d-representation.go \
  theme-d-entities.go \
  theme-d-debug.go

theme-d-compilation-utilities.go : \
  theme-d-forward-declarations.go \
  theme-d-common.go \
  theme-d-symbol-table.go \
  theme-d-representation.go \
  theme-d-entities.go \
  theme-d-expressions.go \
  theme-d-compilation-errors.go \
  theme-d-compiler-core-def.go \
  theme-d-type-system1.go \
  theme-d-translation-common.go \
  theme-d-interface-pcode-reading.go \
  theme-d-keywords.go \
  theme-d-hash-tables.go \
  theme-d-pcode-common.go \
  theme-d-address-environment.go \
  theme-d-print-target-object.go \
  theme-d-debug.go

theme-d-compilation1.go : \
  theme-d-forward-declarations.go \
  theme-d-utilities.go \
  theme-d-entities.go \
  theme-d-expressions.go \
  theme-d-representation.go \
  theme-d-symbol-table.go \
  theme-d-letrec-compilation.go \
  theme-d-compilation-errors.go \
  theme-d-type-system1.go \
  theme-d-compiler-core-def.go \
  theme-d-procedure-utilities.go \
  theme-d-common-procedure-utilities.go \
  theme-d-constructors.go \
  theme-d-compiler-constructors.go \
  theme-d-keywords.go \
  theme-d-common.go \
  theme-d-fields.go \
  theme-d-translation-common.go \
  theme-d-expression-translation.go \
  theme-d-type-translation.go \
  theme-d-special-procedures.go \
  theme-d-expression-cloning.go \
  theme-d-compilation-utilities.go \
  theme-d-compiler-constructors.go \
  theme-d-pcode-common.go \
  theme-d-debug.go

theme-d-compilation2.go : \
  theme-d-forward-declarations.go \
  theme-d-compilation1.go \
  theme-d-utilities.go \
  theme-d-address-allocation.go \
  theme-d-interface-pcode-reading.go \
  theme-d-common.go \
  theme-d-translation-common.go \
  theme-d-entities.go \
  theme-d-expressions.go \
  theme-d-representation.go \
  theme-d-type-translation.go \
  theme-d-expression-translation.go \
  theme-d-compiler-core-def.go \
  theme-d-common-procedure-utilities.go \
  theme-d-procedure-utilities.go \
  theme-d-compilation-utilities.go \
  theme-d-symbol-table.go \
  theme-d-type-system1.go \
  theme-d-keywords.go \
  theme-d-macros.go \
  theme-d-address-environment.go \
  theme-d-address-table.go \
  theme-d-expression-cloning.go \
  theme-d-constructors.go \
  theme-d-compiler-constructors.go \
  theme-d-parametrized-instances.go \
  theme-d-hash-tables.go \
  theme-d-pcode-common.go \
  theme-d-binder.go \
  theme-d-print-target-object.go \
  theme-d-debug.go

theme-d-compile-unit.go : \
  theme-d-compiler-core-def.go \
  theme-d-address-allocation.go \
  theme-d-phase2-compilation.go \
  theme-d-common.go \
  theme-d-pcode-common.go \
  theme-d-entities.go \
  theme-d-keywords.go \
  theme-d-compilation2.go \
  theme-d-param-cache.go \
  theme-d-param-cache-opt.go \
  theme-d-binder.go \
  theme-d-macros.go \
  theme-d-representation.go \
  theme-d-file-handling.go \
  theme-d-compilation-utilities.go \
  theme-d-cycles.go \
  theme-d-compilation-errors.go \
  theme-d-debug.go

theme-d-compile.go : \
  theme-d-compiler.go \
  theme-d-debug.go

theme-d-compiler-constructors.go : \
  theme-d-forward-declarations.go \
  theme-d-common.go \
  theme-d-utilities.go \
  theme-d-constructors.go \
  theme-d-binder.go \
  theme-d-entities.go \
  theme-d-expressions.go \
  theme-d-symbol-table.go \
  theme-d-type-system1.go \
  theme-d-type-translation.go \
  theme-d-representation.go \
  theme-d-compiler-core-def.go \
  theme-d-compilation-utilities.go \
  theme-d-common-procedure-utilities.go \
  theme-d-debug.go

theme-d-compiler-core-def.go : \
  theme-d-entities.go \
  theme-d-address-allocation.go \
  theme-d-debug.go

theme-d-compiler.go : \
  theme-d-var-names.go \
  theme-d-print-target-object.go \
  theme-d-debug.go \
  theme-d-common.go \
  theme-d-hash-tables.go \
  theme-d-symbol-table.go \
  theme-d-compiler-core-def.go \
  theme-d-entities.go \
  theme-d-expressions.go \
  theme-d-keywords.go \
  theme-d-file-handling.go \
  theme-d-macro-config.go \
  theme-d-macros.go \
  theme-d-errors-common.go \
  theme-d-compilation-errors.go \
  theme-d-representation.go \
  theme-d-param-cache.go \
  theme-d-binder.go \
  theme-d-address-environment.go \
  theme-d-type-system1.go \
  theme-d-common-procedure-utilities.go \
  theme-d-procedure-utilities.go \
  theme-d-type-translation.go \
  theme-d-builtin-type-finalization.go \
  theme-d-translation-common.go \
  theme-d-constructors.go \
  theme-d-compiler-constructors.go \
  theme-d-parametrized-definitions.go \
  theme-d-parametrized-instances.go \
  theme-d-expression-translation.go \
  theme-d-letrec-compilation.go \
  theme-d-fields.go \
  theme-d-expression-rebinding.go \
  theme-d-expression-cloning.go \
  theme-d-special-procedures.go \
  theme-d-pcode-common.go \
  theme-d-interface-pcode-reading.go \
  theme-d-compilation-utilities.go \
  theme-d-compilation1.go \
  theme-d-compilation2.go \
  theme-d-cycles.go \
  theme-d-phase2-compilation.go \
  theme-d-compile-unit.go \
  theme-d-builtins.go

theme-d-constructors.go : \
  theme-d-forward-declarations.go \
  theme-d-entities.go \
  theme-d-expressions.go \
  theme-d-representation.go \
  theme-d-binder.go \
  theme-d-type-translation.go \
  theme-d-type-system1.go \
  theme-d-pcode-common.go \
  theme-d-debug.go

theme-d-cycles.go : \
  theme-d-forward-declarations.go \
  theme-d-representation.go \
  theme-d-entities.go \
  theme-d-expressions.go \
  theme-d-binder.go \
  theme-d-expression-cloning.go \
  theme-d-type-system1.go \
  theme-d-debug.go

theme-d-debug.go : \
  theme-d-forward-declarations.go \
  theme-d-print-target-object.go

theme-d-entities.go : \
  theme-d-forward-declarations.go

theme-d-errors-common.go : \
  theme-d-forward-declarations.go \
  theme-d-common.go \
  theme-d-var-names.go \
  theme-d-file-handling.go \
  theme-d-keywords.go \
  theme-d-entities.go \
  theme-d-expressions.go \
  theme-d-representation.go \
  theme-d-print-target-object.go

theme-d-expression-cloning.go : \
  theme-d-forward-declarations.go \
  theme-d-common.go \
  theme-d-utilities.go \
  theme-d-entities.go \
  theme-d-expressions.go \
  theme-d-representation.go \
  theme-d-binder.go \
  theme-d-type-translation.go \
  theme-d-common-procedure-utilities.go \
  theme-d-type-system1.go \
  theme-d-expression-translation.go \
  theme-d-constructors.go \
  theme-d-translation-common.go \
  theme-d-expression-rebinding.go \
  theme-d-parametrized-instances.go \
  theme-d-pcode-common.go \
  theme-d-debug.go

theme-d-expression-rebinding.go : \
  theme-d-expressions.go \
  theme-d-binder.go \
  theme-d-representation.go \
  theme-d-expression-translation.go

theme-d-expression-translation.go : \
  theme-d-forward-declarations.go \
  theme-d-binder.go \
  theme-d-entities.go \
  theme-d-representation.go \
  theme-d-type-system1.go \
  theme-d-expressions.go \
  theme-d-type-translation.go \
  theme-d-common-procedure-utilities.go \
  theme-d-special-procedures.go \
  theme-d-parametrized-definitions.go \
  theme-d-common.go \
  theme-d-translation-common.go \
  theme-d-pcode-common.go \
  theme-d-utilities.go \
  theme-d-debug.go

theme-d-expressions.go : \
  theme-d-entities.go \
  theme-d-forward-declarations.go

theme-d-fields.go : \
  theme-d-forward-declarations.go \
  theme-d-entities.go \
  theme-d-expressions.go \
  theme-d-common.go \
  theme-d-compiler-core-def.go \
  theme-d-type-system1.go \
  theme-d-representation.go

theme-d-file-handling.go :

theme-d-forward-declarations.go :

theme-d-hash-tables.go : \
  theme-d-entities.go

theme-d-implementation-pcode-reading.go : \
  theme-d-representation.go \
  theme-d-linker-core-def.go \
  theme-d-address-environment.go \
  theme-d-expression-translation.go \
  theme-d-entities.go \
  theme-d-expressions.go \
  theme-d-type-system1.go \
  theme-d-type-translation.go \
  theme-d-hash-tables.go \
  theme-d-common-procedure-utilities.go \
  theme-d-expression-cloning.go \
  theme-d-constructors.go \
  theme-d-keywords.go \
  theme-d-parametrized-instances.go \
  theme-d-binder.go \
  theme-d-utilities.go \
  theme-d-representation.go \
  theme-d-translation-common.go \
  theme-d-pcode-common.go \
  theme-d-errors-common.go \
  theme-d-debug.go

theme-d-interface-pcode-reading.go : \
  theme-d-forward-declarations.go \
  theme-d-common.go \
  theme-d-utilities.go \
  theme-d-compiler-core-def.go \
  theme-d-macros.go \
  theme-d-entities.go \
  theme-d-expressions.go \
  theme-d-address-environment.go \
  theme-d-pcode-common.go \
  theme-d-hash-tables.go \
  theme-d-representation.go \
  theme-d-keywords.go \
  theme-d-parametrized-instances.go \
  theme-d-type-system1.go \
  theme-d-type-translation.go \
  theme-d-expression-translation.go \
  theme-d-symbol-table.go \
  theme-d-common-procedure-utilities.go \
  theme-d-file-handling.go \
  theme-d-debug.go

theme-d-keywords.go : \
  theme-d-entities.go \
  theme-d-symbol-table.go

theme-d-letrec-compilation.go : \
  theme-d-forward-declarations.go \
  theme-d-compiler-core-def.go \
  theme-d-entities.go \
  theme-d-type-system1.go \
  theme-d-binder.go \
  theme-d-representation.go \
  theme-d-symbol-table.go \
  theme-d-procedure-utilities.go \
  theme-d-common-procedure-utilities.go

theme-d-link-program-split-b20241014-1.go : \
  theme-d-forward-declarations.go \
  theme-d-link-program.go \
  theme-d-linker-errors.go \
  theme-d-stripping.go \
  theme-d-target-compilation-common.go \
  theme-d-scheme-target-common.go \
  theme-d-scheme0-target-compilation.go \
  theme-d-scheme-target-compilation.go \
  theme-d-tree-il-target-compilation.go \
  theme-d-file-handling.go \
  theme-d-common.go \
  theme-d-linker-instantiation.go \
  theme-d-implementation-pcode-reading.go \
  theme-d-linker-core-def.go \
  theme-d-symbol-table.go \
  theme-d-special-procedures.go \
  theme-d-entities.go \
  theme-d-expressions.go \
  theme-d-address-environment.go \
  theme-d-representation.go \
  theme-d-param-cache.go \
  theme-d-binder.go \
  theme-d-translation-common.go \
  theme-d-hash-tables.go \
  theme-d-debug.go

theme-d-link-program-split.go : \
  theme-d-forward-declarations.go \
  theme-d-link-program.go \
  theme-d-linker-errors.go \
  theme-d-stripping.go \
  theme-d-target-compilation-common.go \
  theme-d-scheme-target-common.go \
  theme-d-scheme0-target-compilation.go \
  theme-d-scheme-target-compilation.go \
  theme-d-tree-il-target-compilation.go \
  theme-d-file-handling.go \
  theme-d-common.go \
  theme-d-linker-instantiation.go \
  theme-d-implementation-pcode-reading.go \
  theme-d-linker-core-def.go \
  theme-d-symbol-table.go \
  theme-d-special-procedures.go \
  theme-d-entities.go \
  theme-d-expressions.go \
  theme-d-address-environment.go \
  theme-d-representation.go \
  theme-d-param-cache.go \
  theme-d-binder.go \
  theme-d-translation-common.go \
  theme-d-hash-tables.go \
  theme-d-debug.go

theme-d-link-program.go : \
  theme-d-forward-declarations.go \
  theme-d-linker-errors.go \
  theme-d-address-allocation.go \
  theme-d-stripping.go \
  theme-d-target-compilation-common.go \
  theme-d-scheme-target-common.go \
  theme-d-scheme-target-objects.go \
  theme-d-scheme0-target-compilation.go \
  theme-d-scheme-target-compilation.go \
  theme-d-tree-il-target-compilation.go \
  theme-d-file-handling.go \
  theme-d-common.go \
  theme-d-linker-instantiation.go \
  theme-d-implementation-pcode-reading.go \
  theme-d-linker-core-def.go \
  theme-d-symbol-table.go \
  theme-d-special-procedures.go \
  theme-d-entities.go \
  theme-d-expressions.go \
  theme-d-address-environment.go \
  theme-d-address-table.go \
  theme-d-representation.go \
  theme-d-param-cache.go \
  theme-d-param-cache-opt.go \
  theme-d-binder.go \
  theme-d-translation-common.go \
  theme-d-hash-tables.go \
  theme-d-debug.go

theme-d-link.go : \
  theme-d-linker.go

theme-d-linker-core-def.go : \
  theme-d-common.go \
  theme-d-entities.go \
  theme-d-address-allocation.go \
  theme-d-debug.go

theme-d-linker-errors.go : \
  theme-d-common.go \
  theme-d-print-target-object.go \
  theme-d-file-handling.go \
  theme-d-errors-common.go \
  theme-d-entities.go \
  theme-d-representation.go \
  theme-d-debug.go

theme-d-linker-instantiation-1.go : \
  theme-d-forward-declarations.go \
  theme-d-common.go \
  theme-d-entities.go \
  theme-d-expressions.go \
  theme-d-stripping.go \
  theme-d-hash-tables.go \
  theme-d-linker-core-def.go \
  theme-d-expression-cloning.go \
  theme-d-type-system1.go \
  theme-d-utilities.go \
  theme-d-representation.go \
  theme-d-parametrized-definitions.go \
  theme-d-pcode-common.go \
  theme-d-representation.go \
  theme-d-debug.go

theme-d-linker-instantiation.go : \
  theme-d-forward-declarations.go \
  theme-d-common.go \
  theme-d-entities.go \
  theme-d-expressions.go \
  theme-d-stripping.go \
  theme-d-hash-tables.go \
  theme-d-linker-core-def.go \
  theme-d-expression-cloning.go \
  theme-d-type-system1.go \
  theme-d-utilities.go \
  theme-d-representation.go \
  theme-d-parametrized-definitions.go \
  theme-d-pcode-common.go \
  theme-d-representation.go \
  theme-d-debug.go

theme-d-linker.go : \
  theme-d-var-names.go \
  theme-d-print-target-object.go \
  theme-d-debug.go \
  theme-d-common.go \
  theme-d-hash-tables.go \
  theme-d-symbol-table.go \
  theme-d-entities.go \
  theme-d-expressions.go \
  theme-d-keywords.go \
  theme-d-file-handling.go \
  theme-d-errors-common.go \
  theme-d-linker-errors.go \
  theme-d-representation.go \
  theme-d-param-cache.go \
  theme-d-binder.go \
  theme-d-address-environment.go \
  theme-d-linker-core-def.go \
  theme-d-type-system1.go \
  theme-d-common-procedure-utilities.go \
  theme-d-type-translation.go \
  theme-d-builtin-type-finalization.go \
  theme-d-translation-common.go \
  theme-d-constructors.go \
  theme-d-parametrized-definitions.go \
  theme-d-parametrized-instances.go \
  theme-d-expression-translation.go \
  theme-d-expression-rebinding.go \
  theme-d-expression-cloning.go \
  theme-d-special-procedures.go \
  theme-d-pcode-common.go \
  theme-d-implementation-pcode-reading.go \
  theme-d-stripping.go \
  theme-d-cycles.go \
  theme-d-linker-instantiation.go \
  theme-d-target-compilation-common.go \
  theme-d-scheme-target-common.go \
  theme-d-tree-il-target-compilation.go \
  theme-d-scheme0-target-compilation.go \
  theme-d-scheme-target-compilation.go \
  theme-d-builtins.go \
  theme-d-link-program.go \
  theme-d-link-program-split.go

theme-d-macro-config.go :

theme-d-macros.go : \
  theme-d-macro-config.go \
  theme-d-keywords.go \
  theme-d-var-names.go \
  theme-d-file-handling.go \
  theme-d-pcode-common.go \
  theme-d-debug.go

theme-d-param-cache-opt.go : \
  theme-d-forward-declarations.go \
  theme-d-entities.go \
  theme-d-representation.go \
  theme-d-param-cache.go

theme-d-param-cache.go : \
  theme-d-forward-declarations.go \
  theme-d-entities.go \
  theme-d-representation.go

theme-d-parametrized-definitions.go : \
  theme-d-forward-declarations.go \
  theme-d-entities.go \
  theme-d-expressions.go \
  theme-d-representation.go \
  theme-d-param-cache.go \
  theme-d-param-cache-opt.go \
  theme-d-hash-tables.go \
  theme-d-type-system1.go \
  theme-d-common-procedure-utilities.go \
  theme-d-type-translation.go \
  theme-d-binder.go \
  theme-d-keywords.go \
  theme-d-translation-common.go \
  theme-d-constructors.go \
  theme-d-utilities.go \
  theme-d-common.go \
  theme-d-errors-common.go \
  theme-d-pcode-common.go \
  theme-d-print-target-object.go \
  theme-d-debug.go

theme-d-parametrized-instances.go : \
  theme-d-forward-declarations.go \
  theme-d-entities.go \
  theme-d-utilities.go \
  theme-d-representation.go \
  theme-d-parametrized-definitions.go \
  theme-d-constructors.go \
  theme-d-binder.go \
  theme-d-common.go \
  theme-d-keywords.go \
  theme-d-common-procedure-utilities.go \
  theme-d-type-translation.go \
  theme-d-param-cache.go \
  theme-d-type-system1.go \
  theme-d-debug.go

theme-d-pcode-common.go : \
  theme-d-entities.go \
  theme-d-representation.go

theme-d-phase2-compilation.go : \
  theme-d-entities.go \
  theme-d-expressions.go \
  theme-d-common.go \
  theme-d-cycles.go \
  theme-d-compiler-core-def.go \
  theme-d-symbol-table.go \
  theme-d-representation.go \
  theme-d-pcode-common.go \
  theme-d-type-system1.go \
  theme-d-file-handling.go \
  theme-d-debug.go

theme-d-print-target-object.go : \
  theme-d-forward-declarations.go \
  theme-d-entities.go \
  theme-d-var-names.go \
  theme-d-representation.go

theme-d-procedure-utilities.go :

theme-d-representation.go : \
  theme-d-entities.go \
  theme-d-expressions.go \
  theme-d-keywords.go \
  theme-d-common.go \
  theme-d-forward-declarations.go

theme-d-scheme-target-common.go : \
  theme-d-forward-declarations.go \
  theme-d-common.go \
  theme-d-target-compilation-common.go \
  theme-d-linker-core-def.go \
  theme-d-entities.go \
  theme-d-expressions.go \
  theme-d-linker-instantiation.go \
  theme-d-cycles.go \
  theme-d-representation.go \
  theme-d-type-system1.go \
  theme-d-type-translation.go \
  theme-d-parametrized-definitions.go \
  theme-d-hash-tables.go \
  theme-d-stripping.go \
  theme-d-pcode-common.go \
  theme-d-debug.go

theme-d-scheme-target-compilation-1.go : \
  theme-d-forward-declarations.go \
  theme-d-target-compilation-common.go \
  theme-d-scheme-target-common.go \
  theme-d-scheme-target-objects.go \
  theme-d-entities.go \
  theme-d-expressions.go \
  theme-d-linker-core-def.go \
  theme-d-representation.go \
  theme-d-type-system1.go \
  theme-d-stripping.go \
  theme-d-hash-tables.go \
  theme-d-common.go \
  theme-d-special-procedures.go \
  theme-d-parametrized-definitions.go \
  theme-d-param-cache.go \
  theme-d-pcode-common.go \
  theme-d-debug.go

theme-d-scheme-target-compilation.go : \
  theme-d-forward-declarations.go \
  theme-d-target-compilation-common.go \
  theme-d-scheme-target-common.go \
  theme-d-scheme-target-objects.go \
  theme-d-entities.go \
  theme-d-expressions.go \
  theme-d-linker-core-def.go \
  theme-d-representation.go \
  theme-d-type-system1.go \
  theme-d-stripping.go \
  theme-d-hash-tables.go \
  theme-d-common.go \
  theme-d-special-procedures.go \
  theme-d-parametrized-definitions.go \
  theme-d-param-cache.go \
  theme-d-pcode-common.go \
  theme-d-debug.go

theme-d-scheme-target-objects.go : \
  theme-d-forward-declarations.go \
  theme-d-target-compilation-common.go \
  theme-d-scheme-target-common.go \
  theme-d-linker-core-def.go \
  theme-d-representation.go \
  theme-d-cycles.go \
  theme-d-entities.go \
  theme-d-expressions.go \
  theme-d-type-system1.go \
  theme-d-expression-cloning.go \
  theme-d-stripping.go \
  theme-d-special-procedures.go \
  theme-d-hash-tables.go \
  theme-d-param-cache.go \
  theme-d-parametrized-definitions.go \
  theme-d-common-procedure-utilities.go \
  theme-d-common.go \
  theme-d-utilities.go \
  theme-d-debug.go

theme-d-scheme0-target-compilation-1.go : \
  theme-d-forward-declarations.go \
  theme-d-target-compilation-common.go \
  theme-d-scheme-target-common.go \
  theme-d-scheme-target-objects.go \
  theme-d-linker-core-def.go \
  theme-d-representation.go \
  theme-d-cycles.go \
  theme-d-entities.go \
  theme-d-expressions.go \
  theme-d-type-system1.go \
  theme-d-expression-cloning.go \
  theme-d-stripping.go \
  theme-d-special-procedures.go \
  theme-d-hash-tables.go \
  theme-d-param-cache.go \
  theme-d-parametrized-definitions.go \
  theme-d-common-procedure-utilities.go \
  theme-d-common.go \
  theme-d-debug.go

theme-d-scheme0-target-compilation-2.go : \
  theme-d-forward-declarations.go \
  theme-d-target-compilation-common.go \
  theme-d-scheme-target-common.go \
  theme-d-scheme-target-objects.go \
  theme-d-linker-core-def.go \
  theme-d-representation.go \
  theme-d-cycles.go \
  theme-d-entities.go \
  theme-d-expressions.go \
  theme-d-type-system1.go \
  theme-d-expression-cloning.go \
  theme-d-stripping.go \
  theme-d-special-procedures.go \
  theme-d-hash-tables.go \
  theme-d-param-cache.go \
  theme-d-parametrized-definitions.go \
  theme-d-common-procedure-utilities.go \
  theme-d-common.go \
  theme-d-debug.go

theme-d-scheme0-target-compilation-3.go : \
  theme-d-forward-declarations.go \
  theme-d-target-compilation-common.go \
  theme-d-scheme-target-common.go \
  theme-d-scheme-target-objects.go \
  theme-d-linker-core-def.go \
  theme-d-representation.go \
  theme-d-cycles.go \
  theme-d-entities.go \
  theme-d-expressions.go \
  theme-d-type-system1.go \
  theme-d-expression-cloning.go \
  theme-d-stripping.go \
  theme-d-special-procedures.go \
  theme-d-hash-tables.go \
  theme-d-param-cache.go \
  theme-d-parametrized-definitions.go \
  theme-d-common-procedure-utilities.go \
  theme-d-common.go \
  theme-d-debug.go

theme-d-scheme0-target-compilation.go : \
  theme-d-forward-declarations.go \
  theme-d-target-compilation-common.go \
  theme-d-scheme-target-common.go \
  theme-d-scheme-target-objects.go \
  theme-d-entities.go \
  theme-d-expressions.go \
  theme-d-linker-core-def.go \
  theme-d-representation.go \
  theme-d-type-system1.go \
  theme-d-stripping.go \
  theme-d-hash-tables.go \
  theme-d-common.go \
  theme-d-special-procedures.go \
  theme-d-parametrized-definitions.go \
  theme-d-param-cache.go \
  theme-d-pcode-common.go \
  theme-d-debug.go

theme-d-special-procedures.go : \
  theme-d-forward-declarations.go \
  theme-d-utilities.go \
  theme-d-representation.go \
  theme-d-entities.go \
  theme-d-expressions.go \
  theme-d-binder.go \
  theme-d-type-translation.go \
  theme-d-type-system1.go \
  theme-d-debug.go

theme-d-stripping.go : \
  theme-d-common.go \
  theme-d-linker-core-def.go \
  theme-d-entities.go \
  theme-d-expressions.go \
  theme-d-hash-tables.go \
  theme-d-expression-cloning.go \
  theme-d-representation.go \
  theme-d-pcode-common.go \
  theme-d-debug.go

theme-d-symbol-table.go : \
  theme-d-hash-tables.go

theme-d-target-compilation-common.go : \
  theme-d-forward-declarations.go \
  theme-d-linker-core-def.go \
  theme-d-entities.go \
  theme-d-representation.go \
  theme-d-expression-cloning.go \
  theme-d-special-procedures.go \
  theme-d-expressions.go \
  theme-d-hash-tables.go \
  theme-d-type-system1.go

theme-d-translation-common.go : \
  theme-d-forward-declarations.go \
  theme-d-common.go \
  theme-d-utilities.go \
  theme-d-entities.go \
  theme-d-expressions.go \
  theme-d-representation.go \
  theme-d-type-system1.go \
  theme-d-type-translation.go \
  theme-d-binder.go \
  theme-d-debug.go \
  theme-d-pcode-common.go

theme-d-tree-il-target-compilation-1.go : \
  theme-d-forward-declarations.go \
  theme-d-common.go \
  theme-d-linker-core-def.go \
  theme-d-target-compilation-common.go \
  theme-d-entities.go \
  theme-d-expressions.go \
  theme-d-linker-instantiation.go \
  theme-d-parametrized-definitions.go \
  theme-d-cycles.go \
  theme-d-representation.go \
  theme-d-type-system1.go \
  theme-d-hash-tables.go \
  theme-d-expression-cloning.go \
  theme-d-special-procedures.go \
  theme-d-param-cache.go \
  theme-d-stripping.go \
  theme-d-type-translation.go \
  theme-d-utilities.go \
  theme-d-pcode-common.go \
  theme-d-debug.go

theme-d-tree-il-target-compilation.go : \
  theme-d-forward-declarations.go \
  theme-d-common.go \
  theme-d-linker-core-def.go \
  theme-d-target-compilation-common.go \
  theme-d-entities.go \
  theme-d-expressions.go \
  theme-d-linker-instantiation.go \
  theme-d-parametrized-definitions.go \
  theme-d-cycles.go \
  theme-d-representation.go \
  theme-d-type-system1.go \
  theme-d-hash-tables.go \
  theme-d-expression-cloning.go \
  theme-d-special-procedures.go \
  theme-d-param-cache.go \
  theme-d-stripping.go \
  theme-d-type-translation.go \
  theme-d-utilities.go \
  theme-d-pcode-common.go \
  theme-d-debug.go

theme-d-type-translation.go : \
  theme-d-forward-declarations.go \
  theme-d-utilities.go \
  theme-d-representation.go \
  theme-d-entities.go \
  theme-d-type-system1.go \
  theme-d-binder.go \
  theme-d-common-procedure-utilities.go \
  theme-d-param-cache.go \
  theme-d-debug.go

theme-d-var-names.go :

theme-d-type-system1.go : \
  theme-d-forward-declarations.go \
  theme-d-utilities.go \
  theme-d-common.go \
  theme-d-address-allocation.go \
  theme-d-representation.go \
  theme-d-entities.go \
  theme-d-expressions.go \
  theme-d-binder.go \
  theme-d-hash-tables.go \
  theme-d-pcode-common.go \
  theme-d-debug.go

theme-d-utilities.go : \
  theme-d-forward-declarations.go \
  theme-d-representation.go \
  theme-d-entities.go \
  theme-d-binder.go \
  theme-d-debug.go

