
;; Copyright (C) 2024 Tommi Höynälänmaa

(define-module (theme-d testing custom1)
  #:duplicates (merge-generics replace last)
  #:export (bytevector-zero
	    bytevector-zero2
	    _b_my-map
	    my-apply
	    count-prim-classes
	    return-invalid-object
	    return-valid-object))

(import (rnrs))
(import (rnrs bytevectors))
(import (theme-d runtime rte-base))
(import (theme-d runtime runtime-theme-d-environment))

(define bytevector-zero (make-bytevector 1 0))

(define bytevector-zero2 (make-bytevector 5 0))

(define (_b_my-map %arglist %result proc . lists)
  (let* ((arglist2 (general-list->list %arglist))
	 (proc2 (lambda args
		  (_i_call-proc proc args arglist2)))
	 (result
	  (apply map (cons proc2 lists))))
    result))

(define (my-apply %arglist %result proc args)
  (let ((arglist2 (general-list->list %arglist)))
    (_i_call-proc proc args arglist2)))

(define (count-prim-classes)
  (let ((i-count 0))
    (for-each (lambda (lst)
		(if (eq? (cadr lst) bytevector?)
		    (set! i-count (+ i-count 1))))
	      gl-custom-prim-classes)
    i-count))

(define (return-invalid-object)
  2/3)

(define (return-valid-object)
  1)

