
;; Copyright (C) 2018, 2021-2024 Tommi Höynälänmaa
;; Distributed under GNU General Public License version 3,
;; see file doc/GPL-3.

(define-module (theme-d testing test-info))

(export nr-of-tests modules no-run
        custom1-programs custom2-programs scripts
        guile-hashtable-units extra-math-units
        l-split-linking)

(define nr-of-tests 910)

(define modules '(2 3 66 75 76 78 82 83 99 106 107 108 139 140
                    218 221 224 226 247 280 303 312 316 322 323 359 362
                    364 365 367 369 375 377 384 391 392 394 398 404 405
                    446 535 537 539 541 553 554 573 603 604 618 630 692
                    703 732 752 754 760 785 788 795 796 806 807 809 811
                    819 833 838 839 845 868 872 879 886))

;; These tests are interactive so we don't run them here.
(define no-run '(114 230 271 274 449))

;; These tests require additional definitions
;; into the runtime environment.
(define custom1-programs
  '(142 143 144 145 223 225 227 228 318 393 395 396 397
        496 497 517 562 662 663 664 665 666 696 700 711))

(define custom2-programs '(279 281 319 320 399 518 561 697 705 712 713
                               822 876 877 878 881 883 884 885 887 888 889))

(define scripts '(591 597 598 606 607 716 717 720 730))

(define guile-hashtable-units '(390 587 635 738))

(define extra-math-units '(684 685 686 687))

(define l-split-linking '(873))
